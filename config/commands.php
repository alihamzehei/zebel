<?php

declare(strict_types=1);

use App\Plan\Application as Plan;
use App\User\Application as User;
use App\Collect\Application as Collect;
use App\Target\Application as Target;
use App\Schedule\Application as Schedule;
use App\Notification\Application as Notification;


return [
    /*
    |--------------------------------------------------------------------------
    | Plan context
    |--------------------------------------------------------------------------
    |
    */
    Plan\Command\PlanJourneyCommand::class => Plan\Handler\PlanJourneyHandler::class,
    Plan\Command\UpdateJourneyCommand::class => Plan\Handler\UpdateJourneyHandler::class,
    Plan\Command\PublishJourneyCommand::class => Plan\Handler\PublishJourneyHandler::class,
    Plan\Command\ToggleJourneyCommand::class => Plan\Handler\ToggleJourneyHandler::class,
    Plan\Command\RemoveJourneyCommand::class => Plan\Handler\RemoveJourneyHandler::class,


    /*
    |--------------------------------------------------------------------------
    | User context
    |--------------------------------------------------------------------------
    |
    */

    User\Command\RegisterUserCommand::class => User\Handler\RegisterUserHandler::class,
    User\Command\LoginUserCommand::class => User\Handler\LoginUserHandler::class,


    /*
    |--------------------------------------------------------------------------
    | Collect context
    |--------------------------------------------------------------------------
    |
    */

    Collect\Command\CaptureActivityCommand::class => Collect\Handler\CaptureActivityHandler::class,

    /*
    |--------------------------------------------------------------------------
    | Target context
    |--------------------------------------------------------------------------
    |
    */

    Target\Command\RegisterTargetCommand::class => Target\Handler\RegisterTargetHandler::class,

    /*
    |--------------------------------------------------------------------------
    | Schedule context
    |--------------------------------------------------------------------------
    |
    */

    Schedule\Command\CreateScheduleCommand::class => Schedule\Handler\CreateScheduleHandler::class,
    Schedule\Command\DispatchSchedulesCommand ::class => Schedule\Handler\DispatchSchedulesHandler::class,
    Schedule\Command\RunScheduleCommand ::class => Schedule\Handler\RunScheduleHandler::class,

    /*
    |--------------------------------------------------------------------------
    | Notification context
    |--------------------------------------------------------------------------
    |
    */

    Notification\Command\CreateNotificationCommand::class => Notification\Handler\CreateNotificationHandler::class,
    Notification\Command\SendNotifyCommand::class => Notification\Handler\SendNotifyHandler::class,
    Notification\Command\PersistNotifyCommand::class => Notification\Handler\PersistNotifyHandler::class,
];
