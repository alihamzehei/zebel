<?php

return [
    'host' => env('RABBITMQ_HOST', 'localhost'),
    'port' => env('RABBITMQ_PORT', '5672'),
    'user' => env('RABBITMQ_USER', 'admin'),
    'password' => env('RABBITMQ_PASSWORD', 'admin'),
    'queue' => [
        'durable' => true,
        'exclusive' => true,
        'auto_delete' => true,
    ],
    'exchange' => [
        'name' => 'fanout',
    ]
];
