<?php

declare(strict_types=1);

use App\Plan\Application as Plan;
use App\Target\Application as Target;
use App\Collect\Application as Collect;
use App\Notification\Application as Notification;
use App\Analytic\Application as Analytic;

return [
    /*
    |--------------------------------------------------------------------------
    | Plan context
    |--------------------------------------------------------------------------
    |
    */

    Plan\Query\JourneyQuery::class => Plan\Query\JourneyQueryHandler::class,
    Plan\Query\JourneysQuery::class => Plan\Query\JourneysQueryHandler::class,
    Plan\Query\TriggerQuery::class => Plan\Query\TriggerQueryHandler::class,

    /*
    |--------------------------------------------------------------------------
    | Target context
    |--------------------------------------------------------------------------
    |
    */

    Target\Query\TargetQuery::class => Target\Query\TargetQueryHandler::class,

    /*
    |--------------------------------------------------------------------------
    | Collect context
    |--------------------------------------------------------------------------
    |
    */

    Collect\Query\ActivitiesQuery::class => Collect\Query\ActivitiesQueryHandler::class,
    Collect\Query\ActivityByIdQuery::class => Collect\Query\ActivityByIdQueryHandler::class,
    Collect\Query\ActivityByIdsQuery::class => Collect\Query\ActivityByIdsQueryHandler::class,
    /*
    |--------------------------------------------------------------------------
    | Notification context
    |--------------------------------------------------------------------------
    |
    */

    Notification\Query\EventAttributesQuery::class => Notification\Query\EventAttributesQueryHandler::class,
    Notification\Query\NotificationQuery::class => Notification\Query\NotificationQueryHandler::class,

    /*
    |--------------------------------------------------------------------------
    | Analytic context
    |--------------------------------------------------------------------------
    |
    */

    Analytic\Query\CheckpointUserCountQuery::class => Analytic\Query\CheckpointUserCountQueryHandler::class,
    Analytic\Query\JourneyConversionQuery::class => Analytic\Query\JourneyConversionQueryHandler::class,
    Analytic\Query\JourneyUserCountQuery::class => Analytic\Query\JourneyUserCountQueryHandler::class,
];
