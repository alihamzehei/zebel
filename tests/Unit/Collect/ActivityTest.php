<?php

declare(strict_types=1);

namespace Test\Unit\Collect;

use DateTimeImmutable;
use App\Collect\Domain\Event\ActivityCollected;
use App\Collect\Domain\Model\Activity;
use App\Collect\Domain\ValueObject\ActivityId;
use Tests\Support\TestCase;

class ActivityTest extends TestCase
{
    public function test_it_records_activity()
    {
        $activity = new Activity(new ActivityId('foo'), 'The title', 1, new DateTimeImmutable(), [
            'foo' => 'bar',
        ]);

        $this->assertInstanceOf(ActivityCollected::class, $activity->pullRecordedEvents()[0]);
    }
}
