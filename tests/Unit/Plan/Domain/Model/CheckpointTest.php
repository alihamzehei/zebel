<?php

declare(strict_types=1);

namespace Tests\Unit\Plan\Domain;

use Exception;
use App\Plan\Domain\Model\Checkpoint\IdleCheckpoint;
use App\Plan\Domain\Model\Checkpoint\ReachabilityConditionalCheckpoint;
use App\Plan\Domain\Model\Checkpoint\StopCheckpoint;
use App\Plan\Domain\ValueObject\CheckpointId;
use App\Plan\Domain\ValueObject\Timer;
use Tests\Support\TestCase;;

class CheckpointTest extends TestCase
{
    public function test_a_stop_checkpoint_cannot_have_child()
    {
        $this->expectException(Exception::class);

        new StopCheckpoint(
            new CheckpointId('x', new CheckpointId('y')),
            ['metaData']
        );
    }

    public function test_an_idle_checkpoint_should_have_left_child()
    {
        $this->expectException(Exception::class);

        new IdleCheckpoint(
            new CheckpointId('x'),
            ['metaData'],
            Timer::fromMinimumMinutes()
        );
    }

    public function test_an_idle_checkpoint_cannot_have_right_child()
    {
        $this->expectException(Exception::class);

        new IdleCheckpoint(
            new CheckpointId('x', new CheckpointId('y'), new CheckpointId('z')),
            ['metaData'],
            Timer::fromMinimumMinutes()
        );
    }

    public function test_a_conditional_checkpoint_should_have_two_children()
    {
        $this->expectException(Exception::class);

        new ReachabilityConditionalCheckpoint(
            new CheckpointId('x'),
            ['metaData'],
            'check-something'
        );
    }
}
