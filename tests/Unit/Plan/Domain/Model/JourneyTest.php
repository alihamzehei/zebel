<?php

declare(strict_types=1);

namespace Tests\Unit\Plan\Domain;

use Tests\Support\TestCase;;
use App\Plan\Domain\Model\Journey;
use App\Plan\Domain\Model\Trigger;
use App\Plan\Domain\ValueObject\Name;
use App\Plan\Domain\Event\JourneyPlanned;
use App\Plan\Domain\ValueObject\JourneyId;
use App\Plan\Domain\Factory\CheckpointAggregateFactory;
use Tests\Support\Trait\Snapshot;

class JourneyTest extends TestCase
{
    use Snapshot;

    public function test_it_records_journey_creation()
    {
        $raw = json_decode($this->snap('journey.json'), true);
        $factory = new CheckpointAggregateFactory($raw['root'], $raw['checkpoints']);

        $journey = Journey::plan(
            new JourneyId(uniqid()),
            new Name($raw['name']),
            Trigger::CartCreated,
            $raw['meta'],
            $factory->create()
        );

        $this->assertInstanceOf(JourneyPlanned::class, $journey->pullRecordedEvents()[0]);
    }
}
