<?php

declare(strict_types=1);

namespace Tests\Unit\Plan\Domain\ValueObject;

use App\Plan\Domain\ValueObject\Timer;
use Tests\Support\TestCase;;

class TimerTest extends TestCase
{
    private const MINUTES = 12;
    private Timer $timer;

    protected function setUp(): void
    {
        $this->timer = new Timer(self::MINUTES);
    }

    public function test_compare_timers()
    {
        $this->assertTrue($this->timer->isEquals(new Timer(self::MINUTES)));
    }

    public function test_static_creators()
    {
        $this->assertTrue($this->timer->isEquals(Timer::fromSeconds(self::MINUTES * 60)));
        $this->assertTrue($this->timer->isEquals(Timer::fromMinutes(self::MINUTES)));
    }

    public function test_convertors()
    {
        $this->assertSame($this->timer->asMinutes(), self::MINUTES);
        $this->assertSame($this->timer->asSeconds(), self::MINUTES * 60);
    }
}
