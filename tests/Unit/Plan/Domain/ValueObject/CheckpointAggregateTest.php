<?php

declare(strict_types=1);

namespace Tests\Unit\Plan\Domain\ValueObject;

use App\Plan\Domain\Model\Checkpoint\ActionCheckpoint;
use App\Plan\Domain\Model\Checkpoint\EventOccurrenceConditionalCheckpoint;
use App\Plan\Domain\Model\Checkpoint\IdleCheckpoint;
use App\Plan\Domain\Model\Checkpoint\ReachabilityConditionalCheckpoint;
use App\Plan\Domain\Model\Checkpoint\StopCheckpoint;
use App\Plan\Domain\ValueObject\CheckpointAggregate;
use App\Plan\Domain\ValueObject\CheckpointId;
use App\Plan\Domain\ValueObject\Timer;
use App\Shared\Domain\ValueObject\ValueError;
use Tests\Support\TestCase;;

class CheckpointAggregateTest extends TestCase
{
    public function test_create_a_valid_aggregate()
    {
        $action = new CheckpointId('action-id', $stop2 = new CheckpointId('stop-2-id'));
        $condition = new CheckpointId('cond-id', $stop = new CheckpointId('stop-id'), $action);
        $idle = new CheckpointId('idle-id', $condition);

        $aggregate = new CheckpointAggregate(
            $idle,
            new IdleCheckpoint($idle, ['metadata'], Timer::fromMinimumMinutes()),
            new ReachabilityConditionalCheckpoint($condition, ['metadata'], 'check-sth'),
            new ActionCheckpoint($action, ['metadata'], 'notification_id'),
            new StopCheckpoint($stop, ['metadata']),
            new StopCheckpoint($stop2, ['metadata']),
        );

        $this->assertInstanceOf(CheckpointAggregate::class, $aggregate);
    }

    public function test_idle_checkpoint_cannot_have_idle_child()
    {
        $this->expectException(ValueError::class);
        $this->expectExceptionMessage('Idle checkpoint can not have idle checkpoint as child');

        $action = new CheckpointId('action-id', $stop2 = new CheckpointId('stop-2-id'));
        $condition = new CheckpointId('cond-id', $stop = new CheckpointId('stop-id'), $action);
        $idle = new CheckpointId('idle-id', $condition);
        $idle2 = new CheckpointId('idle-id2', $idle);

        new CheckpointAggregate(
            $idle,
            new IdleCheckpoint($idle, ['metaData'], Timer::fromMinimumMinutes()),
            new ReachabilityConditionalCheckpoint($condition, ['metaData'], 'check-sth'),
            new ActionCheckpoint($action, ['metaData'], 'notification_id'),
            new IdleCheckpoint($idle2, ['metaData'], Timer::fromMinimumMinutes()),
            new StopCheckpoint($stop, ['metaData']),
            new StopCheckpoint($stop2, ['metaData']),
        );
    }

    public function test_a_root_should_exists_in_checkpoints()
    {
        $this->expectException(ValueError::class);

        new CheckpointAggregate(
            new CheckpointId('a', new CheckpointId('b')),
            new ActionCheckpoint(
                new CheckpointId('c', new CheckpointId('stop')),
                ['metaData'],
                'notification_id'
            ),
            new StopCheckpoint(new CheckpointId('stop'), ['metaData'])
        );
    }

    public function test_a_stop_checkpoint_cannot_be_a_root()
    {
        $this->expectException(ValueError::class);

        new CheckpointAggregate(
            new CheckpointId('root-id'),
            new StopCheckpoint(new CheckpointId('root-id'), ['metaData'])
        );
    }

    public function test_children_should_exist_in_aggregate()
    {
        $this->expectException(ValueError::class);
        new CheckpointAggregate(
            $rootId = new CheckpointId('root', $actionId = new CheckpointId('c', new CheckpointId('non-existing-stop'))),
            new IdleCheckpoint($rootId, ['metaData'], Timer::fromMinimumMinutes()),
            new ActionCheckpoint($actionId, ['metaData'], 'notification_id'),
            new StopCheckpoint(new CheckpointId('stop'), ['metaData'], )
        );
    }

    public function test_reachability_conditional_checkpoint_cannot_have_itself_as_child()
    {
        $this->expectException(ValueError::class);
        $this->expectExceptionMessage('reachability conditional checkpoint can not has reachability conditional checkpoint as child');

        $action = new CheckpointId('action-id', $stop2 = new CheckpointId('stop-2-id'));
        $reachabilityCondition = new CheckpointId('reachability-id', $stop2 = new CheckpointId('stop-2-id'), $action);
        $reachabilityCondition2 = new CheckpointId('cond-id', $stop = new CheckpointId('stop-id'), $reachabilityCondition);
        $stop3 = new CheckpointId('stop-3-id');
        $stop4 = new CheckpointId('stop-4-id');
        $idle = new CheckpointId('idle-id', $reachabilityCondition2);

        new CheckpointAggregate(
            $idle,
            new IdleCheckpoint($idle, ['metaData'], Timer::fromMinimumMinutes()),
            new ReachabilityConditionalCheckpoint($reachabilityCondition2, ['metaData'], 'reachability question?'),
            new ReachabilityConditionalCheckpoint($reachabilityCondition, ['metaData'], 'reachability second question?'),
            new ActionCheckpoint($action, ['metaData'], 'notification_id'),
            new StopCheckpoint($stop, ['metaData']),
            new StopCheckpoint($stop3, ['metaData']),
            new StopCheckpoint($stop4, ['metaData']),
            new StopCheckpoint($stop2, ['metaData']),
        );
    }

    public function test_event_occurrence_conditional_checkpoint_cannot_has_conditional_checkpoint_as_child()
    {
        $this->expectException(ValueError::class);
        $this->expectExceptionMessage('event occurrence conditional checkpoint can not has event occurrence conditional checkpoint as child');

        $action = new CheckpointId('action-id', $stop2 = new CheckpointId('stop-2-id'));
        $eventOccurrenceCondition = new CheckpointId('reachability-id', $stop2 = new CheckpointId('stop-2-id'), $action);
        $eventOccurrenceCondition2 = new CheckpointId('event-occurrence-id', $stop3 = new CheckpointId('stop-3-id'), $eventOccurrenceCondition);
        $idle = new CheckpointId('idle-id', $eventOccurrenceCondition2);

        new CheckpointAggregate(
            $idle,
            new IdleCheckpoint($idle, ['metaData'], Timer::fromMinimumMinutes()),
            new EventOccurrenceConditionalCheckpoint($eventOccurrenceCondition, ['metaData'], 'event occurrence question?'),
            new EventOccurrenceConditionalCheckpoint($eventOccurrenceCondition2, ['metaData'], 'event occurrence second question?'),
            new ActionCheckpoint($action, ['metaData'], 'notification_id'),
            new StopCheckpoint($stop3, ['metaData']),
            new StopCheckpoint($stop2, ['metaData']),
        );
    }
}
