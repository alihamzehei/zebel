<?php

declare(strict_types=1);

namespace Tests\Unit\Schedule\Domain\ValueObject;

use Tests\Support\TestCase;
use App\Schedule\Domain\ValueObject\IdleTime;
use App\Shared\Domain\ValueObject\ValueError;

class IdleTimeTest extends TestCase
{
    public function test_time_length_cant_be_smaller_than_the_min_length()
    {
        $this->expectException(ValueError::class);

        $this->expectExceptionMessage('Idle time must be at least '.IdleTime::MINIMUM_TIMER_IN_MINUTES.' minutes');

        new IdleTime(IdleTime::MINIMUM_TIMER_IN_MINUTES - 1);
    }
}
