<?php

declare(strict_types=1);

namespace Tests\Unit\Schedule\Domain\ValueObject;

use DateTimeImmutable;
use Tests\Support\TestCase;
use App\Schedule\Domain\ValueObject\ActivityName;
use App\Schedule\Domain\ValueObject\Expectant;

class ExpectantTest extends TestCase
{
    private Expectant $expectant;

    protected function setUp(): void
    {
        $this->expectant = new Expectant(
            new ActivityName('Do something'), new DateTimeImmutable('-10 minutes')
        );
    }

    public function test_before_dates()
    {
        $this->assertFalse(
            $this->expectant->isMatchWith(new Expectant($this->expectant->activity, new DateTimeImmutable('-11 minutes')))
        );
    }

    public function test_after_dates()
    {
        $this->assertTrue(
            $this->expectant->isMatchWith(new Expectant($this->expectant->activity, new DateTimeImmutable('-10 minutes')))
        );

        $this->assertTrue(
            $this->expectant->isMatchWith(new Expectant($this->expectant->activity, new DateTimeImmutable()))
        );
    }

    public function test_mismatch_name()
    {
        $this->assertFalse(
            $this->expectant->isMatchWith(new Expectant(new ActivityName('Do something else'), new DateTimeImmutable()))
        );
    }
}
