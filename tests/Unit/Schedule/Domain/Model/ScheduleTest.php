<?php

declare(strict_types=1);

namespace Tests\Unit\Schedule\Domain\Model;

use DateTimeImmutable;
use Tests\Support\TestCase;
use App\Shared\Domain\DomainError;
use App\Schedule\Domain\Model\Target;
use App\Schedule\Domain\Model\Activity;
use Tests\Support\Factory\ScheduleFactory;
use App\Schedule\Domain\ValueObject\Status;
use App\Schedule\Domain\ValueObject\IdleTime;
use App\Schedule\Domain\ValueObject\Expectant;
use App\Schedule\Domain\ValueObject\Activities;
use App\Schedule\Domain\ValueObject\ActivityName;
use App\Schedule\Domain\Event\ScheduleInitialized;
use App\Schedule\Domain\Event\IdleCheckpointReached;
use App\Schedule\Domain\Event\StopCheckpointReached;
use App\Schedule\Domain\Model\Checkpoint\IdleCheckpoint;
use App\Schedule\Domain\Model\Checkpoint\StopCheckpoint;
use App\Schedule\Domain\Model\Checkpoint\ActionCheckpoint;
use App\Schedule\Domain\Model\Checkpoint\EventOccurrenceCheckpoint;
use App\Schedule\Domain\Model\Checkpoint\CheckReachabilityCheckpoint;

class ScheduleTest extends TestCase
{
    public function test_initialize()
    {
        $schedule = ScheduleFactory::fromStatus(Status::init())->create();
        $this->assertEventPublishedAtLast(ScheduleInitialized::class, $schedule->pullRecordedEvents());
    }

    public function test_idle_state()
    {
        $schedule = ScheduleFactory::checkpoints([
            new ActionCheckpoint('id-1', 'id-2', false, 'baz'),
            new ActionCheckpoint('id-2', 'id-3', false, 'baz'),
            new IdleCheckpoint('id-3', 'id-4', false, $idleTime = new IdleTime(120)),
            new StopCheckpoint('id-4', false),
        ])->create();

        $schedule->run(fn($schedule) => null);

        $this->assertEventPublishedAtLast(IdleCheckpointReached::class, $schedule->pullRecordedEvents());

        $this->assertTrue($schedule->status()->is(Status::idle()));

        $diff = $schedule->idleUntil()->diff(new DateTimeImmutable());

        $this->assertEqualsWithDelta(120, $diff->h * 60 + $diff->i, 1);
    }

    public function test_schedule_knows_the_next_activity()
    {
        $schedule = ScheduleFactory::checkpoints([
            new ActionCheckpoint('id-1', 'id-2', false, 'foo'),
            new ActionCheckpoint('id-2', 'id-3', false, 'foo'),
            new IdleCheckpoint('id-3', 'id-4', false, new IdleTime(120)),
            new EventOccurrenceCheckpoint(
                'id-4',
                'id-5',
                'id-6',
                false,
                $activity = new Activity(uniqid(), new ActivityName('foo-activity'), new DateTimeImmutable())
            ),
            new ActionCheckpoint('id-5', 'id-7', false, 'foo'),
            new StopCheckpoint('id-6', false),
            new StopCheckpoint('id-7', false),
        ])->create();

        $schedule->run(fn($schedule) => null);

        $this->assertTrue(
            $schedule->expectant()->isMatchWith(new Expectant($activity->name, new DateTimeImmutable()))
        );
    }

    public function test_it_removes_next_activity()
    {
        $schedule = ScheduleFactory::checkpoints([
            new ActionCheckpoint('id-1', 'id-2', false, 'foo'),
            new ActionCheckpoint('id-2', 'id-3', false, 'foo'),
            new IdleCheckpoint('id-3', 'id-4', false, new IdleTime(120)),
            new EventOccurrenceCheckpoint(
                'id-4',
                'id-5',
                'id-6',
                false,
                new Activity(uniqid(), new ActivityName('foo-activity'), new DateTimeImmutable())
            ),
            new ActionCheckpoint('id-5', 'id-7', false, 'foo'),
            new StopCheckpoint('id-6', false),
            new StopCheckpoint('id-7', false),
        ])->create();

        $schedule->run(fn($schedule) => null);
        $schedule->run(fn($schedule) => null);

        $this->assertNull($schedule->expectant());
    }

    public function test_stop_state()
    {
        $schedule = ScheduleFactory::checkpoints([
            new ActionCheckpoint('id-1', 'id-2', false, 'foo'),
            new ActionCheckpoint('id-2', 'id-3', false, 'foo'),
            new StopCheckpoint('id-3', false),
        ])->create();

        $schedule->run(fn($schedule) => null);

        $this->assertEventPublishedAtLast(StopCheckpointReached::class, $schedule->pullRecordedEvents());

        $this->assertTrue($schedule->status()->is(Status::close()));

        $this->expectException(DomainError::class);

        $schedule->run(fn($schedule) => null);
    }

    public function test_reachable_checker_state_where_condition_met()
    {
        $schedule = ScheduleFactory::checkpoints([
            new ActionCheckpoint('id-1', 'id-2', false, 'foo'),
            new ActionCheckpoint('id-2', 'id-3', false, 'bar'),
            new CheckReachabilityCheckpoint(
                'id-3',
                'id-4',
                'id-5',
                false,
                'sms'
            ),
            new StopCheckpoint('id-4', false),
            new StopCheckpoint('id-5', false),
        ])
            ->targetTo(new Target(1, ['sms']))
            ->create();

        $schedule->run(fn($schedule) => null);


        $this->assertEventPublishedAtLast(StopCheckpointReached::class, $event = $schedule->pullRecordedEvents());
        /** @var StopCheckpointReached */
        $latest = end($event);
        $this->assertSame('id-4', $latest->checkpointId);
        $this->assertTrue($schedule->status()->is(Status::close()));
    }

    public function test_reachable_checker_state_where_condition_does_not_met()
    {
        $schedule = ScheduleFactory::checkpoints([
            new ActionCheckpoint('id-1', 'id-2', false, 'foo'),
            new ActionCheckpoint('id-2', 'id-3', false, 'bar'),
            new CheckReachabilityCheckpoint(
                'id-3',
                'id-4',
                'id-5',
                false,
                'sms'
            ),
            new StopCheckpoint('id-4', false),
            new StopCheckpoint('id-5', false),
        ])
            ->targetTo(new Target(1, ['email', 'push']))
            ->create();

        $schedule->run(fn($schedule) => null);

        $this->assertEventPublishedAtLast(StopCheckpointReached::class, $events = $schedule->pullRecordedEvents());
        /** @var StopCheckpointReached */
        $latest = end($events);
        $this->assertSame('id-5', $latest->checkpointId);
        $this->assertTrue($schedule->status()->is(Status::close()));
    }

    public function test_event_occurrence_checker_state_where_condition_met()
    {
        $checkpoints = [
            new ActionCheckpoint('id-1', 'id-2', false, 'foo'),
            new ActionCheckpoint('id-2', 'id-3', false, 'foo'),
            new EventOccurrenceCheckpoint(
                'id-3',
                'id-4',
                'id-5',
                false,
                $activity = new Activity(uniqid(), new ActivityName('foo-activity'), new DateTimeImmutable())
            ),
            new StopCheckpoint('id-4', false),
            new StopCheckpoint('id-5', false),
        ];

        $schedule = ScheduleFactory::checkpoints($checkpoints)
            ->setActivities(new Activities($activity))
            ->create();

        $schedule->run(fn($schedule) => null);

        $this->assertEventPublishedAtLast(StopCheckpointReached::class, $events = $schedule->pullRecordedEvents());
        /** @var StopCheckpointReached */
        $latest = end($events);
        $this->assertSame('id-4', $latest->checkpointId);

        $this->assertTrue($schedule->status()->is(Status::close()));
    }

    public function test_event_occurrence_checker_state_where_condition_does_not_met()
    {
        $schedule = ScheduleFactory::checkpoints([
            new ActionCheckpoint('id-1', 'id-2', false, 'foo'),
            new ActionCheckpoint('id-2', 'id-3', false, 'foo'),
            new EventOccurrenceCheckpoint(
                'id-3',
                'id-4',
                'id-5',
                false,
                new Activity(uniqid(), new ActivityName('foo-activity'), new DateTimeImmutable())
            ),
            new StopCheckpoint('id-4', false),
            new StopCheckpoint('id-5', false),
        ])
            ->setActivities(new Activities(new Activity(uniqid(), new ActivityName('bar-activity'), new DateTimeImmutable())))
            ->create();

        $schedule->run(fn($schedule) => null);

        $this->assertEventPublishedAtLast(StopCheckpointReached::class, $events = $schedule->pullRecordedEvents());
        /** @var StopCheckpointReached */
        $latest = end($events);
        $this->assertSame('id-5', $latest->checkpointId);
        $this->assertTrue($schedule->status()->is(Status::close()));
    }

    public function assertEventPublishedAtLast(string $event, array $publishedEvents)
    {
        $this->assertInstanceOf($event, end($publishedEvents));
    }
}
