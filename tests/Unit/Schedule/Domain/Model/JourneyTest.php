<?php

declare(strict_types=1);

namespace Tests\Unit\Schedule\Domain\Model;

use Tests\Support\TestCase;
use App\Shared\Domain\DomainError;
use App\Schedule\Domain\Model\Journey;
use App\Schedule\Domain\Model\Checkpoint\StopCheckpoint;
use App\Schedule\Domain\Model\Checkpoint\ActionCheckpoint;

class JourneyTest extends TestCase
{
    private Journey $journey;

    protected function setUp(): void
    {
        $this->journey = new Journey('foo', 'foo_name', 'id-1', 'id-1', 'trigger', [
            new ActionCheckpoint('id-1', 'id-2', false, 'foo'),
            new ActionCheckpoint('id-2', 'id-3', false, 'fo'),
            new StopCheckpoint('id-3', false),
        ]);
    }

    public function test_go_to()
    {
        $current = $this->journey->current();
        $this->journey->goTo('id-2');
        $this->assertTrue($this->journey->findById($current->id())->isChecked());
        $this->assertSame($this->journey->current()->id(), 'id-2');
    }

    public function test_go_to_non_existing_checkpoint()
    {
        $this->expectException(DomainError::class);
        $this->journey->goTo('foo');
    }
}
