<?php

declare(strict_types=1);

namespace Tests\Unit\User\Domain\ValueObject;

use ValueError;
use App\User\Domain\ValueObject\Name;
use Tests\Support\TestCase;;

class NameTest extends TestCase
{
    public function test_it_breaks_with_small_first_name()
    {
        $this->expectException(ValueError::class);

        new Name('a', 'doe');
    }

    public function test_it_breaks_with_small_last_name()
    {
        $this->expectException(ValueError::class);

        new Name('john', 'b');
    }

    public function test_full_name()
    {
        $name = new Name('John', 'Doe');

        $this->assertSame('John Doe', $name->fullName());
    }
}
