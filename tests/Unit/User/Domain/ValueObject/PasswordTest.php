<?php

declare(strict_types=1);

namespace Tests\Unit\User\Domain\ValueObject;

use App\Shared\Domain\ValueObject\ValueError;
use App\User\Domain\ValueObject\Password;
use Tests\Support\TestCase;;

class PasswordTest extends TestCase
{
    public function test_should_be_at_least_8_char()
    {
        $this->expectException(ValueError::class);
        new Password('a');
    }
}
