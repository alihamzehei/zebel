<?php

declare(strict_types=1);

namespace Tests\Unit\User\Domain\ValueObject;

use App\Shared\Domain\ValueObject\ValueError;
use App\User\Domain\ValueObject\Email;
use Tests\Support\TestCase;;

class EmailTest extends TestCase
{
    public function test_it_validates_email()
    {
        $this->expectException(ValueError::class);

        new Email('invalid_mail');
    }
}
