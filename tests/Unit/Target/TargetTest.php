<?php

declare(strict_types=1);

namespace Tests\Unit\Target;

use Tests\Support\TestCase;
use App\Target\Domain\Model\Target;
use App\Target\Domain\Event\TargetRegistered;

class TargetTest extends TestCase
{
    public function test_register_event()
    {
        $target = Target::register(1, 'John Doe', 'foo@bar.com', '+989121234567');
        $this->assertInstanceOf(TargetRegistered::class, $target->pullRecordedEvents()[0]);
    }
}
