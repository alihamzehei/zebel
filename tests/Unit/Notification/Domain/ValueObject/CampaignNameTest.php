<?php

declare(strict_types=1);

namespace Tests\Unit\Notification\Domain\ValueObject;

use App\Notification\Domain\ValueObject\CampaignName;
use App\Shared\Domain\ValueObject\ValueError;
use Tests\Support\TestCase;

class CampaignNameTest extends TestCase
{
    public function test_can_not_be_empty()
    {
        $this->expectException(ValueError::class);
        $this->expectExceptionMessage('Campaign name cant be empty');

        new CampaignName('');
    }

    public function test_should_be_at_least_2_char()
    {
        $this->expectException(ValueError::class);
        $this->expectExceptionMessage('Campaign name cant be smaller than '.CampaignName::MIN_LENGTH.' characters');

        new CampaignName('a');
    }

    public function test_should_not_be_greater_than_190_char()
    {
        $this->expectException(ValueError::class);
        $this->expectExceptionMessage('Campaign name cant be greater than '.CampaignName::MAX_LENGTH.' characters');

        new CampaignName(str_repeat('a', 190));
    }
}
