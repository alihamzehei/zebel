<?php

declare(strict_types=1);

namespace Tests\Unit\Notification\Domain\ValueObject;

use App\Notification\Domain\ValueObject\SmsMessage;
use App\Shared\Domain\ValueObject\ValueError;
use Exception;
use Tests\Support\TestCase;

class SmsMessageTest extends TestCase
{
    public function test_can_not_be_empty()
    {
        $this->expectException(ValueError::class);
        $this->expectExceptionMessage('Message cant be empty');

        new SmsMessage('');
    }

    public function test_should_be_at_least_2_char()
    {
        $this->expectException(ValueError::class);
        $this->expectExceptionMessage('Message cant be smaller than '.SmsMessage::MIN_LENGTH.' characters');

        new SmsMessage('ae');
    }

    public function test_should_not_be_more_than_160_char()
    {
        $this->expectException(ValueError::class);
        $this->expectExceptionMessage('Message cant be greater than '.SmsMessage::MAX_LENGTH.' characters');

        new SmsMessage(str_repeat('a', 160));
    }
}
