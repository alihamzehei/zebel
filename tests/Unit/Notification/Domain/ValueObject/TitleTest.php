<?php

declare(strict_types=1);

namespace Tests\Unit\Notification\Domain\ValueObject;

use App\Notification\Domain\ValueObject\Title;
use App\Shared\Domain\ValueObject\ValueError;
use Tests\Support\TestCase;

class TitleTest extends TestCase
{
    public function test_title_cant_be_empty_string()
    {
        $this->expectException(ValueError::class);

        $this->expectExceptionMessage('The title cant be empty');

        new Title('');
    }

    public function test_titles_length_cant_be_smaller_than_the_min_length()
    {
        $this->expectException(ValueError::class);

        $this->expectExceptionMessage('The title cant be smaller than '.Title::MIN_LENGTH);

        new Title('aa');
    }

    public function test_titles_length_cant_be_greater_than_the_max_length()
    {
        $this->expectException(ValueError::class);

        $this->expectExceptionMessage('The title cant be greater than '.Title::MAX_LENGTH);

        new Title(str_repeat('hello', 38));
    }
}
