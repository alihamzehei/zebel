<?php

declare(strict_types=1);

namespace Tests\Unit\Notification\Domain\ValueObject;

use App\Notification\Domain\ValueObject\PushMessage;
use App\Shared\Domain\ValueObject\ValueError;
use Tests\Support\TestCase;

class PushMessageTest extends TestCase
{
    public function test_can_not_be_empty()
    {
        $this->expectException(ValueError::class);
        $this->expectExceptionMessage('Message cant be empty');

        new PushMessage('');
    }

    public function test_should_be_at_least_2_char()
    {
        $this->expectException(ValueError::class);
        $this->expectExceptionMessage('Message cant be smaller than '.PushMessage::MIN_LENGTH.' characters');

        new PushMessage('a');
    }

    public function test_should_not_be_greater_than_190_char()
    {
        $this->expectException(ValueError::class);
        $this->expectExceptionMessage('Message cant be greater than '.PushMessage::MAX_LENGTH.' characters');

        new PushMessage(str_repeat('a', 150));
    }
}
