<?php

declare(strict_types=1);

namespace Tests\Unit\Notification\Domain\ValueObject;

use App\Notification\Domain\ValueObject\EmailMessage;
use App\Shared\Domain\ValueObject\ValueError;
use Tests\Support\TestCase;

class EmailMessageTest extends TestCase
{
    public function test_can_not_be_empty()
    {
        $this->expectException(ValueError::class);
        $this->expectExceptionMessage('Message cant be empty');

        new EmailMessage('');
    }

    public function test_should_be_at_least_2_char()
    {
        $this->expectException(ValueError::class);
        $this->expectExceptionMessage('Message cant be smaller than '.EmailMessage::MIN_LENGTH.' characters');

        new EmailMessage('ae');
    }
}
