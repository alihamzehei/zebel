<?php

declare(strict_types=1);

namespace Tests\Support\Trait;

trait InteractsWithAuth
{
    protected function registerUser(array $credentials = []): array
    {
        $this->post('/auth/register', $payload = [
            'firstname' => $credentials['firstname'] ?? 'John',
            'lastname' => $credentials['lastname'] ?? 'Doe',
            'email' => $credentials['email'] ?? 'john@doe.com',
            'password' => $credentials['password'] ?? 'secret$$password#',
        ]);

        return $payload;
    }

    protected function getUserToken(string $email, string $password): string
    {
        return $this->postJson('/auth/login', compact('email', 'password'))['token'];
    }

    protected function signIn(array $credentials = []): string
    {
        $user = $this->registerUser($credentials);

        return $this->getUserToken($user['email'], $user['password']);
    }
}
