<?php

declare(strict_types=1);

namespace Tests\Support\Trait;

use App\Shared\Domain\AggregateRoot;
use App\Shared\Domain\DomainEvent;

trait WithEvents
{
    /** @param class-string<DomainEvent> $eventClass */
    protected function assertEventHasBeenPublished(AggregateRoot $aggregateRoot, string $eventClass): void
    {
        foreach ($aggregateRoot->pullRecordedEvents() as $event) {
            if ($event instanceof $eventClass) {
                $this->assertInstanceOf($eventClass, $event);

                return;
            }
        }

        $this->fail("Failed to assert event {$eventClass} has been published");
    }
}
