<?php

namespace Tests\Support\Trait;

trait Snapshot
{
    protected function snap(string $path): bool|string
    {
        return file_get_contents(\dirname(__DIR__, 2).'/Support/snapshots/'.$path);
    }

    protected function jsonSnap(string $path, string $extension = '.json'): array
    {
        return json_decode($this->snap($path.$extension), true);
    }
}
