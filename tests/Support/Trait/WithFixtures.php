<?php

declare(strict_types=1);

namespace Tests\Support\Trait;

trait WithFixtures
{
    /** @return array<mixed> */
    public function arrayFixture(string $path): array
    {
        $context = explode('\\', static::class)[1];

        $root = \dirname(__DIR__, 4);
        $filename = "{$root}/app/{$context}/Tests/fixtures/{$path}";

        return json_decode(file_get_contents($filename), true);
    }
}
