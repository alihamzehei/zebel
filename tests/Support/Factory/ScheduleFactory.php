<?php

declare(strict_types=1);

namespace Tests\Support\Factory;

use DateTimeImmutable;
use Ramsey\Uuid\Uuid;
use App\Schedule\Domain\Model\Journey;
use App\Schedule\Domain\Model\Schedule;
use App\Schedule\Domain\Model\Target;
use App\Schedule\Domain\ValueObject\Activities;
use App\Schedule\Domain\ValueObject\Expectant;
use App\Schedule\Domain\ValueObject\ScheduleId;
use App\Schedule\Domain\ValueObject\Status;

class ScheduleFactory
{
    public ScheduleId $id;

    private Target $target;

    private Journey $journey;

    private ?Status $status = null;

    private ?Activities $activities = null;

    private ?Expectant $expectant = null;

    private ?DateTimeImmutable $idleUntil = null;

    public function create(): Schedule
    {
        return Schedule::initialize(
            $this->id(), $this->target(), $this->journey(),
            $this->status(), $this->activities(), $this->idleUntil, $this->expectant
        );
    }

    private function id(): ScheduleId
    {
        return $this->id ?? new ScheduleId('foo-'.(string) mt_rand(1, 1000));
    }

    public function setId(ScheduleId $id): self
    {
        $this->id = $id;

        return $this;
    }

    private function status(): Status
    {
        return $this->status ?? Status::init();
    }

    private function target(): Target
    {
        return $this->target ?? new Target(1, []);
    }

    public function targetTo(Target $target): self
    {
        $this->target = $target;

        return $this;
    }

    public function setExpectant(Expectant $expectant): self
    {
        $this->expectant = $expectant;

        return $this;
    }

    public function idleUntil(DateTimeImmutable $until): self
    {
        $this->idleUntil = $until;

        return $this;
    }

    private function journey(): Journey
    {
        return $this->journey ?? new Journey('foo', 'foo_name', 'bar', 'bar', 'trigger', []);
    }

    private function activities(): ?Activities
    {
        return $this->activities;
    }

    public function setActivities(Activities $activities): self
    {
        $this->activities = $activities;

        return $this;
    }

    public static function checkpoints(array $checkpoints): self
    {
        $factory = new self();
        $uniqueIdForJourney = Uuid::uuid1()->toString();
        $factory->journey = new Journey($uniqueIdForJourney, 'foo_name', $checkpoints[0]->id(), $checkpoints[0]->id(), 'trigger', $checkpoints);

        return $factory;
    }

    public static function fromStatus(Status $status): self
    {
        $factory = new self();
        $factory->status = $status;

        return $factory;
    }

    public function setStatus(Status $status): self
    {
        $this->status = $status;

        return $this;
    }
}
