<?php

declare(strict_types=1);

namespace Tests\Support;

use App\Shared\Domain\Model\IdGenerator;

class RandomIdGenerator implements IdGenerator
{
    public function generate(): string
    {
        return (string) mt_rand(1, 10000);
    }
}
