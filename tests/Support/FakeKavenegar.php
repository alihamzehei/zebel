<?php

declare(strict_types=1);

namespace App\Shared\Tests\Support;

use Illuminate\Support\LazyCollection;
use Kavenegar\KavenegarApi;
use PHPUnit\Framework\Assert as PHPUnit;
use stdClass;

class FakeKavenegar extends KavenegarApi
{
    private array $messages = [];

    public function __construct()
    {
    }

    public function getMessages()
    {
        return $this->messages;
    }

    public function Send($sender, $receptor, $message, $date = null, $type = null, $localid = null)
    {
        return $this->respond('Send', \func_get_args());
    }

    public function SendArray($sender, $receptor, $message, $date = null, $type = null, $localmessageid = null)
    {
        return $this->respond('SendArray', \func_get_args());
    }

    public function Status($messageid)
    {
        return $this->respond('Status', \func_get_args());
    }

    public function StatusLocalMessageId($localid)
    {
        return $this->respond('StatusLocalMessageId', \func_get_args());
    }

    public function Select($messageid)
    {
        return $this->respond('Select', \func_get_args());
    }

    public function SelectOutbox($startdate, $enddate, $sender)
    {
        return $this->respond('SelectOutbox', \func_get_args());
    }

    public function LatestOutbox($pagesize, $sender)
    {
        return $this->respond('LatestOutbox', \func_get_args());
    }

    public function CountOutbox($startdate, $enddate, $status = 0)
    {
        return $this->respond('CountOutbox', \func_get_args());
    }

    public function Cancel($messageid)
    {
        return $this->respond('Cancel', \func_get_args());
    }

    public function Receive($linenumber, $isread = 0)
    {
        return $this->respond('Receive', \func_get_args());
    }

    public function CountInbox($startdate, $enddate, $linenumber, $isread = 0)
    {
        return $this->respond('CountInbox', \func_get_args());
    }

    public function CountPostalcode($postalcode)
    {
        return $this->respond('CountPostalcode', \func_get_args());
    }

    public function SendbyPostalcode($sender, $postalcode, $message, $mcistartindex, $mcicount, $mtnstartindex, $mtncount, $date)
    {
        return $this->respond('SendbyPostalcode', \func_get_args());
    }

    public function AccountInfo()
    {
        return $this->respond('AccountInfo', \func_get_args());
    }

    public function AccountConfig($apilogs, $dailyreport, $debug, $defaultsender, $mincreditalarm, $resendfailed)
    {
        return $this->respond('AccountConfig', \func_get_args());
    }

    public function VerifyLookup($receptor, $token, $token2, $token3, $template, $type = null)
    {
        return $this->respond('VerifyLookup', \func_get_args());
    }

    public function CallMakeTTS($receptor, $message, $date = null, $localid = null)
    {
        return $this->respond('CallMakeTTS', \func_get_args());
    }

    private function respond($method, $args)
    {
        $this->messages[] = [
            ...$args,
            'method' => $method,
        ];

        $response = tap(new stdClass(), function ($result) {
            $result->messageid = random_int((int) 1e5, (int) 1e6 - 1);
        });

        return [$response];
    }

    public function assertSend(
        $sender = null,
        $receptor = null,
        $message = null,
        $date = null,
        $type = null,
        $localid = null,
        $count = 1,
    ) {
        $messages = LazyCollection::make($this->messages)
            ->filter(fn ($msg) => $msg['method'] === 'Send')
            ->when($sender, fn ($coll, $s) => $coll->where('0', $s))
            ->when($receptor, fn ($coll, $r) => $coll->where('1', $r))
            ->when($message, fn ($coll, $m) => $coll->where('2', $m))
            ->when($date, fn ($coll, $d) => $coll->where('3', $d))
            ->when($type, fn ($coll, $t) => $coll->where('4', $t))
            ->when($localid, fn ($coll, $id) => $coll->where('5', $id))
            ->all();
        PHPUnit::assertCount($count, $messages);
    }

    public function assertVerifyLookup(
        $receptor = null,
        $token = null,
        $token2 = null,
        $token3 = null,
        $template = null,
        $type = null,
        $count = 1,
    ) {
        $messages = LazyCollection::make($this->messages)
            ->filter(fn ($msg) => $msg['method'] === 'VerifyLookup')
            ->when($receptor, fn ($coll, $r) => $coll->where('0', $r))
            ->when($token, fn ($coll, $t) => $coll->where('1', $t))
            ->when($token2, fn ($coll, $t) => $coll->where('2', $t))
            ->when($token3, fn ($coll, $t) => $coll->where('3', $t))
            ->when($template, fn ($coll, $t) => $coll->where('4', $t))
            ->when($type, fn ($coll, $t) => $coll->where('5', $t))
            ->all();
        PHPUnit::assertCount($count, $messages);
    }
}
