<?php

declare(strict_types=1);

namespace Tests\E2E\Plan;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Support\TestCase;;
use Tests\Support\Trait\Snapshot;

class GetJourneyTest extends TestCase
{
    use DatabaseMigrations;
    use Snapshot;

    public function test_it_responds_for_planning()
    {
        $payload = $this->jsonSnap('journey');

        $id = $this->postJson('/journeys', $payload)
            ->json('id');
        
        // todo: assert the json response in a more restrict way.
        $this->getJson("/journeys/{$id}")
            ->assertOk()
            ->assertJson([
                'name' => $payload['name'],
                'trigger' => $payload['trigger'],
                'meta' => $payload['meta'],
            ]);
    }
}
