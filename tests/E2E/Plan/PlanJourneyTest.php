<?php

declare(strict_types=1);

namespace Tests\E2E\Plan;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Support\TestCase;;
use Tests\Support\Trait\Snapshot;

class PlanJourneyTest extends TestCase
{
    use DatabaseMigrations;
    use Snapshot;

    public function test_it_responds_for_planning()
    {
        $payload = $this->jsonSnap('journey');

        $this->postJson('/journeys', $payload);
        $this->getJson("/journeys")
            ->assertOk()
            ->assertJsonCount(1, 'data');
    }
}
