<?php

declare(strict_types=1);

namespace Tests\E2E\Journey;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Support\TestCase;;
use Tests\Support\Trait\Snapshot;


class ToggleJourneyTest extends TestCase
{
    use DatabaseMigrations;
    use Snapshot;

    public function test_it_responds_for_toggle()
    {
        $payload = $this->jsonSnap('journey');

        $id = $this->postJson('/journeys', $payload)->json('id');


        $this->postJson("/journeys/{$id}/toggle")
            ->assertNoContent();
    }
}
