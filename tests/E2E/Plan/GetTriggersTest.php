<?php

namespace Tests\E2E\Plan;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Support\TestCase;

class GetTriggersTest extends TestCase
{
    use DatabaseMigrations; 

    public function test_get_triggers()
    {
        $this->getJson('triggers')
            ->assertOk()
            ->assertJsonStructure([
                'items' => [
                    '*' => ['name', 'value']
                ]
            ]);
    }
}
