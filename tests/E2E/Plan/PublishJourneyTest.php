<?php

declare(strict_types=1);

namespace Tests\E2E\Journey;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Support\TestCase;;
use Tests\Support\Trait\Snapshot;

class PublishJourneyTest extends TestCase
{
    use DatabaseMigrations;
    use Snapshot;

    public function test_it_responds_for_publishing()
    {
        $payload = $this->jsonSnap('journey');

        $id = $this->postJson('/journeys', $payload)->json('id');


        $this->postJson("/journeys/{$id}/publish")
            ->assertNoContent();
    }
}
