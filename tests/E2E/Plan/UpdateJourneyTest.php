<?php

namespace Tests\E2E\Plan;

use Tests\Support\TestCase;;
use Tests\Support\Trait\Snapshot;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class UpdateJourneyTest extends TestCase
{
    use DatabaseMigrations;
    use Snapshot;

    public function test()    
    {
        $payload = $this->jsonSnap('journey');
        $payload2 = $this->jsonSnap('otherJourney');

        $id = $this->postJson('/journeys', [
            'name' => 'my awesome journey',
            'root' => $payload['root'],
            'trigger' => $payload['trigger'],
            'meta' => $payload['meta'],
            'checkpoints' => $payload['checkpoints'],
        ])->json('id');


        $this->postJson("/journeys/{$id}", [
            'name' => $payload2['name'],
            'root' => $payload2['root'],
            'trigger' => $payload2['trigger'],
            'meta' => $payload2['meta'],
            'checkpoints' => $payload2['checkpoints'],
        ])->assertNoContent();
    }
}
