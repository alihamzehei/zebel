<?php

declare(strict_types=1);

namespace Tests\E2E\Analytic;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\DB;
use Tests\Support\TestCase;

class CheckpointUserCountTest extends TestCase
{
    use DatabaseMigrations;

    public function test_count_of_all_users_interacted_with_specific_checkpoint()
    {
        DB::table('analytic_checkpoints')->insert([
            'schedule_id' => 1,
            'journey_id' => 'journey-1',
            'checkpoint_id' => 'checkpoint-1',
            'type' => 'type',
            'occurred_at' => now()
        ]);

        DB::table('analytic_checkpoints')->insert([
            'schedule_id' => 1,
            'journey_id' => 'journey-2',
            'checkpoint_id' => 'checkpoint-2',
            'type' => 'type',
            'occurred_at' => now()
        ]);

        DB::table('analytic_checkpoints')->insert([
            'schedule_id' => 2,
            'journey_id' => 'journey-1',
            'checkpoint_id' => 'checkpoint-3',
            'type' => 'type',
            'occurred_at' => now()
        ]);

        DB::table('analytic_checkpoints')->insert([
            'schedule_id' => 2,
            'journey_id' => 'journey-1',
            'checkpoint_id' => 'checkpoint-3',
            'type' => 'type',
            'occurred_at' => now()
        ]);

        $this->getJson('/analytics/journey/journey-1/checkpoints/user')
            ->assertOk()
            ->assertJson([
                ['checkpoint' => 'checkpoint-1', 'count' => 1],
                ['checkpoint' => 'checkpoint-3', 'count' => 2],
            ]);
    }
}
