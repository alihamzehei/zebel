<?php

declare(strict_types=1);

namespace Tests\E2E\Analytic;


use App\Analytic\Application\Listener\PersistScheduleListener;
use App\Schedule\Domain\Event\ScheduleInitialized;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Support\TestCase;

class PersistCheckpointListener extends TestCase
{
    use RefreshDatabase;
    protected function setUp(): void
    {
        parent::setUp();
        $event = new ScheduleInitialized('schedule-id', 123, 'journey-id');

        event($event);
    }
}
