<?php

declare(strict_types=1);

namespace Tests\E2E\Analytic;

use App\Schedule\Domain\Event\CheckReachabilityCheckpointReached;

class PersistCheckReachabilityCheckpointListenerTest extends PersistCheckpointListener
{
    public function test_reachability_checkpoint_has_been_persisted()
    {
        $event = new CheckReachabilityCheckpointReached('schedule-id', 'checkpoint-id', 'sms');

        event($event);

        $this->assertDatabaseCount('analytic_checkpoints',1);
    }

}
