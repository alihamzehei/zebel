<?php

declare(strict_types=1);

namespace Tests\E2E\Analytic;

use DateTime;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\DB;
use Tests\Support\TestCase;

class JourneyConversionTest extends TestCase
{
    use DatabaseMigrations;

    protected function setUp(): void
    {
        parent::setUp();

        DB::table('analytic_schedules')->insert([
            'schedule_id' => 'schedule-1',
            'journey_id' => 'journey-1',
            'target_id' => 1,
            'active' => 1,
            'expectant' => 'some-event',
            'occurred_at' => now(),
        ]);

        DB::table('analytic_schedules')->insert([
            'schedule_id' => 'schedule-2',
            'journey_id' => 'journey-2',
            'target_id' => 2,
            'active' => 1,
            'expectant' => 'some-event',
            'occurred_at' => now()->addMinute(),
        ]);

        DB::table('analytic_schedules')->insert([
            'schedule_id' => 'schedule-3',
            'journey_id' => 'journey-1',
            'target_id' => 3,
            'active' => 1,
            'expectant' => 'some-event',
            'occurred_at' => now()->addMinutes(2),
        ]);

        DB::table('analytic_checkpoints')->insert([
            'schedule_id' => 1,
            'journey_id' => 'journey-1',
            'checkpoint_id' => 'checkpoint-1',
            'type' => 'type',
            'occurred_at' => now(),
        ]);

        DB::table('analytic_activities')->insert([
            'event_id' => 'event-id',
            'schedule_id' => 1,
            'title' => 'CartCreated',
            'occurred_at' => now()->addMinutes(9),
        ]);

        DB::table('analytic_activities')->insert([
            'event_id' => 'event-id-2',
            'schedule_id' => 1,
            'title' => 'CartRemoved',
            'occurred_at' => now()->addMinutes(10),
        ]);

        DB::table('analytic_activities')->insert([
            'event_id' => 'event-id-3',
            'schedule_id' => 1,
            'title' => 'CartCreated',
            'occurred_at' => now()->addMinutes(11),
        ]);
    }

    public function test_journey_conversion_rate()
    {
        $this->json('GET', '/analytics/journey/journey-1/conversion', [
            'event' => 'CartCreated',
            'fromDate' => (new DateTime('120 minutes ago'))->format('U'),
            'toDate' => (new DateTime('+120 minutes'))->format('U'),
        ])->assertOk()
            ->assertJson([
                'rate' => 100,
            ]);


//        $this->json('GET', '/analytics/journey/journey-1/conversion', [
//            'event' => 'CartCreated',
//            'deadline' => 60 * 12,
//            'fromDate' => (new DateTime('120 minutes ago'))->format('U'),
//            'toDate' => (new DateTime('+120 minutes'))->format('U'),
//        ])->assertOk()
//            ->assertJson(['rate' => 0]);
//
//        $this->json('GET', '/analytics/journey/journey-1/conversion', [
//            'event' => 'CartCreated',
//            'deadline' => 60 * 10,
//            'fromDate' => (new DateTime('120 minutes ago'))->format('U'),
//            'toDate' => (new DateTime('+120 minutes'))->format('U'),
//        ])
//            ->assertOk()
//            ->assertJson(['rate' => 50]);
    }
}
