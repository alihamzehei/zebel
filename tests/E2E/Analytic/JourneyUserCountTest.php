<?php

declare(strict_types=1);

namespace Tests\E2E\Analytic;

use DateTime;
use Tests\Support\TestCase;

class JourneyUserCountTest extends TestCase
{
//    public function test_count_of_all_users_interacted_with_specific_journey()
//    {
//        $this->connection()->executeStatement(
//            'insert into analytic_schedules (`schedule_id`, `journey_id`, `target_id`, `active`, `expectant`, `occurred_at`) values (?, ?, ?, ?, ?, ?)',
//            ['s1', 'j1', 1, 1, 'e', $this->dateTimeForPersist($dt = new DateTime())]
//        );
//
//        $this->connection()->executeStatement(
//            'insert into analytic_schedules (`schedule_id`, `journey_id`, `target_id`, `active`, `expectant`, `occurred_at`) values (?, ?, ?, ?, ?, ?)',
//            ['s2', 'j2', 2, 1, 'e', $this->dateTimeForPersist($dt)]
//        );
//
//        $this->connection()->executeStatement(
//            'insert into analytic_schedules (`schedule_id`, `journey_id`, `target_id`, `active`, `expectant`, `occurred_at`) values (?, ?, ?, ?, ?, ?)',
//            ['s3', 'j1', 3, 1, 'e', $this->dateTimeForPersist($dt)]
//        );
//
//        $this->connection()->executeStatement(
//            'insert into analytic_schedules (`schedule_id`, `journey_id`, `target_id`, `active`, `expectant`, `occurred_at`) values (?, ?, ?, ?, ?, ?)',
//            ['s4', 'j1', 3, 0, 'e', $this->dateTimeForPersist($dt)]
//        );
//
//        $actives = $this->getJson(
//            '/analytics/journey/j1/user?now=1', headers: ['authorization' => 'Bearer '.$this->singIn()]
//        );
//
//        $this->assertSame(2, $actives['count']);
//
//        $all = $this->getJson(
//            '/analytics/journey/j1/user?now=0', headers: ['authorization' => 'Bearer '.$this->singIn()]
//        );
//
//        $this->assertSame(3, $all['count']);
//    }
}
