<?php

declare(strict_types=1);

namespace Tests\E2E\Analytic;

use App\Collect\Domain\Event\ActivityCollected;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\DB;
use Tests\Support\TestCase;

class ActivityCollectedListenerTest extends TestCase
{
    use DatabaseMigrations;

    public function test_persist_activity_when_a_related_schedule_exists()
    {
        DB::table('analytic_schedules')->insert([
            'schedule_id' => 'schedule-id',
            'journey_id' => 'journey-id',
            'target_id' => 1,
            'active' => 1,
            'expectant' => 'some-event',
            'occurred_at' => now(),
        ]);

        event($event = new ActivityCollected('foo', 'some-event', 1, []));

        $this->assertDatabaseHas('analytic_activities', [
            'event_id' => $event->id,
            'occurred_at' => $event->occurredAt(),
        ]);
    }
}
