<?php

declare(strict_types=1);

namespace Tests\E2E\Analytic;


use App\Schedule\Domain\Event\ActionCheckpointReached;

class PersistActionCheckpointListenerTest extends PersistCheckpointListener
{
    public function test_action_checkpoint_has_been_persisted()
    {
        $event = new ActionCheckpointReached('schedule-id', 'checkpoint-id', 'notification-id', 123);

        event($event);

        $this->assertDatabaseCount('analytic_checkpoints' , 1);
        $this->assertDatabaseHas('analytic_checkpoints' , [
            'checkpoint_id' => 'checkpoint-id'
        ]);
    }

}
