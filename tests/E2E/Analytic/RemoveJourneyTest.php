<?php

declare(strict_types=1);

namespace Test\E2E\Analytic;

use Test\Support\TestCase\E2ETest;
use Tests\Support\TestCase;

class RemoveJourneyTest extends TestCase
{
//    protected function setUp(): void
//    {
//        parent::setUp();
//        $this->token = $this->singIn();
//        $this->dbal = Zebel::dbal();
//    }
//
//    public function test_remove_analytic_when_journey_was_removed()
//    {
//        $payload = $this->jsonSnap('journey');
//        $response = $this->post('/journeys', [
//            'name' => 'journey1',
//            'root' => $payload['root'],
//            'trigger' => $payload['trigger'],
//            'meta' => $payload['meta'],
//            'checkpoints' => $payload['checkpoints'],
//        ], ['authorization' => 'Bearer '.$this->token]);
//
//        $journeyId = $this->jsonify($response)['id'];
//
//        $event = new ScheduleInitialized('schedule-id', 123, $journeyId);
//
//        \call_user_func(
//            Zebel::get(PersistScheduleListener::class),
//            $event
//        );
//
//        $journeyRepository = Zebel::get(JourneyRepository::class);
//        $journeyRepository->deleteById(new JourneyId($journeyId));
//
//        $event = new JourneyRemoved($journeyId);
//
//        \call_user_func(
//            Zebel::get(RemoveJourneyListener::class),
//            $event
//        );
//        $this->assertCount(0, Zebel::dbal()->fetchAllAssociative('select * from analytic_schedules'));
//    }
}
