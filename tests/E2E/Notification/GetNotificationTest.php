<?php

declare(strict_types=1);

namespace Tests\E2E\Notification;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Support\TestCase;

class GetNotificationTest extends TestCase
{
    use DatabaseMigrations;

    public function test_get_notification_with_sms_channel()
    {
        $id = $this->post('/notifications', [
            'channel' => $payload = [
                'type' => 'sms',
                'event' => 'CartCreated',
                'campaignName' => 'yaldaaaa',
                'message' => 'hi mehdi, this is a test',
            ],
        ])->json('id');

        $this->getJson("/notifications/{$id}")
            ->assertOk()
            ->assertJson([
                'id' => $id,
                'type' => $payload['type'],
                'campaignName' => $payload['campaignName'],
                'message' => $payload['message'],
            ]);
    }

    public function test_get_notification_with_email_channel()
    {
        $id = $this->post('/notifications', [
            'channel' => $payload = [
                'type' => 'email',
                'event' => 'CartCreated',
                'campaignName' => 'yalda',
                'senderEmail' => 'malltina@yahoo.com',
                'senderName' => 'malltina',
                'message' => 'hi every body',
                'subject' => 'subject',
                'attachment' => null,
                'canReplied' => false,
            ],
        ])->json('id');

        $this->getJson("/notifications/{$id}")
            ->assertJson([
                'id' => $id,
                'type' => $payload['type'],
                'campaignName' => $payload['campaignName'],
                'senderEmail' => $payload['senderEmail'],
                'senderName' => $payload['senderName'],
                'message' => $payload['message'],
                'subject' => $payload['subject'],
                'attachment' => $payload['attachment'],
                'canReply' => $payload['canReplied']
            ]);
    }

    public function test__notification_with_web_push_channel()
    {
        $id = $this->postJson('/notifications', [
            'channel' => $payload = [
                'type' => 'webPush',
                'title' => 'some title',
                'pushMessage' => 'hi every body',
                'link' => 'some link',
                'campaignName' => 'yalda',
                'image' => null,
                'icon' => null,
            ],
        ])->json('id');

        $this->getJson("/notifications/{$id}")
            ->assertJson([
                'id' => $id,
                'type' => $payload['type'],
                'pushMessage' => $payload['pushMessage'],
                'link' => $payload['link'],
                'image' => $payload['image'],
                'icon' => $payload['icon'],
                'campaignName' => $payload['campaignName'],
                'title' => $payload['title'],
            ]);
    }
}
