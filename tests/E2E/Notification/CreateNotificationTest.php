<?php

declare(strict_types=1);

namespace Tests\E2E\Notification;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Support\TestCase;

class CreateNotificationTest extends TestCase
{
    use DatabaseMigrations;

    public function test_create_notification_with_sms_channel()
    {
        $this->postJson('/notifications', [
            'channel' => [
                'type' => 'sms',
                'event' => 'CartCreated',
                'campaignName' => 'yalda',
                'message' => 'hi mehdi, this is a test',
            ],
        ])
            ->assertCreated()
            ->assertJsonStructure(['id']);
    }

    public function test_create_notification_with_email_channel()
    {
        $this->postJson('/notifications', [
            'channel' => [
                'type' => 'email',
                'event' => 'CartCreated',
                'campaignName' => 'yalda',
                'senderEmail' => 'malltina@yahoo.com',
                'senderName' => 'malltina',
                'message' => 'hi every body',
                'subject' => 'subject',
                'attachment' => null,
                'canReplied' => false,
            ]
        ])
            ->assertCreated()
            ->assertJsonStructure(['id']);
    }

    public function test_create_notification_with_web_push_channel()
    {
        $this->postJson('/notifications', [
            'channel' => [
                'type' => 'webPush',
                'title' => 'some title',
                'pushMessage' => 'hi every body',
                'link' => 'some link',
                'campaignName' => 'yalda',
                'image' => null,
                'icon' => null,
            ],
        ])
            ->assertCreated()
            ->assertJsonStructure(['id']);
    }
}
