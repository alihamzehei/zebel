<?php

declare(strict_types=1);

namespace Tests\E2E\Notification;

use App\Plan\Domain\Model\Trigger;
use Tests\Support\TestCase;

class GetEventAttributesTest extends TestCase
{
    public function test_fetch_attributes_for_specific_event()
    {
        $response = $this->getJson('/notifications/eventAttributes?event='.Trigger::CartCreated->name);


        $response->assertJson([
            'name',
            'lastname',
            'email',
            'mobile',
            'cart_id',
            'product_id'
        ]);
    }
}
