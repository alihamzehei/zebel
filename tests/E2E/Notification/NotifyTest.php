<?php

declare(strict_types=1);

namespace Tests\E2E\Notification;

use App\Notification\Infrastructure\Model\NotificationModel;
use App\Notification\Domain\Model\EmailNotice;
use App\Notification\Domain\Model\SmsNotice;
use App\Notification\Domain\Repository\NotifyRepository;
use App\Schedule\Domain\Event\ActionCheckpointReached;
use App\Shared\Application\Command\CommandBus;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\DB;
use Tests\Support\TestCase;

class NotifyTest extends TestCase
{
    use DatabaseMigrations;

    private readonly NotifyRepository $repository;
    protected CommandBus $bus;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->app->get(NotifyRepository::class);
        $this->bus = $this->app->get(CommandBus::class);

    }

    private function persistNotifies(): void
    {
        $id = $this->postJson('/notifications', [
            'channel' => [
                'type' => 'sms',
                'event' => 'CartCreated',
                'campaignName' => 'yalda',
                'message' => 'hi mehdi, this is a test',
            ],
        ])->json('id');

        NotificationModel::query()->where('uuid', $id)->first();

        $this->postJson('/targets', [
            'id' => 1,
            'name' => 'mehdi',
            'email' => 'mehdi@yahoo.com',
            'mobile' => '091493434',
            'najva_token' => 'najva-token2',
        ]);

        $SmsNoticeOne = new SmsNotice(null, $id, 'checkpoint_id_1', 'schedule_id', 1, SmsNotice::TYPE, false);
        $SmsNoticeTwo = new SmsNotice(null, $id, 'checkpoint_id_2', 'schedule_id', 1, SmsNotice::TYPE, true);
        $EmailNoticeOne = new EmailNotice(null, $id, 'checkpoint_id_3', 'schedule_id', 1, EmailNotice::TYPE, false);
        $EmailNoticeTwo = new EmailNotice(null, $id, 'checkpoint_id_4', 'schedule_id', 1, EmailNotice::TYPE, false);

        $this->repository->persist($SmsNoticeOne);
        $this->repository->persist($SmsNoticeTwo);
        $this->repository->persist($EmailNoticeOne);
        $this->repository->persist($EmailNoticeTwo);
    }

    public function test_notify_via_email()
    {
        $id = $this->postJson('/notifications', [
            'channel' => [
                'type' => 'email',
                'event' => 'CartCreated',
                'campaignName' => 'yalda',
                'senderEmail' => 'malltina@yahoo.com',
                'senderName' => 'malltina',
                'message' => 'hello {{$name}}. wellcome to our website we send sms to {{$mobile}}. your email is {{$email}}. order id is {{$order_id}}',
                'subject' => 'subject',
                'attachment' => null,
                'canReplied' => false,
            ],
        ])->json('id');

        DB::table('analytic_schedules')->insert([
            'schedule_id' => 'schedule_id',
            'journey_id' => 'journey-2',
            'target_id' => 1,
            'active' => 1,
            'expectant' => 'some-event',
            'occurred_at' => now()->addMinute()
        ]);

        $this->postJson('/targets', [
            'id' => 1,
            'name' => 'mehdi',
            'email' => 'mehdi@yahoo.com',
            'mobile' => '091493434',
            'najva_token' => 'najva-token2',
        ]);

        event(new ActionCheckpointReached('schedule_id', 'checkpoint_id', $id, 1));

        $this->assertEquals(1, DB::table('notification_logs')->count());
    }

    public function test_notify_via_sms()
    {
        $id = $this->postJson('/notifications', [
            'channel' => [
                'type' => 'sms',
                'event' => 'CartCreated',
                'campaignName' => 'yalda',
                'message' => 'hi mehdi',
            ],
        ])->json('id');

        DB::table('analytic_schedules')->insert([
            'schedule_id' => 'schedule_id',
            'journey_id' => 'journey-2',
            'target_id' => 1,
            'active' => 1,
            'expectant' => 'some-event',
            'occurred_at' => now()->addMinute()
        ]);

        $this->postJson('/targets', [
            'id' => 1,
            'name' => 'mehdi',
            'email' => 'mehdi@yahoo.com',
            'mobile' => '091493434',
            'najva_token' => 'najva-token2',
        ]);

        event(new ActionCheckpointReached('schedule_id', 'checkpoint_id', $id, 1));

        $this->assertEquals(1, DB::table('notification_logs')->count());
    }
}
