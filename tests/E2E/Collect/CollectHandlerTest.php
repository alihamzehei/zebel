<?php

namespace Tests\E2E\Collect;

use App\Collect\Application\Command\CaptureActivityCommand;
use App\Shared\Application\Command\CommandBus;
use DateTimeImmutable;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Support\TestCase;

class CollectHandlerTest extends TestCase
{
    use DatabaseMigrations;

    private readonly CommandBus $bus;

    public function setUp(): void
    {
        parent::setUp();
        $this->bus = $this->app->get(CommandBus::class);
    }

    public function test_foo()    
    {
        $command = new CaptureActivityCommand(
            'foo',
            1,
            'ProductSeen',
            ['foo' => 'bar'],
            new DateTimeImmutable()
        );

        $this->bus->handle($command);

        $this->assertDatabaseHas('collect_activities' , [
            'title' => 'ProductSeen',
            'causer' => 1,
        ]);
        $this->assertDatabaseCount('collect_activities' , 1);
    }
}
