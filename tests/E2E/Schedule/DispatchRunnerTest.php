<?php

declare(strict_types=1);

namespace Test\E2E\Schedule;

use App\Collect\Application\Command\CaptureActivityCommand;
use App\Schedule\Application\Command\DispatchSchedulesCommand ;
use App\Target\Application\Command\RegisterTargetCommand;
use DateTimeImmutable;
use App\Schedule\Domain\Model\Schedule;
use App\Shared\Application\Command\CommandBus;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Support\TestCase;
use Tests\Support\Trait\Snapshot;

class DispatchRunnerTest extends TestCase
{
    use DatabaseMigrations;
    use Snapshot;

    private Schedule $initializedSchedule;
    private Schedule $initializedSchedule2;
    private Schedule $idleSchedule;
    private Schedule $closedSchedule;
    private CommandBus $bus;

    protected function setUp(): void
    {
        parent::setUp();
        $this->bus = $this->app->get(CommandBus::class);
    }

    // todo: increase test coverage.
    public function test_run_schedules()
    {
        // create a journey.
        $payload = $this->jsonSnap('journey');
        $this->post('/journeys', $payload);
        // capture some events related to the journey's trigger.

        $this->bus->handle(
            new RegisterTargetCommand(
                1,
                'fofo',
                'ab@gmail.com',
                '9331534539',
            )
        );

        $this->bus->handle(
            new RegisterTargetCommand(
                2,
                'fogo',
                'acc@gmail.com',
                '9339534539',
            )
        );

        $this->bus->handle(
            new RegisterTargetCommand(
                3,
                'fooy',
                'auu@gmail.com',
                '9321534539',
            )
        );

        $this->bus->handle(
            new CaptureActivityCommand('foo', 1, $payload['trigger'], ['foo' => 'bar'], new DateTimeImmutable())
        );
        $this->bus->handle(
            new CaptureActivityCommand('foo', 2, $payload['trigger'], ['foo' => 'bar'], new DateTimeImmutable())
        );
        $this->bus->handle(
            new CaptureActivityCommand('foo', 3, $payload['trigger'], ['foo' => 'bar'], new DateTimeImmutable())
        );

        $this->assertEquals(3, $this->bus->handle(new DispatchSchedulesCommand));
        $this->assertEquals(0, $this->bus->handle(new DispatchSchedulesCommand));
    }

    // public function test_schedules_state()
    // {
    //     $this->console->run(new StringInput('schedule:run'), new BufferedOutput());

    //     $this->assertCount(
    //         3, ScheduleModel::where('status', 'idle')->get()
    //     );
    // }

    // public function test_appending_activities()
    // {
    //     $schedule = Zebel::dbal()->fetchAssociative('select * from schedule_schedules');

    //     $this->console->run(new StringInput('schedule:run'), new BufferedOutput());

    //     \call_user_func(
    //         Zebel::subscriber(ExternalEventSubscriber::class),
    //         new ExternalEvent('foo', Trigger::CartCreated->name,
    //             ['id' => 123, 'name' => 'john', 'email' => 'john@doe.com', 'mobile' => '+989121234567'],
    //             new DateTimeImmutable('+20 minutes'),
    //             ['foo' => 'bar']
    //         )
    //     );

    //     zebel::dbal()->executequery(
    //         'update schedule_schedules set idle_until=?, expectant_name=?, expectant_date=? where id=?',
    //         [$this->dateTimeForPersist($idleFrom = new DateTimeImmutable('10 minutes ago')), Trigger::CartCreated->name, $this->dateTimeForPersist($idleFrom), $schedule['id']]
    //     );

    //     $this->console->run(new StringInput('schedule:run'), new BufferedOutput());

    //     $this->assertSame(
    //         'close', $this->uow->repository()->findById(new ScheduleId($schedule['id']))->status()->state
    //     );
    // }

    // public function test_analytic_records_should_be_persist_after_initializing_and_running_schedules()
    // {
    //     $this->console->run(new StringInput('schedule:run'), new BufferedOutput());

    //     $this->assertNotFalse(Zebel::dbal()->fetchAssociative('select * from analytic_checkpoints'));
    //     $this->assertNotFalse(Zebel::dbal()->fetchAssociative('select * from analytic_schedules'));
    // }
}
