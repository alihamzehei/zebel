<?php

declare(strict_types=1);

namespace Test\E2E\Schedule;

use App\Plan\Domain\Event\JourneyRemoved;
use App\Plan\Domain\Repository\JourneyRepository;
use App\Plan\Domain\ValueObject\JourneyId;
use App\Schedule\Application\Listener\RemoveJourneyListener;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Support\TestCase;
use Tests\Support\Trait\Snapshot;

class RemoveJourneyTest extends TestCase
{
    use DatabaseMigrations;
    use Snapshot;

    private JourneyRepository $repository;

    public function test_remove_schedule_when_journey_was_removed()
    {
        $payload = $this->jsonSnap('journey');
        $id = $this->postJson('/journeys', $payload)->json('id');

        $this->deleteJson("/journeys/{$id}");


         $this->assertDatabaseCount('plan_journeys' , 0);
    }
}
