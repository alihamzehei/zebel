<?php

declare(strict_types=1);

namespace Test\E2E\Plan;

use Tests\Support\TestCase;
use App\Target\Presentation\Module\TargetModule;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class TargetTest extends TestCase
{
    use DatabaseMigrations;

    protected TargetModule $module;

    protected function setUp(): void
    {
        parent::setUp();
        $this->module = $this->app->get(TargetModule::class);
    }

    public function test_register_target()
    {
        $this->postJson('/targets', [
            'id' => 1010,
            'name' => 'mehdi',
            'email' => 'mehdi@yahoo.com',
            'mobile' => '091493434',
            'najva_token' => 'najva-token2',
        ])->assertCreated();
    }

    public function test_get_target()
    {
        $this->postJson('/targets', $payload = [
            'id' => $id = 1010,
            'name' => 'mehdi',
            'email' => 'mehdi@yahoo.com',
            'mobile' => '091493434',
            'najva_token' => 'najva-token2',
        ]);

        $this->assertEquals($payload, $this->module->getTargetById($id));
    }
}
