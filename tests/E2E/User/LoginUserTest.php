<?php

declare(strict_types=1);

namespace Tests\E2E\User;

use Tests\Support\TestCase;;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Arr;

class LoginUserTest extends TestCase
{
    use DatabaseMigrations;

    public function test_register_users()
    {
        $this->postJson('/auth/register', $payload = [
            'first_name' => 'John',
            'last_name' => 'Doe',
            'email' => 'john@doe.com',
            'password' => 'secret$$password#',
        ]);

        $this->postJson('/auth/login', Arr::only($payload, ['email', 'password']))
            ->assertOk()
            ->assertJsonStructure(['token']);
    }
}
