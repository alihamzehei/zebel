<?php

declare(strict_types=1);

namespace Tests\E2E\User;

use Tests\Support\TestCase;;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class RegisterUserTest extends TestCase
{
    use DatabaseMigrations;

    public function test_register_users()
    {
        $this->postJson('/auth/register', [
            'first_name' => 'John',
            'last_name' => 'Doe',
            'email' => 'john@doe.com',
            'password' => 'secret$$password#',
        ])
            ->assertOk()
            ->assertJsonStructure([
                'token'
            ]);
    }
}
