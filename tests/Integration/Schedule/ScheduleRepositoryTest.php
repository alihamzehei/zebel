<?php

declare(strict_types=1);

namespace Tests\Integration\Schedule;

use DateTimeImmutable;
use Tests\Support\TestCase;
use App\Schedule\Domain\Model\Target;
use App\Schedule\Domain\Model\Journey;
use App\Schedule\Domain\Model\Activity;
use App\Schedule\Domain\Model\Schedule;
use Tests\Support\Factory\ScheduleFactory;
use App\Schedule\Domain\ValueObject\Status;
use App\Schedule\Infrastructure\Model\ScheduleModel;
use App\Schedule\Domain\ValueObject\IdleTime;
use App\Schedule\Domain\ValueObject\Expectant;
use App\Schedule\Domain\ValueObject\ScheduleId;
use App\Schedule\Domain\ValueObject\ActivityName;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Schedule\Domain\Repository\ScheduleRepository;
use App\Schedule\Domain\Model\Checkpoint\IdleCheckpoint;
use App\Schedule\Domain\Model\Checkpoint\StopCheckpoint;
use App\Schedule\Application\Serializer\JourneySerializer;
use App\Schedule\Domain\Model\Checkpoint\ActionCheckpoint;
use App\Schedule\Domain\Model\Checkpoint\EventOccurrenceCheckpoint;


class ScheduleRepositoryTest extends TestCase
{
    use DatabaseMigrations;

    private ScheduleRepository $repository;
    private JourneySerializer $serializer;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->app->get(ScheduleRepository::class);
        $this->serializer = new JourneySerializer;
    }

    public function test_persist_schedule()
    {
        $schedule = ScheduleFactory::checkpoints([
            new ActionCheckpoint('id-1', 'id-2', true, 'foo'),
            new ActionCheckpoint('id-2', 'id-3', true, 'foo'),
            new IdleCheckpoint('id-3', 'id-4', false, new IdleTime(120)),
            new EventOccurrenceCheckpoint(
                'id-4',
                'id-5',
                'id-6',
                false,
                new Activity(uniqid(), new ActivityName('activity-3'), new DateTimeImmutable()),
            ),
        ])
            ->setExpectant(new Expectant(new ActivityName('next-event'), new DateTimeImmutable()))
            ->targetTo(new Target(1, ['sms', 'email']))
            ->setId(new ScheduleId('foo-bar-baz'))
            ->idleUntil(new DateTimeImmutable('+130 minutes'))
            ->create();

        $this->repository->persist($schedule);

        $persisted = ScheduleModel::query()->firstWhere('uuid', $schedule->id->id);

        $this->assertSame($schedule->id->id, $persisted->uuid);
        $this->assertSame(['sms', 'email'], $persisted->target_channels);
        $this->assertSame($schedule->target->identifier, $persisted->target_id);
        $this->assertSame($schedule->status()->state, $persisted->status);
        $this->assertSame($schedule->idleUntil()->getTimestamp(), $persisted->idle_until->getTimeStamp());
        $this->assertSame($schedule->expectant()->activity->value, $persisted->expectant_name);
        $this->assertSame($schedule->expectant()->since->getTimestamp(), $persisted->expectant_date->getTimeStamp());
        $this->assertSame($schedule->journey()->id, $persisted->journey_id);
        $this->assertSame($schedule->journey()->rootId, $persisted->journey_root_id);
        $this->assertSame($schedule->journey()->trigger, $persisted->journey_trigger);
        $this->assertSame($schedule->journey()->current()->id(), $persisted->journey_current_id);
        $this->assertEquals(
            array_map([$this->serializer, 'serialize'], $schedule->journey()->checkpoints),
            $persisted->journey_checkpoints
        );

    }

    public function test_find_by_id()
    {
        $schedule = ScheduleFactory::checkpoints([
            new ActionCheckpoint('id-1', 'id-2', true, 'foo'),
            new ActionCheckpoint('id-2', 'id-3', true, 'foo'),
            new IdleCheckpoint('id-3', 'id-4', false, new IdleTime(120)),
            new EventOccurrenceCheckpoint(
                'id-4',
                'id-5',
                'id-6',
                false,
                new Activity(uniqid(), new ActivityName('activity-3'), new DateTimeImmutable()),
            ),
        ])
            ->setExpectant(new Expectant(new ActivityName('next-event'), new DateTimeImmutable()))
            ->targetTo(new Target(1, ['sms', 'email']))
            ->setId(new ScheduleId('foo-bar-baz'))
            ->idleUntil(new DateTimeImmutable('+130 minutes'))
            ->create();

        $this->repository->persist($schedule);

        $persisted = $this->repository->findById($schedule->id);

        $this->assertSame($schedule->journey()->trigger, $persisted->journey()->trigger);
        $this->assertTrue($persisted->expectant()->activity->isEqualsTo(new ActivityName('next-event')));
        $this->assertTrue(
            $persisted->expectant()->isMatchWith(new Expectant(new ActivityName('next-event'), new DateTimeImmutable()))
        );

        $diff = $persisted->idleUntil()->diff(new DateTimeImmutable());
        $this->assertEqualsWithDelta(130, $diff->h * 60 + $diff->i, 1);

        $id1 = $schedule->journey()->findById('id-1');
        $this->assertInstanceOf(ActionCheckpoint::class, $id1);
        $this->assertSame('id-1', $id1->id());
        $this->assertSame('id-2', $id1->leftChildId());
        $this->assertNull($id1->rightChildId());
        $this->assertTrue($id1->isChecked());

        $id2 = $schedule->journey()->findById('id-2');
        $this->assertInstanceOf(ActionCheckpoint::class, $id2);
        $this->assertSame('id-2', $id2->id());
        $this->assertSame('id-3', $id2->leftChildId());
        $this->assertNull($id2->rightChildId());
        $this->assertTrue($id2->isChecked());

        $id3 = $schedule->journey()->findById('id-3');
        $this->assertInstanceOf(IdleCheckpoint::class, $id3);
        $this->assertSame('id-3', $id3->id());
        $this->assertSame('id-4', $id3->leftChildId());
        $this->assertNull($id3->rightChildId());
        $this->assertFalse($id3->isChecked());
        $this->assertSame(120, $id3->until->minutes);

        $id4 = $schedule->journey()->findById('id-4');
        $this->assertInstanceOf(EventOccurrenceCheckpoint::class, $id4);
        $this->assertSame('id-4', $id4->id());
        $this->assertSame('id-5', $id4->leftChildId());
        $this->assertSame('id-6', $id4->rightChildId());
        $this->assertFalse($id3->isChecked());
        $this->assertTrue($id4->activity->name->isEqualsTo(new ActivityName('activity-3')));
    }

    public function test_find_runnable()
    {
        $initializedSchedule = new Schedule(
            new ScheduleId('s1'),
            new Target(1, []),
            new Journey('bar1', 'bar_name', 'root', 'root', 'trigger', [
                new StopCheckpoint('root', false),
            ]),
            Status::init()
        );

        $idleSchedule = new Schedule(
            new ScheduleId('s2'),
            new Target(1, []),
            new Journey('bar2', 'bar_name', 'root', 'root', 'trigger', [
                new StopCheckpoint('root', false),
            ]),
            Status::idle(),
            idleUntil: new DateTimeImmutable('10 minutes ago')
        );

        $beforeDueSchedule = new Schedule(
            new ScheduleId('s3'),
            new Target(1, []),
            new Journey('bar3', 'bar_name', 'root', 'root', 'trigger', [
                new StopCheckpoint('root', false),
            ]),
            Status::idle(),
            idleUntil: new DateTimeImmutable('+10 minutes')
        );

        $beforeDueSchedule2 = new Schedule(
            new ScheduleId('s4'),
            new Target(1, []),
            new Journey('bar4', 'bar_name', 'root', 'root', 'trigger', [
                new StopCheckpoint('root', false),
            ]),
            Status::idle(),
            idleUntil: new DateTimeImmutable('+1 hour')
        );
        $this->repository->persist($initializedSchedule);
        $this->repository->persist($idleSchedule);
        $this->repository->persist($beforeDueSchedule);
        $this->repository->persist($beforeDueSchedule2);

        $this->assertCount(2, $runnable = $this->repository->findRunnable());

        $this->assertScheduleExists($runnable, $initializedSchedule->id);
        $this->assertScheduleExists($runnable, $idleSchedule->id);
    }

    private function assertScheduleExists(array $schedules, ScheduleId $id): void
    {
        foreach ($schedules as $schedule) {
            if ($schedule->id->isEqualsTo($id)) {
                return;
            }
        }

        $this->fail("No such schedule with id [$id->id]");
    }
}
