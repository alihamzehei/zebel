<?php

declare(strict_types=1);

namespace Tests\Integration\Schedule;

use App\Schedule\Infrastructure\Model\ScheduleJourneyModel;
use DateTimeImmutable;
use App\Schedule\Domain\Model\Activity;
use App\Schedule\Domain\Model\Checkpoint\ActionCheckpoint;
use App\Schedule\Domain\Model\Checkpoint\EventOccurrenceCheckpoint;
use App\Schedule\Domain\Model\Checkpoint\IdleCheckpoint;
use App\Schedule\Domain\Model\Journey;
use App\Schedule\Domain\Repository\JourneyRepository;
use App\Schedule\Domain\ValueObject\ActivityName;
use App\Schedule\Domain\ValueObject\IdleTime;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Support\TestCase;

class JourneyRepositoryTest extends TestCase
{
    use DatabaseMigrations;

    private JourneyRepository $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->app->get(JourneyRepository::class);
    }

    public function test_persist()
    {
        $journey = new Journey('foo', 'foo_name', 'id-1', 'id-1', 'trigger', [
            new ActionCheckpoint('id-1', 'id-2', true, 'foo'),
            new ActionCheckpoint('id-2', 'id-3', true, 'foo'),
            new IdleCheckpoint('id-3', 'id-4', false, new IdleTime(120)),
            new EventOccurrenceCheckpoint(
                'id-4',
                'id-5',
                'id-6',
                false,
                new Activity(uniqid(), new ActivityName('activity-3'), new DateTimeImmutable()),
            ),
        ]);

        $this->repository->persist($journey);

        $persisted = ScheduleJourneyModel::first();

        $this->assertSame($journey->id, $persisted->uuid);
        $this->assertSame($journey->current()->id(), $persisted->current_id);
        $this->assertSame($journey->trigger, $persisted->trigger);
        $this->assertCount(count($journey->checkpoints), $persisted->checkpoints);
    }

    public function test_find_by_id()
    {
        $journey = new Journey('foo', 'foo_name', 'id-1', 'id-1', 'trigger', [
            new ActionCheckpoint('id-1', 'id-2', true, 'foo'),
            new ActionCheckpoint('id-2', 'id-3', true, 'foo'),
            new IdleCheckpoint('id-3', 'id-4', false, new IdleTime(120)),
            new EventOccurrenceCheckpoint(
                'id-4',
                'id-5',
                'id-6',
                false,
                new Activity(uniqid(), new ActivityName('activity-3'), new DateTimeImmutable()),
            ),
        ]);

        $this->repository->persist($journey);

        $journey = $this->repository->findById($journey->id);

        $this->assertNotNull($journey);

        $id1 = $journey->findById('id-1');
        $this->assertInstanceOf(ActionCheckpoint::class, $id1);
        $this->assertSame('id-1', $id1->id());
        $this->assertSame('id-2', $id1->leftChildId());
        $this->assertNull($id1->rightChildId());
        $this->assertTrue($id1->isChecked());

        $id2 = $journey->findById('id-2');
        $this->assertInstanceOf(ActionCheckpoint::class, $id2);
        $this->assertSame('id-2', $id2->id());
        $this->assertSame('id-3', $id2->leftChildId());
        $this->assertNull($id2->rightChildId());
        $this->assertTrue($id2->isChecked());

        $id3 = $journey->findById('id-3');
        $this->assertInstanceOf(IdleCheckpoint::class, $id3);
        $this->assertSame('id-3', $id3->id());
        $this->assertSame('id-4', $id3->leftChildId());
        $this->assertNull($id3->rightChildId());
        $this->assertFalse($id3->isChecked());
        $this->assertSame(120, $id3->until->minutes);

        $id4 = $journey->findById('id-4');
        $this->assertInstanceOf(EventOccurrenceCheckpoint::class, $id4);
        $this->assertSame('id-4', $id4->id());
        $this->assertSame('id-5', $id4->leftChildId());
        $this->assertSame('id-6', $id4->rightChildId());
        $this->assertFalse($id4->isChecked());
        $this->assertTrue($id4->activity->name->isEqualsTo(new ActivityName('activity-3')));
    }
}
