<?php

declare(strict_types=1);

namespace Tests\Integration\Notification;

use App\Notification\Domain\Model\EmailNotice;
use App\Notification\Domain\Model\SmsNotice;
use App\Notification\Domain\Repository\NotifyRepository;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Support\TestCase;

class NotifyRepositoryTest extends TestCase
{
    use DatabaseMigrations;

    private NotifyRepository $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->app->get(NotifyRepository::class);
    }

    public function test_persist_notify()
    {
        $notice = new SmsNotice(
            null,
            'notification_id',
            'checkpoint_id',
            'schedule_id',
            1,
            SmsNotice::TYPE,
            false
        );

        $this->repository->persist($notice);

        $this->assertDatabaseHas('notification_notifies', [
            'notification_id' => $notice->notificationId,
            'schedule_id' => $notice->scheduleId,
            'checkpoint_id' => $notice->checkpointId,
            'target_id' =>  $notice->targetId,
            'type' => $notice->type,
            'is_sent' => $notice->isSent,
        ]);
    }

    public function test_idempotency_of_persisting_notify()
    {
        $this->expectException(QueryException::class);
        $SmsNoticeOne = new SmsNotice(null, 'notification_id_1', 'checkpoint_id', 'schedule_id', 1, SmsNotice::TYPE, false);
        $SmsNoticeTwo = new SmsNotice(null, 'notification_id_2', 'checkpoint_id', 'schedule_id', 1, SmsNotice::TYPE, true);

        $this->repository->persist($SmsNoticeOne);
        $this->repository->persist($SmsNoticeTwo);
    }

    public function test_find_sendable()
    {
        $SmsNoticeOne = new SmsNotice(null, 'notification_id', 'checkpoint_id_1', 'schedule_id', 1, SmsNotice::TYPE, false);
        $SmsNoticeTwo = new SmsNotice(null, 'notification_id', 'checkpoint_id_2', 'schedule_id', 1, SmsNotice::TYPE, true);
        $EmailNoticeOne = new EmailNotice(null, 'notification_id', 'checkpoint_id_3', 'schedule_id', 1, EmailNotice::TYPE, false);
        $EmailNoticeTwo = new EmailNotice(null, 'notification_id', 'checkpoint_id_4', 'schedule_id', 1, EmailNotice::TYPE, false);

        $this->repository->persist($SmsNoticeOne);
        $this->repository->persist($SmsNoticeTwo);
        $this->repository->persist($EmailNoticeOne);
        $this->repository->persist($EmailNoticeTwo);

        $this->assertCount(3, $this->repository->findSendable());
    }
}
