<?php

declare(strict_types=1);

namespace Tests\Integration\Notification;

use App\Notification\Domain\Model\Channel\SmsChannel;
use App\Notification\Domain\Model\Notification;
use App\Notification\Domain\Repository\NotificationRepository;
use App\Notification\Domain\ValueObject\CampaignName;
use App\Notification\Domain\ValueObject\NotificationId;
use App\Notification\Domain\ValueObject\Sender;
use App\Notification\Domain\ValueObject\SmsMessage;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Support\TestCase;

class NotificationRepositoryTest extends TestCase
{
    use DatabaseMigrations;
    private NotificationRepository $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->app->get(NotificationRepository::class);
    }

    public function test_persist_channel()
    {
        $channel = new SmsChannel(
            new CampaignName('yalda'),
            new Sender('12345678'),
            new SmsMessage('this is yalda'),
            'b'
        );

        $notification = new Notification(
            new NotificationId(uniqid()),
            $channel,
            'foo'
        );

        $this->repository->persist($notification);

        $this->assertDatabaseHas('notification_notifications', [
            'uuid' => $notification->id->id,
            'journey_id' => $notification->journeyId,
            'type' => $notification->channel->type(),
        ]);
    }

    public function test_find_by_id()
    {
        $channel = new SmsChannel(
            new CampaignName('yalda'),
            new Sender('12345678'),
            new SmsMessage('this is yalda'),
            'x'
        );

        $notification = new Notification(
            new NotificationId(uniqid()),
            $channel,
            'foo'
        );

        $this->repository->persist($notification);

        $fetched = $this->repository->findById($notification->id);

        $this->assertEquals($notification->id->id, $fetched->id->id);
        $this->assertEquals($notification->journeyId, $fetched->journeyId);

        $this->assertInstanceOf(SmsChannel::class, $fetched->channel);
        $this->assertEquals($channel->campaignName->persian, $fetched->channel->campaignName->persian);
        $this->assertEquals($channel->message->content, $fetched->channel->message->content);
        $this->assertEquals($channel->sender->number, $fetched->channel->sender->number);
    }
}
