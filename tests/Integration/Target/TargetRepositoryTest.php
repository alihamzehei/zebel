<?php

declare(strict_types=1);

namespace Test\Integration\Target;

use Tests\Support\TestCase;
use App\Target\Domain\Model\Target;
use App\Target\Domain\Repository\TargetRepository;
use Illuminate\Foundation\Testing\DatabaseMigrations;


class TargetRepositoryTest extends TestCase
{
    use DatabaseMigrations;

    private TargetRepository $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->app->get(TargetRepository::class);
    }

    public function test_persist()
    {
        $this->repository->persist(
            $target = new Target(1010, 'John', 'john@doe.com', '+989121234567')
        );

        $this->assertDatabaseHas('target_targets', [
            'id' => $target->id,
            'name' => $target->name,
            'email' => $target->email,
            'mobile' => $target->mobile,
        ]);
    }

    public function test_idempotency()
    {
        $this->repository->persist($target = new Target(1, 'John', 'john@doe.com', '+989121234567'));

        $this->repository->persist($target);

        $this->assertDatabaseCount('target_targets', 1);
    }
}
