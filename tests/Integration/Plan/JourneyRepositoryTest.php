<?php

declare(strict_types=1);

namespace Tests\Integration\Plan;

use Tests\Support\TestCase;;
use Tests\Support\Trait\Snapshot;
use App\Plan\Domain\Model\Journey;
use App\Plan\Domain\Model\Trigger;
use App\Plan\Domain\ValueObject\Name;
use App\Plan\Infrastructure\Model\JourneyModel;
use App\Plan\Domain\ValueObject\JourneyId;
use App\Plan\Infrastructure\Model\CheckpointModel;
use App\Plan\Domain\Model\Checkpoint\Checkpoint;
use App\Plan\Domain\Repository\JourneyRepository;
use App\Plan\Domain\Model\Checkpoint\IdleCheckpoint;
use App\Plan\Domain\Model\Checkpoint\StopCheckpoint;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Plan\Domain\Model\Checkpoint\ActionCheckpoint;
use App\Plan\Domain\Factory\CheckpointAggregateFactory;
use App\Plan\Domain\Model\Checkpoint\ReachabilityConditionalCheckpoint;
use App\Plan\Domain\Model\Checkpoint\EventOccurrenceConditionalCheckpoint;


class JourneyRepositoryTest extends TestCase
{
    use DatabaseMigrations;
    use Snapshot;

    private array $journey;
    private JourneyRepository $repo;

    protected function setUp(): void
    {
        parent::setUp();
        $this->journey = $this->jsonSnap('journey');
        $this->repo = $this->app->get(JourneyRepository::class);
    }

    public function test_persist()
    {
        $factory = new CheckpointAggregateFactory(
            $this->journey['root'], $this->journey['checkpoints']
        );

        $journey = new Journey(
            new JourneyId(uniqid()),
            new Name($this->journey['name']),
            Trigger::CartCreated,
            $this->journey['meta'],
            $factory->create()
        );

        $this->repo->persist($journey);

        $persisted = JourneyModel::query()
            ->where('uuid', $journey->id->id)
            ->firstOr(fn() => $this->fail('journey not found'));

        $this->assertSame($journey->name->value, $persisted->name);
        $this->assertSame($journey->trigger->name, $persisted->trigger);
        $this->assertSame($journey->checkpoints->root->id, $persisted->root);
        $this->assertSame($journey->enable, $persisted->enable);

        foreach ($journey->checkpoints->checkpoints() as $checkpoint) {
            $this->assertCheckpointPersisted($checkpoint);
        }
    }

    public function test_find_by_id()
    {
        $factory = new CheckpointAggregateFactory(
            $this->journey['root'], $this->journey['checkpoints'], false
        );

        $journey = new Journey(
            new JourneyId(uniqid()),
            new Name($this->journey['name']),
            Trigger::CartCreated,
            $this->journey['meta'],
            $factory->create()
        );

        $this->repo->persist($journey);
        $this->assertInstanceOf(Journey::class, $found = $this->repo->findById($journey->id));

        $this->assertSame($journey->id->id, $found->id->id);
        $this->assertSame($journey->trigger, $found->trigger);
        $this->assertSame($journey->name->value, $found->name->value);
        $this->assertSame($journey->metaData, $found->metaData);
        $this->assertSame($journey->enable, $found->enable);
        $this->assertSame(
            array_keys($journey->checkpoints->asArray()),
            array_keys($found->checkpoints->asArray())
        );
    }

    public function test_delete_by_id()
    {
        $factory = new CheckpointAggregateFactory(
            $this->journey['root'], $this->journey['checkpoints']
        );

        $journey = new Journey(
            new JourneyId(uniqid()),
            new Name($this->journey['name']),
            Trigger::CartCreated,
            $this->journey['meta'],
            $factory->create()
        );


        $this->repo->persist($journey);
        $this->repo->deleteById($journey->id);

        $persistedJourney = JourneyModel::find($journey->id->id);
        $persistedCheckpoints = CheckpointModel::where('journey_id', $journey->id->id)->get();

        $this->assertNull($persistedJourney);
        $this->assertEmpty($persistedCheckpoints);
    }

    public function assertCheckpointPersisted(Checkpoint $checkpoint)
    {
        $this->assertNotFalse(
            $persisted = CheckpointModel::firstWhere('uuid', $checkpoint->id()->id)->toArray()
        );

        $this->assertSame($checkpoint->id()->id, $persisted['uuid']);
        $this->assertSame($checkpoint->type(), $persisted['type']);

        switch (\get_class($checkpoint)) {
            case IdleCheckpoint::class:
                $this->assertIdleCheckpointPersistedCorrectly($checkpoint, $persisted);
                break;
            case ActionCheckpoint::class:
                $this->assertActionCheckpointPersistedCorrectly($checkpoint, $persisted);
                break;
            case ReachabilityConditionalCheckpoint::class:
                $this->assertReachabilityConditionalCheckpointPersistedCorrectly($checkpoint, $persisted);
                break;
            case EventOccurrenceConditionalCheckpoint::class:
                $this->assertEventOccurrenceConditionalCheckpointPersistedCorrectly($checkpoint, $persisted);
                break;
            case StopCheckpoint::class:
                break;
            default:
                $this->fail('Unexpected checkpoint type '.\get_class($checkpoint));
        }
    }

    public function assertIdleCheckpointPersistedCorrectly(IdleCheckpoint $checkpoint, array $persisted)
    {
        $this->assertSame($checkpoint->id()->leftChildId->id, $persisted['left_child_id']);
        $this->assertSame($checkpoint->timer->asMinutes(), $persisted['payload']['time']);
        $this->assertSame($checkpoint->metaData, $persisted['meta']);
    }

    public function assertActionCheckpointPersistedCorrectly(ActionCheckpoint $checkpoint, array $persisted)
    {
        $this->assertSame($checkpoint->id()->leftChildId->id, $persisted['left_child_id']);
        $this->assertSame($checkpoint->metaData, $persisted['meta']);
    }

    public function assertReachabilityConditionalCheckpointPersistedCorrectly(ReachabilityConditionalCheckpoint $checkpoint, array $persisted)
    {
        $this->assertSame($checkpoint->id()->leftChildId->id, $persisted['left_child_id']);
        $this->assertSame($checkpoint->id()->rightChildId->id, $persisted['right_child_id']);
        $this->assertSame($checkpoint->userReachabilityCondition, $persisted['payload']['channel']);
        $this->assertSame($checkpoint->metaData, $persisted['meta']);
    }

    public function assertEventOccurrenceConditionalCheckpointPersistedCorrectly(EventOccurrenceConditionalCheckpoint $checkpoint, array $persisted)
    {
        $this->assertSame($checkpoint->id()->leftChildId->id, $persisted['left_child_id']);
        $this->assertSame($checkpoint->id()->rightChildId->id, $persisted['right_child_id']);
        $this->assertSame($checkpoint->eventOccurrenceCondition, $persisted['payload']['condition']);
        $this->assertSame($checkpoint->metaData, $persisted['meta']);
    }
}
