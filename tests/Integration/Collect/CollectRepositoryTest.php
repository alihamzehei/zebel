<?php

declare(strict_types=1);

namespace Test\Integration\Collect;

use App\Collect\Infrastructure\Model\CollectActivityModel;
use DateTimeImmutable;
use Tests\Support\TestCase;
use App\Collect\Domain\Model\Activity;
use App\Collect\Domain\ValueObject\ActivityId;
use App\Collect\Domain\Repository\ActivityRepository;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class CollectRepositoryTest extends TestCase
{
    use DatabaseMigrations;

    private ActivityRepository $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->app->get(ActivityRepository::class);
    }

    public function test_persist_activity()
    {
        $activity = new Activity(new ActivityId('foo'), 'The title', 1, new DateTimeImmutable(), [
            'foo' => 'bar',
        ]);

        $this->repository->persist($activity);

        $persisted = CollectActivityModel::query()->first();

        $this->assertEquals('foo', $persisted->uuid);
        $this->assertEquals('The title', $persisted->title);
        $this->assertEquals(1, $persisted->causer);
        $this->assertEquals(['foo' => 'bar'], $persisted->payload);
    }
}
