<?php

namespace App\Notification\Presentation\Requests;

use App\Plan\Domain\Model\Trigger;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class GetEventAttributesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'event' => ['required', Rule::in([
                Trigger::CartCreated->name,
                Trigger::CartRemoved->name,
                Trigger::OrderPaid->name,
                Trigger::OrderStored->name,
                Trigger::OrderLineCanceled->name,
                Trigger::ProductSeen->name,
                Trigger::ProductAddedToWishList->name,
                Trigger::ProductRemovedFromWishList->name,
                Trigger::CustomerRegistered->name,
            ])]
        ];
    }
}
