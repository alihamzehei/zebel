<?php

namespace App\Notification\Presentation\Console;

use App\Notification\Domain\Repository\NotifyRepository;
use Illuminate\Console\Command;
use App\Shared\Application\Command\CommandBus;
use App\Notification\Application\Command\SendNotifyCommand;


class NotifyConsoleCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:notify';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Execute the scheduled journeys';

    /**
     * Execute the console command.
     */
    public function handle(CommandBus $bus)
    {
        $notifies = $this->notifies()->findSendable();

        if (!$notifies) {
            return self::SUCCESS;
        }

        // $this->overrideNotifyRepositoryWithCachedVersion(
        //     new CacheAwareNotifyRepository(Zebel::get(NotifyRepository::class), $notifies)
        // );

        foreach ($notifies as $notify) {
            $bus->handle(new SendNotifyCommand($notify->id, $notify->notificationId, $notify->targetId));
        }

        // $output->writeln(sprintf('%s notifies have been send', \count($notifies)));

         return self::SUCCESS;
    }

    private function notifies(): NotifyRepository
    {
        return app(NotifyRepository::class);
    }
}
