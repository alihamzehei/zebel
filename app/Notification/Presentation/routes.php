<?php

use App\Notification\Presentation\Controllers\CreateNotificationController;
use App\Notification\Presentation\Controllers\GetEventAttributesController;
use App\Notification\Presentation\Controllers\GetNotificationController;
use Illuminate\Support\Facades\Route;

Route::post('notifications', CreateNotificationController::class);
Route::get('notifications/eventAttributes', GetEventAttributesController::class);
Route::get('notifications/{id}', GetNotificationController::class);
