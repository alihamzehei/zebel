<?php

declare(strict_types=1);

namespace App\Notification\Presentation\Controllers;

use App\Notification\Presentation\Requests\GetEventAttributesRequest;
use App\Shared\Application\Query\QueryBus;
use App\Shared\Presentation\Http\Controllers\Controller;
use App\Notification\Application\Query\EventAttributesQuery;
use Illuminate\Http\JsonResponse;

class GetEventAttributesController extends Controller
{
    public function __construct(private readonly QueryBus $bus)
    {
    }

    public function __invoke(GetEventAttributesRequest $request): JsonResponse
    {
        return response()->json($this->bus->ask(new EventAttributesQuery($request->validated('event'))));
    }
}
