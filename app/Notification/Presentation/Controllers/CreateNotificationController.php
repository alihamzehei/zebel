<?php

declare(strict_types=1);

namespace App\Notification\Presentation\Controllers;

use App\Plan\Domain\Model\Trigger;
use App\Shared\Application\Command\CommandBus;
use App\Shared\Presentation\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Notification\Application\Command\CreateNotificationCommand;
use Illuminate\Validation\Rule;
use Symfony\Component\HttpFoundation\Response;

class CreateNotificationController extends Controller
{
    public function __construct(private readonly CommandBus $bus)
    {
    }

    public function __invoke(Request $request): Response
    {
        $request->validate([
            'channel' => ['required', 'array'],
            'channel.type' => ['required']
        ]);

        $fields = match ($request['channel']['type']) {
            'sms' => $this->smsChannelFields(),
            'email' => $this->emailChannelFields(),
            'webPush' => $this->webPushChannelFields(),
        };

        foreach ($fields as $key => $field) {
            $fields['channel.' . $key] = $field;

            unset($fields[$key]);
        }

        $request->validate($fields);

        $id = $this->bus->handle(new CreateNotificationCommand($request['channel']));

        return response()->json(compact('id'), Response::HTTP_CREATED);
    }

    private function smsChannelFields(): array
    {
        return [
            'campaignName' => ['required'],
            'message' => ['required'],
            'event' => ['required', Rule::in(array_map(fn($item) => $item->name, Trigger::cases()))],
        ];
    }

    private function emailChannelFields(): array
    {
        return [
            'campaignName' => ['required'],
            'message' => ['required'],
            'senderEmail' => ['required'],
            'senderName' => ['required'],
            'subject' => ['required'],
            'attachment' => ['nullable'],
            'canReplied' => ['required'],
            'event' => ['required', Rule::in(array_map(fn($item) => $item->name, Trigger::cases()))],
        ];
    }

    private function webPushChannelFields(): array
    {
        return [
            'title' => ['required'],
            'pushMessage' => ['required'],
            'link' => ['required'],
            'campaignName' => ['required'],
            'image' => ['nullable'],
            'icon' => ['nullable'],
        ];
    }
}
