<?php

declare(strict_types=1);

namespace App\Notification\Presentation\Message;

use App\Notification\Application\Sender\SenderFactory;
use App\Notification\Application\UnitOfWork\NotificationUnitOfWork;
use App\Notification\Domain\Model\Channel\SmsChannel;
use App\Notification\Domain\ValueObject\NotificationId;
use App\Schedule\Domain\Event\ActionCheckpointReached;
use App\Shared\Domain\Event\DomainEvent;
use App\Shared\Presentation\Message\Subscriber;
use App\Target\Presentation\Module\TargetModule;

class SendNotificationSubscriber extends Subscriber
{
    public function __construct(
        private readonly NotificationUnitOfWork $uow,
        private readonly TargetModule $targets,
        private readonly SenderFactory $factory
    ) {
    }

    public function __invoke(DomainEvent|ActionCheckpointReached $event): void
    {
        $notification = $this->uow->repository()->findById(
            new NotificationId($event->notificationId)
        );

        if ($notification === null) {
            return;
        }

        $target = $this->targets->getTargetById($event->targetId);

        if (!$target) {
            return;
        }

        if (\get_class($notification->channel) === SmsChannel::class && !$target['mobile']) {
            return;
        }

        $messageData = [
            'type' => $notification->channel->type(),
            'campaignName' => $notification->channel->campaignName->persian,
            ...$event->activities[array_key_last($event->activities)]->payload,
        ];

        $this->factory->createSenderForTarget($notification->channel, $event->targetId)
            ->send($target, $notification->channel, $messageData);
    }
}
