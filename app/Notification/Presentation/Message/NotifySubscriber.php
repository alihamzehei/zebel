<?php

declare(strict_types=1);

namespace App\Notification\Presentation\Message;

use App\Notification\Application\Command\PersistNotifyCommand;
use App\Schedule\Domain\Event\ActionCheckpointReached;
use App\Shared\Application\CommandBus;
use App\Shared\Domain\Event\DomainEvent;
use App\Shared\Presentation\Message\Subscriber;

class NotifySubscriber extends Subscriber
{
    public function __construct(private CommandBus $bus)
    {
    }

    public function __invoke(DomainEvent|ActionCheckpointReached $event): void
    {
        $this->bus->handle(new PersistNotifyCommand($event->notificationId, $event->targetId, $event->scheduleId, $event->checkpointId));
    }
}
