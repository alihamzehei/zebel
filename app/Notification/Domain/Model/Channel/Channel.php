<?php

declare(strict_types=1);

namespace App\Notification\Domain\Model\Channel;

interface Channel
{
    public function type(): string;
}
