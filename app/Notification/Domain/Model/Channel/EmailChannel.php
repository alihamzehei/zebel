<?php

declare(strict_types=1);

namespace App\Notification\Domain\Model\Channel;

use App\Notification\Domain\ValueObject\CampaignName;
use App\Notification\Domain\ValueObject\EmailMessage;

class EmailChannel implements Channel
{
    public const TYPE = 'email';

    public function __construct(public readonly CampaignName $campaignName,
                                public readonly string       $senderEmail,
                                public readonly string       $senderName,
                                public readonly string       $subject,
                                public readonly EmailMessage $message,
                                public readonly ?string      $attachment = '',
                                public readonly ?bool        $canReply = true,
                                public readonly string       $event)
    {
    }

    public function type(): string
    {
        return self::TYPE;
    }
}
