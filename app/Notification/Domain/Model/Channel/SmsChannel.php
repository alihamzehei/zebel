<?php

declare(strict_types=1);

namespace App\Notification\Domain\Model\Channel;

use App\Notification\Domain\ValueObject\CampaignName;
use App\Notification\Domain\ValueObject\Sender;
use App\Notification\Domain\ValueObject\SmsMessage;

class SmsChannel implements Channel
{
    public const TYPE = 'sms';

    public function __construct(public readonly CampaignName $campaignName,
                                public readonly Sender       $sender,
                                public readonly SmsMessage   $message,
                                public readonly string       $event)
    {
    }

    public function type(): string
    {
        return self::TYPE;
    }
}
