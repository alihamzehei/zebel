<?php

declare(strict_types=1);

namespace App\Notification\Domain\Model\Channel;

use App\Notification\Domain\ValueObject\CampaignName;
use App\Notification\Domain\ValueObject\PushMessage;
use App\Notification\Domain\ValueObject\Title;

class WebPushChannel implements Channel
{
    public const TYPE = 'webPush';

    public function __construct(public readonly Title $title,
                                public readonly PushMessage $message,
                                public readonly string $link,
                                public readonly CampaignName $campaignName,
                                public readonly ?string $image,
                                public readonly ?string $icon)
    {
    }

    public function type(): string
    {
        return self::TYPE;
    }
}
