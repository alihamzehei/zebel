<?php

declare(strict_types=1);

namespace App\Notification\Domain\Model\Channel;

use App\Notification\Domain\ValueObject\CampaignName;
use App\Notification\Domain\ValueObject\PushMessage;
use App\Notification\Domain\ValueObject\Title;

class PushChannel implements Channel
{
    public const TYPE = 'push';

    public function __construct(public readonly CampaignName $campaignName,
                                public readonly Title $title,
                                public readonly PushMessage $message,
                                public readonly ?string $image = null)
    {
    }

    public function type(): string
    {
        return self::TYPE;
    }
}
