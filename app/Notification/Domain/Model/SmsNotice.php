<?php

declare(strict_types=1);

namespace App\Notification\Domain\Model;

use App\Shared\Domain\AggregateRoot;

class SmsNotice extends AggregateRoot implements Notice
{
    public const TYPE = 'sms';

    public function __construct(public readonly ?int $id,
                                public readonly string $notificationId,
                                public readonly string $checkpointId,
                                public readonly string $scheduleId,
                                public readonly int $targetId,
                                public readonly string $type,
                                public readonly bool $isSent)
    {
    }
}
