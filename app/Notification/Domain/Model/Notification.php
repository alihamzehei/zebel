<?php

declare(strict_types=1);

namespace App\Notification\Domain\Model;

use App\Shared\Domain\AggregateRoot;
use App\Notification\Domain\Model\Channel\Channel;
use App\Notification\Domain\ValueObject\NotificationId;


class Notification extends AggregateRoot
{
    public function __construct(public readonly NotificationId $id,
                                public readonly Channel $channel,
                                public readonly ?string $journeyId = null)
    {
    }
}
