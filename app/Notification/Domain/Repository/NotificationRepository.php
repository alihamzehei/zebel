<?php

declare(strict_types=1);

namespace App\Notification\Domain\Repository;

use App\Notification\Domain\Model\Notification;
use App\Notification\Domain\ValueObject\NotificationId;
use App\Shared\Domain\Repository;

interface NotificationRepository extends Repository
{
    public function nextIdentity(): NotificationId;

    public function findById(NotificationId $id): ?Notification;

    public function persist(Notification $notification): void;
}
