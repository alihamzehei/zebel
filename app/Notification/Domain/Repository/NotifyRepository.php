<?php

declare(strict_types=1);

namespace App\Notification\Domain\Repository;

use App\Shared\Domain\Repository;
use App\Notification\Domain\Model\Notice;

interface NotifyRepository extends Repository
{
    public function findById(int $id): ?Notice;

    public function persist(Notice $notify): void;

    /**  @return Notice[] */
    public function findSendable(): array;
}
