<?php

declare(strict_types=1);

namespace App\Notification\Domain\ValueObject;

use App\Shared\Domain\ValueObject\ValueError;

class Title
{
    public const MAX_LENGTH = 35;
    public const MIN_LENGTH = 3;

    public function __construct(
        public readonly string $persian,
        public readonly ?string $english = null
    ) {
        if (!mb_strlen($this->persian)) {
            throw new ValueError('The title cant be empty');
        }

        $this->validateLength($this->persian);
        if ($this->english !== null) {
            $this->validateLength($this->english);
        }
    }

    public function validateLength(string $value): void
    {
        if (mb_strlen($value) >= self::MAX_LENGTH) {
            throw new ValueError('The title cant be greater than '.self::MAX_LENGTH);
        }

        if (mb_strlen($value) <= self::MIN_LENGTH) {
            throw new ValueError('The title cant be smaller than '.self::MIN_LENGTH);
        }
    }
}
