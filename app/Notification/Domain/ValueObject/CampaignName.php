<?php

declare(strict_types=1);

namespace App\Notification\Domain\ValueObject;

use App\Shared\Domain\ValueObject\ValueError;

class CampaignName
{
    public const MIN_LENGTH = 2;
    public const MAX_LENGTH = 190;

    public function __construct(public readonly string $persian)
    {
        if (!mb_strlen($this->persian)) {
            throw new ValueError('Campaign name cant be empty');
        }

        $this->validateLength($this->persian);
    }

    private function validateLength(string $value): void
    {
        if (mb_strlen($value) <= self::MIN_LENGTH) {
            throw new ValueError('Campaign name cant be smaller than '.self::MIN_LENGTH.' characters');
        }

        if (mb_strlen($value) >= self::MAX_LENGTH) {
            throw new ValueError('Campaign name cant be greater than '.self::MAX_LENGTH.' characters');
        }
    }
}
