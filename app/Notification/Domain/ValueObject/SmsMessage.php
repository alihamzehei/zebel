<?php

declare(strict_types=1);

namespace App\Notification\Domain\ValueObject;

use App\Shared\Domain\ValueObject\ValueError;

class SmsMessage
{
    public const MIN_LENGTH = 2;
    public const MAX_LENGTH = 160;

    public function __construct(public readonly string $content)
    {
        if (!mb_strlen($this->content)) {
            throw new ValueError('Message cant be empty');
        }

        $this->validateLength($this->content);
    }

    private function validateLength(string $value): void
    {
        if (mb_strlen($value) <= self::MIN_LENGTH) {
            throw new ValueError('Message cant be smaller than '.self::MIN_LENGTH.' characters');
        }

        if (mb_strlen($value) >= self::MAX_LENGTH) {
            throw new ValueError('Message cant be greater than '.self::MAX_LENGTH.' characters');
        }
    }
}
