<?php

declare(strict_types=1);

namespace App\Notification\Domain\ValueObject;

class NotificationId
{
    public function __construct(public readonly string $id)
    {
    }
}
