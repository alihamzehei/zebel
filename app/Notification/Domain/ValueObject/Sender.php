<?php

declare(strict_types=1);

namespace App\Notification\Domain\ValueObject;

class Sender
{
    public function __construct(public readonly string $number)
    {
    }
}
