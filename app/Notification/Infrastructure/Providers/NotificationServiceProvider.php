<?php

namespace App\Notification\Infrastructure\Providers;

use App\Notification\Infrastructure\Repository\EloquentNotificationRepository;
use App\Notification\Infrastructure\Repository\EloquentNotifyRepository;
use App\Notification\Application\Client\EmailClient;
use App\Notification\Application\Client\KavenegarSmsClient;
use App\Notification\Application\Client\LaravelMailClient;
use App\Notification\Application\Client\LoggerEmailClient;
use App\Notification\Application\Client\LoggerSmsClient;
use App\Notification\Application\Client\SmsClient;
use App\Notification\Domain\Repository\NotificationRepository;
use App\Notification\Domain\Repository\NotifyRepository;
use Illuminate\Support\ServiceProvider;

class NotificationServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->bind(NotificationRepository::class, EloquentNotificationRepository::class);
        $this->app->bind(NotifyRepository::class, EloquentNotifyRepository::class);

        $this->app->bind(SmsClient::class, function () {
            if (app()->environment('production')) {
                return resolve(KavenegarSmsClient::class);
            }

            return resolve(LoggerSmsClient::class);
        });

        $this->app->bind(EmailClient::class, function () {
            if (app()->environment('production')) {
                return resolve(LaravelMailClient::class);
            }

            return resolve(LoggerEmailClient::class);
        });
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        $this->loadMigrationsFrom(dirname(__DIR__) . '/Migrations');
        $this->loadViewsFrom(
            dirname(__DIR__) . '/../Presentation/Views/',
            'notifications'
        );
    }
}
