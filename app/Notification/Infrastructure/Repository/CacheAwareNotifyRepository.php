<?php

declare(strict_types=1);

namespace App\Notification\Infrastructure\Repository;

use App\Notification\Domain\Model\Notice;
use App\Notification\Domain\Repository\NotifyRepository;

class CacheAwareNotifyRepository implements NotifyRepository
{
    private array $notifies = [];

    public function __construct(private readonly NotifyRepository $origin, array $cachedNotifies = [])
    {
        foreach ($cachedNotifies as $notify) {
            $this->persistToCache($notify);
        }
    }

    private function persistToCache(Notice $notify)
    {
        $this->notifies[$notify->id] = $notify;
    }

    public function seen(): array
    {
        return [];
    }

    public function findById(int $id): ?Notice
    {
        if ($this->notifies[$id]) {
            $this->notifies[$id]->meta['persisted'] = true;

            return $this->notifies[$id];
        }

        return $this->origin->findById($id);
    }

    public function findSendable(): array
    {
        return array_filter($this->notifies, function (Notice $notify): bool {
            return $notify->isSent === false;
        });
    }

    public function persist(Notice $notify): void
    {
        $this->origin->persist($notify);
        $this->persistToCache($notify);
    }

    public function nextIdentity()
    {
    }
}
