<?php

declare(strict_types=1);

namespace App\Notification\Infrastructure\Repository;

use App\Notification\Infrastructure\Model\NotifyModel;
use InvalidArgumentException;
use App\Notification\Domain\Model\EmailNotice;
use App\Notification\Domain\Model\Notice;
use App\Notification\Domain\Model\SmsNotice;
use App\Notification\Domain\Repository\NotifyRepository;

class EloquentNotifyRepository implements NotifyRepository
{
    public function nextIdentity()
    {
    }

    public function seen(): array
    {
        return [];
    }

    public function findById(int $id): ?Notice
    {
        return NotifyModel::query()->find($id)?->toDomain();
    }

    public function findSendable(): array
    {
        return array_map(
            [$this, 'createNotifyFromArray'],
            NotifyModel::query()->where('is_sent', 0)->lock('FOR UPDATE SKIP LOCKED')->limit(100)->get()->toArray()
        );
    }

    private function createNotifyFromArray(array $notify): Notice
    {
        return match ($notify['type']) {
            SmsNotice::TYPE => new SmsNotice($notify['id'],
                                   $notify['notification_id'],
                                   $notify['checkpoint_id'],
                                   $notify['schedule_id'],
                                   $notify['target_id'],
                                   $notify['type'],
                                   (bool) $notify['is_sent']),

            EmailNotice::TYPE => new EmailNotice($notify['id'],
                                       $notify['notification_id'],
                                       $notify['checkpoint_id'],
                                       $notify['schedule_id'],
                                       $notify['target_id'],
                                       $notify['type'],
                                       (bool) $notify['is_sent']),

            default => throw new InvalidArgumentException('Unknown notify type '.$notify['type'])
        };
    }

    public function persist(Notice $notify): void
    {
        if (isset($notify->meta['persisted']) && $notify->meta['persisted'] === true) {
            $this->update($notify);

            return;
        }

        NotifyModel::query()->create([
            'notification_id' => $notify->notificationId,
            'checkpoint_id' => $notify->checkpointId,
            'schedule_id' => $notify->scheduleId,
            'target_id' => $notify->targetId,
            'type' => $notify->type,
            'is_sent' => (int) $notify->isSent,
            'created_at' => date('Y-m-d H:i:s'),
        ]);
    }

    private function update(Notice $notify): void
    {
        NotifyModel::query()->find($notify->id)->update(['is_sent' => true]);
    }
}
