<?php

declare(strict_types=1);

namespace App\Notification\Infrastructure\Repository;

use App\Notification\Infrastructure\Model\NotificationModel;
use Ramsey\Uuid\Uuid;
use App\Notification\Domain\Model\Notification;
use App\Notification\Domain\Repository\NotificationRepository;
use App\Notification\Domain\ValueObject\NotificationId;

class EloquentNotificationRepository implements NotificationRepository
{
    private array $seen = [];

    public function nextIdentity(): NotificationId
    {
        return new NotificationId(Uuid::uuid4()->toString());
    }

    public function findById(NotificationId $id): ?Notification
    {
        return NotificationModel::query()->where('uuid', $id->id)->first()?->toDomain();
    }

    public function seen(): array
    {
        return $this->seen;
    }

    public function persist(Notification $notification): void
    {
        NotificationModel::query()->updateOrCreate(['id' => $notification->id->id], [
            'uuid' => $notification->id->id,
            'journey_id' => $notification->journeyId,
            'type' => $notification->channel->type(),
            'channel' => $notification->channel,
        ]);

        $this->seen[] = $notification;
    }
}
