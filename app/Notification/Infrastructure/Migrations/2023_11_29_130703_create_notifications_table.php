<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('notification_notifications', function (Blueprint $table) {
            $table->id();
            $table->uuid();
            $table->uuid('journey_id')->nullable();
            $table->string('type');
            $table->json('channel');
            $table->timestamps();
        });

        Schema::create('notification_logs', function (Blueprint $table) {
            $table->id();
            $table->string('content');
            $table->string('type');
            $table->json('payload');
            $table->timestamps();
        });

        Schema::create('notification_notifies', function (Blueprint $table) {
            $table->id();
            $table->uuid('notification_id');
            $table->uuid('schedule_id');
            $table->uuid('checkpoint_id');
            $table->integer('target_id');
            $table->string('type');
            $table->boolean('is_sent');
            $table->timestamps();
            $table->unique(['schedule_id', 'checkpoint_id']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('notification_notifications');
        Schema::dropIfExists('notification_logs');
        Schema::dropIfExists('notification_notifies');
    }
};
