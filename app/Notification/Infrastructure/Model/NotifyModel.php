<?php

declare(strict_types=1);

namespace App\Notification\Infrastructure\Model;

use App\Notification\Domain\Model\Notice;
use Illuminate\Database\Eloquent\Model;
use App\Notification\Domain\Model\EmailNotice;
use App\Notification\Domain\Model\SmsNotice;

class NotifyModel extends Model
{
    protected $table = 'notification_notifies';

    protected $fillable = [
        'type',
        'notification_id',
        'schedule_id',
        'checkpoint_id',
        'target_id',
        'is_sent',
        'created_at',
    ];

    protected $casts = [
        'is_sent' => 'boolean',
    ];

    public function toDomain(): Notice
    {
        if ($this->type === SmsNotice::TYPE) {
            $notify = new SmsNotice($this->id,
                                    $this->notification_id,
                                    $this->checkpoint_id,
                                    $this->schedule_id,
                                    $this->target_id,
                                    SmsNotice::TYPE,
                                    (bool) $this->is_sent);
        } else {
            $notify = new EmailNotice($this->id,
                                      $this->notification_id,
                                      $this->checkpoint_id,
                                      $this->schedule_id,
                                      $this->target_id,
                                      EmailNotice::TYPE,
                                      (bool) $this->is_sent);
        }

        $notify->meta['persisted'] = true;

        return $notify;
    }
}
