<?php

declare(strict_types=1);

namespace App\Notification\Infrastructure\Model;

use App\Plan\Infrastructure\Model\JourneyModel;
use Exception;
use Illuminate\Database\Eloquent\Model;
use App\Notification\Domain\Model\Channel\Channel;
use App\Notification\Domain\Model\Channel\EmailChannel;
use App\Notification\Domain\Model\Channel\PushChannel;
use App\Notification\Domain\Model\Channel\SmsChannel;
use App\Notification\Domain\Model\Channel\WebPushChannel;
use App\Notification\Domain\Model\Notification;
use App\Notification\Domain\ValueObject\CampaignName;
use App\Notification\Domain\ValueObject\EmailMessage;
use App\Notification\Domain\ValueObject\NotificationId;
use App\Notification\Domain\ValueObject\PushMessage;
use App\Notification\Domain\ValueObject\Sender;
use App\Notification\Domain\ValueObject\SmsMessage;
use App\Notification\Domain\ValueObject\Title;

class NotificationModel extends Model
{
    protected $fillable = ['uuid', 'type', 'channel', 'journey_id'];

    protected $table = 'notification_notifications';

    protected $casts = [
        'channel' => 'array',
    ];

    public function toDomain(): Notification
    {
        return new Notification(
            new NotificationId($this->uuid),
            $this->getChannel(),
            $this->journey_id
        );
    }

    public function journey()
    {
        return $this->belongsTo(JourneyModel::class, 'journey_id');
    }

    private function getChannel(): ?Channel
    {
        return match ($this->type) {
            'sms' => new SmsChannel(new CampaignName($this->channel['campaignName']['persian']),
                new Sender($this->channel['sender']['number']),
                new SmsMessage($this->channel['message']['content']),
                $this->channel['event']
            ),

            'email' => new EmailChannel(new CampaignName($this->channel['campaignName']['persian']),
                $this->channel['senderEmail'],
                $this->channel['senderName'],
                $this->channel['subject'],
                new EmailMessage($this->channel['message']['content']),
                $this->channel['attachment'],
                $this->channel['canReply'],
                $this->channel['event']
            ),

            'push' => new PushChannel($this->channel['campaignName'],
                $this->channel['title'],
                $this->channel['message'],
                $this->channel['image']),

            'webPush' => new WebPushChannel(new Title($this->channel['title']['persian']),
                new PushMessage($this->channel['message']['content']),
                $this->channel['link'],
                new CampaignName($this->channel['campaignName']['persian']),
                $this->channel['image'],
                $this->channel['icon']),

            default => throw new Exception("Invalid channel type {$this->type}")
        };
    }
}
