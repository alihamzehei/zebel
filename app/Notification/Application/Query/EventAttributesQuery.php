<?php

declare(strict_types=1);

namespace App\Notification\Application\Query;

use App\Shared\Application\Query\QueryInterface;

class EventAttributesQuery implements QueryInterface
{
    public function __construct(public readonly string $eventName)
    {
    }
}
