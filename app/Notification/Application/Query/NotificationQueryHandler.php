<?php

declare(strict_types=1);

namespace App\Notification\Application\Query;

use App\Notification\Application\Dto\NotificationDto;
use App\Notification\Domain\Model\Channel\EmailChannel;
use App\Notification\Domain\Model\Channel\SmsChannel;
use App\Notification\Domain\Model\Channel\WebPushChannel;
use App\Notification\Domain\Repository\NotificationRepository;
use App\Notification\Domain\ValueObject\NotificationId;
use App\Shared\Application\Dto;
use App\Shared\Application\Error\NoQueryResult;
use App\Shared\Application\Query\QueryHandlerInterface;
use App\Shared\Application\Query\QueryInterface;

class NotificationQueryHandler implements QueryHandlerInterface
{
    public function __construct(private NotificationRepository $repository)
    {
    }

    public function handle(QueryInterface|NotificationQuery $query): array|\JsonSerializable
    {
        $notification = $this->repository->findById(new NotificationId($query->id));

        if ($notification === null) {
            throw new NoQueryResult;
        }

        $result = [
            'id' => $notification->id->id,
            'type' => $notification->channel->type(),
        ];

        if ($notification->channel instanceof SmsChannel) {
            $result = array_merge($result, [
                'campaignName' => $notification->channel->campaignName->persian,
                'message' => $notification->channel->message->content,
                'event' => $notification->channel->event
            ]);
        } elseif ($notification->channel instanceof EmailChannel) {
            $result = array_merge($result, [
                'campaignName' => $notification->channel->campaignName->persian,
                'senderEmail' => $notification->channel->senderEmail,
                'senderName' => $notification->channel->senderName,
                'message' => $notification->channel->message->content,
                'subject' => $notification->channel->subject,
                'attachment' => $notification->channel->attachment,
                'canReply' => $notification->channel->canReply,
                'event' => $notification->channel->event
            ]);
        } elseif ($notification->channel instanceof WebPushChannel) {
            $result = array_merge($result, [
                'title' => $notification->channel->title->persian,
                'pushMessage' => $notification->channel->message->content,
                'link' => $notification->channel->link,
                'campaignName' => $notification->channel->campaignName->persian,
                'image' => $notification->channel->image,
                'icon' => $notification->channel->icon,
            ]);
        }

        return $result;
    }
}
