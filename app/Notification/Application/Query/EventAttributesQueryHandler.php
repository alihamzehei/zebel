<?php

declare(strict_types=1);

namespace App\Notification\Application\Query;

use App\Plan\Domain\Model\Trigger;
use App\Shared\Application\Query\QueryHandlerInterface;
use App\Shared\Application\Query\QueryInterface;
use JsonSerializable;

class EventAttributesQueryHandler implements QueryHandlerInterface
{
    public function handle(QueryInterface|EventAttributesQuery $query): array|JsonSerializable
    {
        $customerData = ['name', 'lastname', 'email', 'mobile'];
        $productData = ['product_id', 'product_link'];

        $eventAttributesMap = [
            Trigger::CustomerRegistered->name => $customerData,
            Trigger::CartCreated->name => [...$customerData, 'cart_id', ...$productData],
            Trigger::ProductSeen->name => [...$customerData, ...$productData],
            Trigger::ProductAddedToWishList->name => [...$customerData, ...$productData],
            Trigger::ProductRemovedFromWishList->name => [...$customerData, ...$productData],
            Trigger::CartRemoved->name => [...$customerData, 'cart_id'],
            Trigger::OrderPaid->name => [...$customerData, 'order_id'],
            Trigger::OrderStored->name => [...$customerData, 'order_id'],
            Trigger::OrderLineCanceled->name => [...$customerData, 'order_id', 'order_item_id', 'cancel_reason', ...$productData],
        ];

        return $eventAttributesMap[$query->eventName];
    }
}
