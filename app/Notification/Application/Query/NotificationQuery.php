<?php

declare(strict_types=1);

namespace App\Notification\Application\Query;

use App\Shared\Application\Query\QueryInterface;

class NotificationQuery implements QueryInterface
{
    public function __construct(public readonly string $id)
    {
    }
}
