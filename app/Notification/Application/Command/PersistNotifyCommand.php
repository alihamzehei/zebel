<?php

declare(strict_types=1);

namespace App\Notification\Application\Command;

use App\Shared\Application\Command\CommandInterface;

class PersistNotifyCommand implements CommandInterface
{
    public function __construct(public readonly string $notificationId,
                                public readonly int $targetId,
                                public readonly string $scheduleId,
                                public readonly string $checkpointId)
    {
    }
}
