<?php

declare(strict_types=1);

namespace App\Notification\Application\Command;

use App\Shared\Application\Command\CommandInterface;

class SendNotifyCommand implements CommandInterface
{
    public function __construct(public readonly int $notifyId,
                                public readonly string $notificationId,
                                public readonly int $targetId)
    {
    }
}
