<?php

declare(strict_types=1);

namespace App\Notification\Application\Command;

use App\Shared\Application\Command\CommandInterface;
use App\Notification\Domain\Model\Channel\SmsChannel;
use App\Notification\Domain\Model\Channel\PushChannel;
use App\Notification\Domain\Model\Channel\EmailChannel;
use App\Notification\Domain\Model\Channel\WebPushChannel;
use App\Shared\Application\Error\InvalidCommandSyntaxException;

class CreateNotificationCommand implements CommandInterface
{
    private const CHANNEL_TYPES = [
        SmsChannel::TYPE,
        EmailChannel::TYPE,
        PushChannel::TYPE,
        WebPushChannel::TYPE,
    ];

    public function __construct(public readonly array $channel)
    {
        $this->validateChannel();
    }

    private function validateChannel(): void
    {
        $this->validateChannelType($this->channel['type']);

        switch ($this->channel['type']) {
            case SmsChannel::TYPE:
                $this->validateSms($this->channel);
                break;
            case EmailChannel::TYPE:
                $this->validateEmail($this->channel);
                break;
            case PushChannel::TYPE:
                $this->validatePush($this->channel);
                break;
            case WebPushChannel::TYPE:
                $this->validateWebPush($this->channel);
                break;
            default:
                break;
        }
    }

    private function validateChannelType(string $type): void
    {
        if (!\in_array($type, self::CHANNEL_TYPES, true)) {
            throw new InvalidCommandSyntaxException("No such channel type $type");
        }
    }

    private function validateSms(array $sms): void
    {
        if (!isset($sms['campaignName']) || !isset($sms['message']) || !isset($sms['event'])) {
            throw new InvalidCommandSyntaxException('Sms must contains campaignName and, message');
        }
    }

    private function validateEmail(array $email): void
    {
        if (!isset($email['campaignName']) ||
            !isset($email['senderEmail']) ||
            !isset($email['senderName']) ||
            !isset($email['subject']) ||
            !isset($email['event']) ||
            !isset($email['message'])) {
            throw new InvalidCommandSyntaxException('Web push must contains campaignName, senderEmail, senderName, subject and, message');
        }
    }

    private function validatePush(array $push): void
    {
        if (!isset($push['campaignName']) ||
            !isset($push['title']) ||
            !isset($push['pushMessage'])) {
            throw new InvalidCommandSyntaxException('Web push must contains campaignName, title and, pushMessage');
        }
    }

    private function validateWebPush(array $webPush): void
    {
        if (!isset($webPush['link']) ||
            !isset($webPush['title']) ||
            !isset($webPush['pushMessage'])) {
            throw new InvalidCommandSyntaxException('Web push must contains link, title and, pushMessage');
        }
    }
}
