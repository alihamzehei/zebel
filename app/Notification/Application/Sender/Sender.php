<?php

declare(strict_types=1);

namespace App\Notification\Application\Sender;

use App\Notification\Domain\Model\Channel\Channel;

interface Sender
{
    public function send($target, Channel $channel, array $messagePayload = []): void;
}
