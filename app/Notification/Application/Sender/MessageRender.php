<?php

declare(strict_types=1);

namespace App\Notification\Application\Sender;

trait MessageRender
{
    protected function renderMessage(string $unprocessedMessage, array $replacementArrayMap): string
    {
        if (\array_key_exists('product_id', $replacementArrayMap)) {
            $replacementArrayMap = ['product_link' => $this->createHyperlink($replacementArrayMap), ...$replacementArrayMap];
        }

        return preg_replace_callback(
            '~\{{\$(.*?)\}}~si',
            fn ($match) => str_replace($match[0],
                isset($replacementArrayMap[$match[1]])
                    ? (string) $replacementArrayMap[$match[1]]
                    : '-',
                $match[0]),
            $unprocessedMessage
        );
    }

    private function createHyperlink(array $data): string
    {
        $queryStringData = [
            'utm_medium' => $data['type'],
            'utm_source' => 'automation',
            'utm_campaign' => $data['campaignName'],
            'utm_item' => 'mlt-'.$data['product_id'],
        ];
        $queryString = http_build_query($queryStringData);

        return "https://malltina.com/product/{$queryStringData['utm_item']}?{$queryString}";
    }
}
