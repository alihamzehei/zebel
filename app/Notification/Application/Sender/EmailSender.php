<?php

declare(strict_types=1);

namespace App\Notification\Application\Sender;

use App\Notification\Application\Client\EmailClient;
use App\Notification\Domain\Model\Channel\Channel;
use App\Notification\Domain\Model\Channel\EmailChannel;

class EmailSender implements Sender
{
    use MessageRender;

    public function __construct(private readonly EmailClient $client)
    {
    }

    public function send($target, Channel|EmailChannel $channel, array $messagePayload = []): void
    {
        $this->client->send(
            $channel->senderEmail,
            $target['email'],
            $channel->subject,
            $this->renderMessage($channel->message->content, [...$target, ...$messagePayload]),
            $channel->attachment ?? '',
            $channel->canReply ?? ''
        );
    }
}
