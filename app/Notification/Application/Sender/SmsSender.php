<?php

declare(strict_types=1);

namespace App\Notification\Application\Sender;

use App\Notification\Application\Client\SmsClient;
use App\Notification\Domain\Model\Channel\Channel;
use App\Notification\Domain\Model\Channel\SmsChannel;

class SmsSender implements Sender
{
    use MessageRender;

    public function __construct(private readonly SmsClient $client)
    {
    }

    public function send($target, Channel|SmsChannel $channel, array $messagePayload = []): void
    {
        $this->client->send(
            $channel->sender->number,
            $target['mobile'],
            $this->renderMessage($channel->message->content, [...$target, ...$messagePayload])
        );
    }
}
