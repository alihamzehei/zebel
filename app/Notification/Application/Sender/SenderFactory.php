<?php

declare(strict_types=1);

namespace App\Notification\Application\Sender;

use InvalidArgumentException;
use App\Notification\Domain\Model\Channel\Channel;
use App\Notification\Domain\Model\Channel\EmailChannel;
use App\Notification\Domain\Model\Channel\SmsChannel;

class SenderFactory
{
    public function createSenderForTarget(Channel $channel, ?int $targetId): Sender
    {
        return match (\get_class($channel)) {
            SmsChannel::class => app(SmsSender::class),

            EmailChannel::class => app(EmailSender::class),

            // PushChannel::class => new PushSender(),

            // WebPushChannel::class => new WebPushSender(),

            default => throw new InvalidArgumentException("Invalid channel type {$channel->type()}")
        };
    }
}
