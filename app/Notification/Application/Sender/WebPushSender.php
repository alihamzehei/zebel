<?php

declare(strict_types=1);

namespace App\Notification\Application\Sender;

use App\Notification\Application\Client\WebPushClient;
use App\Notification\Domain\Model\Channel\Channel;

class WebPushSender implements Sender
{
    public function __construct(private WebPushClient $webPushClient)
    {
    }

    public function send($target, Channel $channel): void
    {
    }
}
