<?php

declare(strict_types=1);

namespace App\Notification\Application\Sender;

use App\Notification\Application\Client\PushClient;
use App\Notification\Domain\Model\Channel\Channel;

class PushSender implements Sender
{
    public function __construct(private PushClient $pushClient)
    {
    }

    public function send($target, Channel $channel): void
    {
    }
}
