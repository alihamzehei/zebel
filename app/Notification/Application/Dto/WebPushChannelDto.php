<?php

declare(strict_types=1);

namespace App\Notification\Application\Dto;

use App\Notification\Domain\Model\Notification;

class WebPushChannelDto extends ChannelDto
{
    public function __construct(public readonly Notification $notification)
    {
    }

    public function asArray(): array
    {
        return [
            'type' => $this->notification->channel->type(),
            'title' => $this->notification->channel->title->persian,
            'pushMessage' => $this->notification->channel->message->content,
            'link' => $this->notification->channel->link,
            'campaignName' => $this->notification->channel->campaignName->persian,
            'image' => $this->notification->channel->image,
            'icon' => $this->notification->channel->icon,
        ];
    }
}
