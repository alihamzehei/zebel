<?php

declare(strict_types=1);

namespace App\Notification\Application\Dto;

use App\Notification\Domain\Model\Notification;

class EmailChannelDto extends ChannelDto
{
    public function __construct(public readonly Notification $notification)
    {
    }

    public function asArray(): array
    {
        return [
            'type' => $this->notification->channel->type(),
            'campaignName' => $this->notification->channel->campaignName->persian,
            'senderEmail' => $this->notification->channel->senderEmail,
            'senderName' => $this->notification->channel->senderName,
            'message' => $this->notification->channel->message->content,
            'subject' => $this->notification->channel->subject,
            'attachment' => $this->notification->channel->attachment,
            'canReply' => $this->notification->channel->canReply,
        ];
    }
}
