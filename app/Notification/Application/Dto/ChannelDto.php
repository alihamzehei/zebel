<?php

declare(strict_types=1);

namespace App\Notification\Application\Dto;

use App\Shared\Application\Dto;

abstract class ChannelDto extends Dto
{
}
