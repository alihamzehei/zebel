<?php

declare(strict_types=1);

namespace App\Notification\Application\Dto;

use App\Notification\Domain\Model\Channel\EmailChannel;
use App\Notification\Domain\Model\Channel\SmsChannel;
use App\Notification\Domain\Model\Channel\WebPushChannel;
use App\Notification\Domain\Model\Notification;

class ChannelDtoFactory
{
    public static function from(Notification $notification): ChannelDto
    {
        return (new self())->createDto($notification);
    }

    public function createDto(Notification $notification): ChannelDto
    {
        return match (\get_class($notification->channel)) {
            SmsChannel::class => new SmsChannelDto($notification),
            EmailChannel::class => new EmailChannelDto($notification),
            WebPushChannel::class => new WebPushChannelDto($notification),
        };
    }
}
