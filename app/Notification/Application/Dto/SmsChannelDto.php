<?php

declare(strict_types=1);

namespace App\Notification\Application\Dto;

use App\Notification\Domain\Model\Notification;

class SmsChannelDto extends ChannelDto
{
    public function __construct(public readonly Notification $notification)
    {
    }

    public function asArray(): array
    {
        return [
            'type' => $this->notification->channel->type(),
            'campaignName' => $this->notification->channel->campaignName->persian,
            'message' => $this->notification->channel->message->content,
        ];
    }
}
