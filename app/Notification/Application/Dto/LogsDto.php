<?php

declare(strict_types=1);

namespace App\Notification\Application\Dto;

use App\Shared\Application\Dto;

class LogsDto extends Dto
{
    public function __construct(public readonly array $logs)
    {
    }

    public function asArray(): array
    {
        return [
            'total_page' => $this->logs['total_page'],
            'page_number' => $this->logs['page_number'],
            'logs' => array_map(fn ($logs): array => [
                'content' => $logs['content'],
                'type' => $logs['type'],
                'payload' => json_decode($logs['payload'], true),
                'created_at' => $logs['created_at'] ? $logs['created_at'] : null,
            ], $this->logs['items']),
        ];
    }
}
