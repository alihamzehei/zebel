<?php

declare(strict_types=1);

namespace App\Notification\Application\Dto;

use App\Notification\Domain\Model\Notification;
use App\Shared\Application\Dto;

class NotificationDto extends Dto
{
    public function __construct(public readonly Notification $notification)
    {
    }

    public function asArray(): array
    {
        return [
            'id' => $this->notification->id->id,
            'channel' => ChannelDtoFactory::from($this->notification),
        ];
    }
}
