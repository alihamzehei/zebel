<?php

declare(strict_types=1);

namespace App\Notification\Application\Dto;

use App\Shared\Application\Dto;

class EventAttributesDto extends Dto
{
    public function __construct(public readonly array $attributes)
    {
    }

    public function asArray(): array
    {
        return $this->attributes;
    }
}
