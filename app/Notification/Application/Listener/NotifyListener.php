<?php

namespace App\Notification\Application\Listener;

use App\Notification\Application\Sender\SenderFactory;
use App\Notification\Domain\Repository\NotificationRepository;
use App\Notification\Domain\ValueObject\NotificationId;
use App\Schedule\Domain\Event\ActionCheckpointReached;
use App\Target\Presentation\Module\TargetModule;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Throwable;

class NotifyListener implements ShouldQueue
{
    use InteractsWithQueue;

    public function __construct(
        private readonly SenderFactory $factory,
        private readonly NotificationRepository $repository,
        private readonly TargetModule $targets,
    )
    {

    }

    public function handle(ActionCheckpointReached $event): void
    {
        $notification = $this->repository->findById(new NotificationId($event->notificationId));

        if ($notification === null) {
            return;
        }

        try {
            $target = $this->targets->getTargetById($event->targetId);
        } catch (Throwable) {
            return;
        }

        $this->factory
            ->createSenderForTarget($notification->channel, $event->targetId)
            ->send($target, $notification->channel, []);
    }
}
