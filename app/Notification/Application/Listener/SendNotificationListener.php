<?php

namespace App\Notification\Application\Listener;

use App\Notification\Application\Sender\SenderFactory;
use App\Notification\Domain\Model\Channel\SmsChannel;
use App\Notification\Domain\Repository\NotificationRepository;
use App\Notification\Domain\ValueObject\NotificationId;
use App\Schedule\Domain\Event\ActionCheckpointReached;
use App\Target\Presentation\Module\TargetModule;

class SendNotificationListener
{
    public function __construct(
        private readonly NotificationRepository $repository,
        private readonly TargetModule $targets,
        private readonly SenderFactory $factory,
    )
    {
    }

    public function handle(ActionCheckpointReached $event): void
    {
        $notification = $this->repository->findById(new NotificationId($event->notificationId));

        if ($notification === null) {
            return;
        }

        $target = $this->targets->getTargetById($event->targetId);

        if (empty($target)) {
            return;
        }

        if (get_class($notification->channel) === SmsChannel::class && !$target['mobile']) {
            return;
        }

        $messageData = [
            'type' => $notification->channel->type(),
            'campaignName' => $notification->channel->campaignName->persian,
            ...$event->activities[array_key_last($event->activities)]->payload,
        ];

        $this->factory
            ->createSenderForTarget($notification->channel, $event->targetId)
            ->send($target, $notification->channel, $messageData);
    }
}
