<?php

declare(strict_types=1);

namespace App\Notification\Application\Listener;

use App\Plan\Domain\Event\JourneyPlanned;
use App\Notification\Domain\Model\Notification;
use App\Notification\Domain\Repository\NotificationRepository;
use App\Notification\Domain\ValueObject\NotificationId;

class PlanJourneyListener
{
    public function __construct(private readonly NotificationRepository $repository)
    {
    }

    public function handle(JourneyPlanned $event): void
    {
        foreach ($event->checkpoints as $checkpoint) {
            if ($checkpoint['type'] === 'action') {
                $notification = $this->repository->findById(new NotificationId($checkpoint['payload']['notification_id']));

                $updatedNotification = new Notification(
                    $notification->id,
                    $notification->channel,
                    $event->id
                );

                $this->repository->persist($updatedNotification);
            }
        }
    }
}
