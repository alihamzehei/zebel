<?php

declare(strict_types=1);

namespace App\Notification\Application\Handler;

use App\Notification\Domain\Model\Channel\SmsChannel;
use App\Target\Presentation\Module\TargetModule;
use App\Shared\Application\Error\ApplicationError;
use App\Shared\Application\Command\CommandInterface;
use App\Notification\Application\Sender\SenderFactory;
use App\Notification\Domain\ValueObject\NotificationId;
use App\Notification\Application\Command\SendNotifyCommand;
use App\Shared\Application\Command\CommandHandlerInterface;
use App\Notification\Domain\Repository\NotificationRepository;
use App\Notification\Domain\Repository\NotifyRepository;

class SendNotifyHandler implements CommandHandlerInterface
{
    public function __construct(public readonly SenderFactory $senderFactory,
                                private readonly TargetModule $target,
                                private readonly NotifyRepository $notifies,
                                private readonly NotificationRepository $notifications)
    {
    }

    /**
     * @throws ApplicationError
     */
    public function handle(CommandInterface|SendNotifyCommand $command): void
    {
        $notification = $this->notifications->findById(new NotificationId($command->notificationId));

        $target = $this->target->getTargetById($command->targetId);

        if (!$target) {
            throw new ApplicationError(sprintf('No such target with id %d', $command->targetId));
        }

        if ($notification === null) {
            throw new ApplicationError("No such notification [$command->notificationId]");
        }

        if (get_class($notification->channel) === SmsChannel::class && !$target['mobile']) {
            return;
        }

        $this->senderFactory
            ->createSenderForTarget($notification->channel, $target['id'])
            ->send($target, $notification->channel);

        $notify = $this->notifies->findById($command->notifyId);

        $this->notifies->persist($notify);
    }
}
