<?php

declare(strict_types=1);

namespace App\Notification\Application\Handler;

use App\Notification\Domain\Model\Channel\SmsChannel;
use App\Notification\Domain\Model\SmsNotice;
use App\Notification\Domain\Model\EmailNotice;
use App\Target\Presentation\Module\TargetModule;
use App\Shared\Application\Error\ApplicationError;
use App\Shared\Application\Command\CommandInterface;
use App\Notification\Application\Sender\SenderFactory;
use App\Notification\Domain\ValueObject\NotificationId;
use App\Notification\Domain\Repository\NotifyRepository;
use App\Shared\Application\Command\CommandHandlerInterface;
use App\Notification\Application\Command\PersistNotifyCommand;
use App\Notification\Domain\Repository\NotificationRepository;

class PersistNotifyHandler implements CommandHandlerInterface
{
    public function __construct(public SenderFactory $senderFactory,
                                private TargetModule $target,
                                private NotifyRepository $notifies,
                                private NotificationRepository $notifications)
    {
    }

    public function handle(CommandInterface|PersistNotifyCommand $command): void
    {
        $notification = $this->notifications->findById(new NotificationId($command->notificationId));

        $target = $this->target->getTargetById($command->targetId);

        if (!$target) {
            throw new ApplicationError(sprintf('No such target with id %d', $command->targetId));
        }

        if ($notification === null) {
            throw new ApplicationError("No such notification [$command->notificationId]");
        }

        if (\get_class($notification->channel) === SmsChannel::class) {
            $notify = new SmsNotice(null, $notification->id->id, $command->checkpointId, $command->scheduleId, $command->targetId, SmsNotice::TYPE, false);
        } else {
            $notify = new EmailNotice(null, $notification->id->id, $command->checkpointId, $command->scheduleId, $command->targetId, EmailNotice::TYPE, false);
        }

        $this->notifies->persist($notify);
    }
}
