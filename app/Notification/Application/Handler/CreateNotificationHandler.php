<?php

declare(strict_types=1);

namespace App\Notification\Application\Handler;

use App\Notification\Application\Command\CreateNotificationCommand;
use App\Notification\Domain\Model\Channel\EmailChannel;
use App\Notification\Domain\Model\Channel\PushChannel;
use App\Notification\Domain\Model\Channel\SmsChannel;
use App\Notification\Domain\Model\Channel\WebPushChannel;
use App\Notification\Domain\Model\Notification;
use App\Notification\Domain\Repository\NotificationRepository;
use App\Notification\Domain\ValueObject\CampaignName;
use App\Notification\Domain\ValueObject\EmailMessage;
use App\Notification\Domain\ValueObject\PushMessage;
use App\Notification\Domain\ValueObject\Sender;
use App\Notification\Domain\ValueObject\SmsMessage;
use App\Notification\Domain\ValueObject\Title;
use App\Shared\Application\Command\CommandHandlerInterface;
use App\Shared\Application\Command\CommandInterface;
use App\Shared\Application\Error\InvalidCommandSyntaxException;

class CreateNotificationHandler implements CommandHandlerInterface
{
    public function __construct(private readonly NotificationRepository $repository)
    {
    }

    public function handle(CommandInterface|CreateNotificationCommand $command): string
    {
        $channel = match ($command->channel['type']) {
            'sms' => new SmsChannel(new CampaignName($command->channel['campaignName']),
                new Sender($command->channel['sender'] ?? ''),
                new SmsMessage($command->channel['message']),
                $command->channel['event']
            ),

            'email' => new EmailChannel(new CampaignName($command->channel['campaignName']),
                $command->channel['senderEmail'],
                $command->channel['senderName'],
                $command->channel['subject'],
                new EmailMessage($command->channel['message']),
                $command->channel['attachment'],
                $command->channel['canReplied'],
                $command->channel['event']
            ),

            'push' => new PushChannel(new CampaignName($command->channel['campaignName']),
                new Title($command->channel['title']),
                new PushMessage($command->channel['pushMessage']),
                $command->channel['image']
            ),

            'webPush' => new WebPushChannel(new Title($command->channel['title']),
                new PushMessage($command->channel['pushMessage']),
                $command->channel['link'],
                new CampaignName($command->channel['campaignName']),
                $command->channel['image'],
                $command->channel['icon']
            ),

            default => throw new InvalidCommandSyntaxException('Invalid channel type')
        };

        $notification = new Notification($this->repository->nextIdentity(), $channel);

        $this->repository->persist($notification);

        return $notification->id->id;
    }
}
