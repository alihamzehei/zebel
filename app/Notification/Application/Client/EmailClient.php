<?php

declare(strict_types=1);

namespace App\Notification\Application\Client;

interface EmailClient
{
    public function send(string $from,
                        string $to,
                        string $subject,
                        string $message,
                        string $attachment,
                        bool $canReply): void;
}
