<?php

declare(strict_types=1);

namespace App\Notification\Application\Client;

use Illuminate\Support\Facades\DB;

class LoggerEmailClient implements EmailClient
{
    public function send(string $from,
                        string $to,
                        string $subject,
                        string $message,
                        string $attachment,
                        bool $canReply): void
    {
        DB::table('notification_logs')->insert([
            'content' => 'email sent',
            'type' => 'email',
            'payload' => json_encode([
                'from' => $from,
                'to' => $to,
                'message' => $message,
                'attachment' => $attachment,
                'canReply' => $canReply,
            ]),
            'created_at' => date('Y-m-d H:i:s'),
        ]);
    }
}
