<?php

declare(strict_types=1);

namespace App\Notification\Application\Client;

use HoomanMirghasemi\Sms\Facades\Sms;
use Kavenegar\Exceptions\ApiException;
use Kavenegar\Exceptions\HttpException;

class KavenegarSmsClient implements SmsClient
{
    public function __construct()
    {
    }


    public function send(string $sender, string $receptor, string $message): void
    {
        try {
            Sms::driver('kavenegar')->to($receptor)->message($message)->send();
        } catch (ApiException|HttpException $e) {
            echo $e->errorMessage();
        }
    }
}
