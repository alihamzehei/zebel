<?php

namespace App\Notification\Application\Client;

use App\Notification\Application\Client\Mail\GeneralMail;
use Exception;
use Mail;

class LaravelMailClient implements EmailClient
{

    public function send(
        string $from,
        string $to,
        string $subject,
        string $message,
        string $attachment,
        bool   $canReply
    ): void
    {
        try {
            Mail::to($to)->send(new GeneralMail(compact(
                'from',
                'to',
                'subject',
                'attachment',
                'canReply',
                'message'
            )));
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}
