<?php

declare(strict_types=1);

namespace App\Notification\Application\Client;

interface SmsClient
{
    public function send(string $sender, string $receptor, string $message): void;
}
