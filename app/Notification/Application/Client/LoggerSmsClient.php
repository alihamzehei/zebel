<?php

declare(strict_types=1);

namespace App\Notification\Application\Client;

use Illuminate\Support\Facades\DB;

class LoggerSmsClient implements SmsClient
{
    public function send(string $sender, string $receptor, string $message): void
    {
        DB::table('notification_logs')->insert([
            'content' => 'email sent',
            'type' => 'email',
            'payload' => json_encode([
                'sender' => $sender,
                'receptor' => $receptor,
                'message' => $message,
            ]),
            'created_at' => date('Y-m-d H:i:s'),
        ]);
    }
}
