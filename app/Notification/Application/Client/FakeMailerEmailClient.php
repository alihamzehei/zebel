<?php

declare(strict_types=1);

namespace App\Notification\Application\Client;

class FakeMailerEmailClient implements EmailClient
{
    public function send(string $form,
                        string $to,
                        string $subject,
                        string $message,
                        string $attachment,
                        bool $canReply): void
    {
    }
}
