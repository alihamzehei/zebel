<?php

declare(strict_types=1);

namespace App\Notification\Application\Client;

class FakeSmsClient implements SmsClient
{
    public function send(string $sender, string $receptor, string $message): void
    {
    }
}
