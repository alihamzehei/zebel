<?php

declare(strict_types=1);

namespace App\Shared\Presentation\Exceptions;

use App\KYC\Domain\Error\EmailNotVerified;
use App\KYC\Domain\Error\TokenNotFound;
use App\KYC\Domain\Error\TraderNotAuthorized;
use App\KYC\Domain\Error\TraderNotFound;
use App\Shared\Application\Error\ApplicationError;
use App\Shared\Application\Query\NoQueryResultError;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of exception types with their corresponding custom log levels.
     *
     * @var array<class-string<Throwable>, \Psr\Log\LogLevel::*>
     */
    protected $levels = [
    ];

    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<Throwable>>
     */
    protected $dontReport = [
    ];

    /**
     * A list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (TokenNotFound|NoQueryResultError $e) {
            abort(404, $e->getMessage());
        });

        $this->reportable(function (TraderNotAuthorized|TraderNotFound|EmailNotVerified $e) {
            abort(401);
        });

        $this->reportable(fn (ApplicationError $e) => abort(400, $e->getMessage()));

        $this->reportable(function (Throwable $e) {
            if (app()->bound('sentry')) {
                app('sentry')->captureException($e);
            }
        });
    }
}
