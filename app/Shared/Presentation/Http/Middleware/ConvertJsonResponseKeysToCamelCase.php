<?php

declare(strict_types=1);

namespace App\Shared\Presentation\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ConvertJsonResponseKeysToCamelCase
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     *
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->is(ltrim(config(
            'openapi.collections.default.route.uri'
        ), '/'))) {
            return $next($request);
        }

        /** @var \Illuminate\Http\Response $response */
        $response = $next($request);

        if (!Str::startsWith($response->headers->get('content-type'), 'application/json')) {
            return $response;
        }

        $json = @json_decode($response->getContent(), true);
        if (!$json) {
            return $response;
        }

        $response->setContent(
            json_encode($this->replaceResponseKeys($json))
        );

        return $response;
    }

    private function replaceResponseKeys(array $json): array
    {
        $replaced = [];
        foreach ($json as $key => $value) {
            if (\is_array($value)) {
                $value = $this->replaceResponseKeys($value);
            }
            $replaced[
                \is_string($key) ? Str::camel($key) : $key
            ] = $value;
        }

        return $replaced;
    }
}
