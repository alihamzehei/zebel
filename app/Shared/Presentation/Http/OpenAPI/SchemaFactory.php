<?php

declare(strict_types=1);

namespace App\Shared\Presentation\Http\OpenAPI;

use GoldSpecDigital\ObjectOrientedOAS\Objects\Schema;

class SchemaFactory
{
    public static function paginationLinks(): array
    {
        return [
            Schema::string('first')->example('https://exmpl.com/api/tickets?page=1'),
            Schema::string('last')->example('https://exmpl.com/api/tickets?page=10'),
            Schema::string('prev')->nullable()->example('https://exmpl.com/api/tickets?page=2'),
            Schema::string('next')->nullable()->example('https://exmpl.com/api/tickets?page=4'),
        ];
    }

    public static function paginationMeta(): array
    {
        return [
            Schema::integer('currentPage'),
            Schema::integer('from'),
            Schema::integer('lastPage'),
            Schema::array('links')->items(
                Schema::object()->properties(
                    Schema::string('url'),
                    Schema::string('label'),
                    Schema::boolean('active'),
                )
            ),
            Schema::string('path'),
            Schema::integer('perPage'),
            Schema::integer('to'),
            Schema::integer('total'),
        ];
    }
}
