<?php

declare(strict_types=1);

namespace App\Shared\Presentation\Http\OpenAPI\Responses;

use GoldSpecDigital\ObjectOrientedOAS\Contracts\SchemaContract;
use GoldSpecDigital\ObjectOrientedOAS\Objects\MediaType;
use GoldSpecDigital\ObjectOrientedOAS\Objects\Response;
use GoldSpecDigital\ObjectOrientedOAS\Objects\Schema;
use Vyuldashev\LaravelOpenApi\Factories\ResponseFactory;

abstract class PaginatedResponse extends ResponseFactory
{
    public function build(): Response
    {
        return Response::ok()->content(
            MediaType::json()->schema(
                Schema::object()->properties(
                    $this->buildData(),
                    Schema::object('pages')->properties(...$this->paginationLinks()),
                    Schema::object('meta')->properties(...$this->paginationMeta()),
                )
            )
        );
    }

    abstract public function buildData(): SchemaContract;

    protected function paginationLinks(): array
    {
        return [
            Schema::string('first')->example('https://exmpl.com/api/tickets?page=1'),
            Schema::string('last')->example('https://exmpl.com/api/tickets?page=10'),
            Schema::string('prev')->nullable()->example('https://exmpl.com/api/tickets?page=2'),
            Schema::string('next')->nullable()->example('https://exmpl.com/api/tickets?page=4'),
        ];
    }

    protected function paginationMeta(): array
    {
        return [
            Schema::integer('currentPage'),
            Schema::integer('from'),
            Schema::integer('lastPage'),
            Schema::array('links')->items(
                Schema::object()->properties(
                    Schema::string('url'),
                    Schema::string('label'),
                    Schema::boolean('active'),
                )
            ),
            Schema::string('path'),
            Schema::integer('perPage'),
            Schema::integer('to'),
            Schema::integer('total'),
        ];
    }
}
