<?php

declare(strict_types=1);

namespace App\Shared\Presentation\Http\OpenAPI\Responses;

use GoldSpecDigital\ObjectOrientedOAS\Objects\Response;
use Vyuldashev\LaravelOpenApi\Factories\ResponseFactory;

class NoContentResponse extends ResponseFactory
{
    public function build(): Response
    {
        return Response::create('noContent')
            ->statusCode(204)
            ->description('No content');
    }
}
