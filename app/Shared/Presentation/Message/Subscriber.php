<?php

namespace App\Shared\Presentation\Message;


use App\Shared\Domain\DomainEvent;

abstract class Subscriber
{
    abstract public function __invoke(DomainEvent $event): void;
}
