<?php

namespace App\Shared\Presentation\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Log;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     */
    protected function schedule(Schedule $schedule): void
    {
         $schedule->command('app:schedule:dispatch')->hourly();
    }

    /**
     * Register the commands for the application.
     */
    protected function commands(): void
    {
        $this->load(
            app_path('Collect/Presentation/Console')
        );

        $this->load(
            app_path('Schedule/Presentation/Console')
        );

        $this->load(
            app_path('Notification/Presentation/Console')
        );

        require base_path('routes/console.php');
    }
}
