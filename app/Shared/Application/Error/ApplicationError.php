<?php

declare(strict_types=1);

namespace App\Shared\Application\Error;

use Exception;

class ApplicationError extends Exception
{
}
