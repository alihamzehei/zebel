<?php

declare(strict_types=1);

namespace App\Shared\Application\Bus\Command;

use App\Shared\Application\Command\CommandHandlerInterface;
use App\Shared\Application\Command\CommandInterface;

interface CommandRepository
{
    public function handlerExistsFor(CommandInterface $command): bool;

    public function getHandlerFor(CommandInterface $command): CommandHandlerInterface;
}
