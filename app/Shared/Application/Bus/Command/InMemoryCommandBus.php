<?php

declare(strict_types=1);

namespace App\Shared\Application\Bus\Command;

use App\Shared\Application\Command\CommandBus;
use App\Shared\Application\Command\CommandHandlerInterface;
use App\Shared\Application\Command\CommandInterface;
use App\Shared\Application\Error\ApplicationError;
use App\Shared\Domain\DomainEvent;
use App\Shared\Domain\Repository;
use Illuminate\Support\Facades\DB;
use ReflectionClass;
use ReflectionObject;
use Throwable;

class InMemoryCommandBus implements CommandBus
{
    private const DB_TRANSACTION_ATTEMPTS = 2;

    /** @var DomainEvent[] */
    private array $events = [];

    private mixed $results = null;

    public function __construct(private readonly CommandRepository $commands)
    {
    }

    public function handle(CommandInterface|DomainEvent $input): mixed
    {
        return DB::transaction(function () use ($input) {
            $this->handleInput($input);

            return $this->results;
        }, self::DB_TRANSACTION_ATTEMPTS);
    }

    /**
     * @throws ApplicationError
     * @throws Throwable
     */
    protected function handleInput(CommandInterface|DomainEvent $input): void
    {
        if ($input instanceof CommandInterface) {
            $this->handleCommand($input);
        } else {
            $this->handleEvent($input);
        }

        while ( ! empty($this->events)) {
            $this->handleInput(collect(array_pop($this->events))->first());
        }
    }

    protected function handleCommand(CommandInterface $command): void
    {
        if ( ! $this->commands->handlerExistsFor($command)) {
            throw new ApplicationError('No handler found for command '.\get_class($command));
        }

        try {
            tap($this->commands->getHandlerFor($command), function (CommandHandlerInterface $handler) use ($command) {
                $this->results = $handler->handle($command);

                $this->appendEventsOf($handler);
            });

            logger()->info('Command handling succeed: '.$this->commandAsString($command));
        } catch (Throwable $exception) {
            logger()->info('Command handling failed: '.$this->commandAsString($command), [
                'message' => $exception->getMessage(),
            ]);
            report($exception);
            throw $exception;
        }
    }

    protected function handleEvent(DomainEvent $event): void
    {
        event($event);
    }

    private function commandAsString(CommandInterface $command): string
    {
        return (new ReflectionClass($command))->getShortName();
    }

    private function appendEventsOf(CommandHandlerInterface $handler): void
    {
        $ref = new ReflectionObject($handler);

        /** @var Repository[] $repositories */
        $repositories = array_filter(
            array_map(fn($prop) => $prop->getValue($handler), $ref->getProperties()),
            fn($dependency) => $dependency instanceof Repository
        );

        foreach ($repositories as $repository) {
            foreach ($repository->seen() as $aggregateRoot) {
                if (count($event = $aggregateRoot->pullRecordedEvents())) {
                    $this->events[] = $event;
                }
            }
        }
    }
}
