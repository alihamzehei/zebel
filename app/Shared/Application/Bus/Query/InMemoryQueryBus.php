<?php

declare(strict_types=1);

namespace App\Shared\Application\Bus\Query;

use App\Shared\Application\Error\ApplicationError;
use App\Shared\Application\Query\QueryBus;
use App\Shared\Application\Query\QueryInterface;
use JsonSerializable;
use ReflectionClass;
use Throwable;

class InMemoryQueryBus implements QueryBus
{
    public function __construct(
        private readonly QueryRepository $repository,
    )
    {
    }

    public function ask(QueryInterface $query)
    {
        if (!$this->repository->handlerExistsFor($query)) {
            throw new ApplicationError('No handler found');
        }

        try {
            $result = $this->repository->getHandlerFor($query)->handle($query);
            logger()->info('Query handling succeed: '.$this->queryAsString($query));
            return $result;
        } catch (Throwable $exception) {
            logger()->error('Query handling failed: '.$this->queryAsString($query));
            throw $exception;
        }
    }

    private function queryAsString(QueryInterface $query): string
    {
        return (new ReflectionClass($query))->getShortName();
    }
}
