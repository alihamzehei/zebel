<?php

declare(strict_types=1);

namespace App\Shared\Application\Bus\Query;

use App\Shared\Application\Query\QueryInterface;
use App\Shared\Application\Query\QueryHandlerInterface;

interface QueryRepository
{
    public function handlerExistsFor(QueryInterface $query): bool;

    public function getHandlerFor(QueryInterface $query): QueryHandlerInterface;
}
