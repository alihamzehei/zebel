<?php

declare(strict_types=1);

namespace App\Shared\Application\Bus\Guard;

interface Guarded
{
    public function actorId(): string;
}
