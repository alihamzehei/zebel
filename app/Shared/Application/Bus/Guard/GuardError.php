<?php

declare(strict_types=1);

namespace App\Shared\Application\Bus\Guard;

use App\Shared\Application\Error\ApplicationError;

class GuardError extends ApplicationError
{
}
