<?php

declare(strict_types=1);

namespace App\Shared\Application\Bus\Guard;

use App\Guard\Presentation\Module\ActorModule;
use App\Shared\Application\Command\CommandInterface;
use App\Shared\Application\Query\QueryInterface;

class Guard
{
    public const PUBLIC_ACCESS = 0;
    public const ACTOR_ACCESS = 1;
    public const GROUP_ACCESS = 2;

    public function __construct(private readonly ActorModule $actors)
    {
    }

    public function canCommandBeExecuted(CommandInterface $command): bool
    {
        if ($command instanceof Guarded) {
            return $this->actors->canActorPassTheGuard($command->actorId(), \get_class($command));
        }

        return true;
    }

    public function canQueryBeAsked(QueryInterface $query): bool
    {
        if ($query instanceof Guarded) {
            return $this->actors->canActorPassTheGuard($query->actorId(), \get_class($query));
        }

        return true;
    }
}
