<?php

declare(strict_types=1);

namespace App\Shared\Application\Dto;

use JsonSerializable;

interface DtoInterface extends JsonSerializable
{
}
