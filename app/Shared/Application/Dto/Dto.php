<?php

declare(strict_types=1);

namespace App\Shared\Application\Dto;

abstract class Dto implements DtoInterface
{
    abstract public function asArray(): array;

    public function jsonSerialize(): mixed
    {
        return $this->asArray();
    }
}
