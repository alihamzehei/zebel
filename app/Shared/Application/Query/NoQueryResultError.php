<?php

declare(strict_types=1);

namespace App\Shared\Application\Query;

use App\Shared\Application\Error\ApplicationError;

class NoQueryResultError extends ApplicationError
{
}
