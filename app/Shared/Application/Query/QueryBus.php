<?php

declare(strict_types=1);

namespace App\Shared\Application\Query;

use JsonSerializable;

interface QueryBus
{
    public function ask(QueryInterface $query);
}
