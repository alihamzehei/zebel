<?php

declare(strict_types=1);

namespace App\Shared\Application\Query;

use JsonSerializable;

interface QueryHandlerInterface
{
    /**
     * @throws \App\Shared\Application\Query\NoQueryResultError
     */
    public function handle(QueryInterface $query): JsonSerializable|array;
}
