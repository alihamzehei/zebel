<?php

declare(strict_types=1);

namespace App\Shared\Application;

use App\Shared\Domain\AggregateRootId;

interface NotifiableEvent
{
    public function getNotifiableId(): AggregateRootId;

    public function getNotifiableType(): string;
}
