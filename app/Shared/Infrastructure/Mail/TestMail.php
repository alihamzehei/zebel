<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Mail;

use Illuminate\Mail\Mailable;

class TestMail extends Mailable
{
    public function __construct(public readonly string $message)
    {
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('mail.test', ['message' => $this->message]);
    }
}
