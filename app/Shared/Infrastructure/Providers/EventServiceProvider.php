<?php

namespace App\Shared\Infrastructure\Providers;

use App\Analytic\Application\Listener\ActivityCollectedListener;
use App\Analytic\Application\Listener\PersistActionCheckpointListener;
use App\Analytic\Application\Listener\PersistCheckReachabilityCheckpointListener;
use App\Analytic\Application\Listener\PersistEventOccurrenceCheckpointListener;
use App\Analytic\Application\Listener\PersistIdleCheckpointListener;
use App\Analytic\Application\Listener\PersistScheduleListener;
use App\Analytic\Application\Listener\PersistStopCheckpointListener;
use App\Collect\Domain\Event\ActivityCollected;
use App\Notification\Application\Listener\NotifyListener;
use App\Plan\Domain\Event\JourneyPlanned;
use App\Plan\Domain\Event\JourneyRemoved;
use App\Schedule\Application\Listener\RemoveJourneyListener;
use App\Schedule\Application\Listeners\CreateScheduleListener;
use App\Schedule\Application\Listeners\PlanJourneyListener;
use App\Schedule\Application\Listeners\RunScheduleListener;
use App\Schedule\Domain\Event\ActionCheckpointReached;
use App\Schedule\Domain\Event\CheckReachabilityCheckpointReached;
use App\Schedule\Domain\Event\EventOccurrenceCheckpointReached;
use App\Schedule\Domain\Event\IdleCheckpointReached;
use App\Schedule\Domain\Event\ScheduleDispatched;
use App\Schedule\Domain\Event\ScheduleInitialized;
use App\Schedule\Domain\Event\StopCheckpointReached;
use HoomanMirghasemi\Sms\Events\SmsSentEvent;
use HoomanMirghasemi\Sms\Listeners\DbLogListener;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event to listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        JourneyPlanned::class => [
            PlanJourneyListener::class,
        ],
        JourneyRemoved::class => [
            RemoveJourneyListener::class,
        ],
        ActivityCollected::class => [
            CreateScheduleListener::class,
            ActivityCollectedListener::class,
        ],
        ScheduleDispatched::class => [
            RunScheduleListener::class,
        ],
        ActionCheckpointReached::class => [
            NotifyListener::class,
            PersistActionCheckpointListener::class,
        ],
        IdleCheckpointReached::class => [
            PersistIdleCheckpointListener::class,
        ],
        StopCheckpointReached::class => [
            PersistStopCheckpointListener::class,
        ],
        EventOccurrenceCheckpointReached::class => [
            PersistEventOccurrenceCheckpointListener::class,
        ],
        CheckReachabilityCheckpointReached::class => [
            PersistCheckReachabilityCheckpointListener::class,
        ],
        ScheduleInitialized::class => [
            PersistScheduleListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     */
    public function boot(): void
    {
        //
    }

    /**
     * Determine if events and listeners should be automatically discovered.
     */
    public function shouldDiscoverEvents(): bool
    {
        return false;
    }
}
