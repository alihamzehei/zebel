<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Providers;

use App\Shared\Application\Bus\Command\CommandRepository;
use App\Shared\Application\Bus\Command\InMemoryCommandBus;
use App\Shared\Application\Bus\Query\InMemoryQueryBus;
use App\Shared\Application\Command\CommandBus;
use App\Shared\Application\Query\QueryBus;
use App\Shared\Infrastructure\Bus\InMemoryCommandRepository;
use App\Shared\Infrastructure\Bus\InMemoryQueryRepository;
use Illuminate\Support\ServiceProvider;

class BusServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(CommandRepository::class, fn () => new InMemoryCommandRepository(config('commands')));
        $this->app->bind(CommandBus::class, fn ($app) => $app->get(InMemoryCommandBus::class));

        // $this->app->bind(QueryRepository::class, fn () => new InMemoryQueryRepository(config('queries')));
        $this->app->bind(QueryBus::class, function () {
            return new InMemoryQueryBus(
                new InMemoryQueryRepository(config('queries'))
            );
        });
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
    }
}
