<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Bus;

use App\Shared\Application\Bus\Query\QueryRepository;
use App\Shared\Application\Query\QueryHandlerInterface;
use App\Shared\Application\Query\QueryInterface;

class InMemoryQueryRepository implements QueryRepository
{
    public function __construct(private array $queries)
    {
    }

    public function handlerExistsFor(QueryInterface $query): bool
    {
        return \array_key_exists(\get_class($query), $this->queries);
    }

    public function getHandlerFor(QueryInterface $query): QueryHandlerInterface
    {
        return app($this->queries[\get_class($query)]);
    }
}
