<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Bus;

use App\Shared\Application\Bus\Command\CommandRepository;
use App\Shared\Application\Command\CommandHandlerInterface;
use App\Shared\Application\Command\CommandInterface;

class InMemoryCommandRepository implements CommandRepository
{
    public function __construct(private array $commands)
    {
    }

    public function handlerExistsFor(CommandInterface $command): bool
    {
        return \array_key_exists(\get_class($command), $this->commands);
    }

    public function getHandlerFor(CommandInterface $command): CommandHandlerInterface
    {
        return app($this->commands[\get_class($command)]);
    }
}
