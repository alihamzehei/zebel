<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Eloquent;

use Illuminate\Database\Eloquent\Model as BaseModel;

class Model extends BaseModel
{
    use HasPagination;
}
