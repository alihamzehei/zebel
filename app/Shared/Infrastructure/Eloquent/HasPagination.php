<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Eloquent;

trait HasPagination
{
    protected $perPageMax = 100;

    public function initializeHasPagination(array $attributes = [])
    {
        $this->perPage = config('mitra.shared.pagination.default_per_page');
    }

    /**
     * Get the number of models to return per page.
     */
    public function getPerPage(): int
    {
        request()->validate(['perPage' => 'sometimes|nullable|numeric']);
        $perPage = request('perPage', $this->perPage);

        if ($perPage === 0) {
            $perPage = $this->count();
        }

        return max(1, min($this->perPageMax, (int) $perPage));
    }

    public function setPerPageMax(int $perPageMax): void
    {
        $this->perPageMax = $perPageMax;
    }
}
