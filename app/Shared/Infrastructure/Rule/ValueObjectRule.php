<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Rule;

use App\Shared\Domain\ValueObject\ValueError;
use Illuminate\Contracts\Validation\InvokableRule;
use InvalidArgumentException;
use ReflectionClass;
use ReflectionMethod;

class ValueObjectRule implements InvokableRule
{
    /** @param class-string $valueObjectClass */
    public function __construct(private readonly string $valueObjectClass)
    {
    }

    /**
     * Run the validation rule.
     *
     * @param string $attribute
     * @param mixed  $value
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     *
     * @return void
     */
    public function __invoke($attribute, $value, $fail)
    {
        try {
            $this->instantiateValueObject($value);
        } catch (ValueError $e) {
            $fail($attribute, "{$attribute} is invalid: '{$e->getMessage()}'");
        }
    }

    private function instantiateValueObject($value)
    {
        $reflection = new ReflectionClass($this->valueObjectClass);
        if ($this->isInvokableConstructor($reflection->getConstructor())) {
            new $this->valueObjectClass($value);

            return;
        }

        /** @var ReflectionMethod $method */
        $method = collect($reflection->getMethods(ReflectionMethod::IS_PUBLIC | ReflectionMethod::IS_STATIC))
            ->filter(fn (ReflectionMethod $m) => $this->isInvokableConstructor($m))
            ->first();

        if ($method === null) {
            throw new InvalidArgumentException("value object is not validatable '{$this->valueObjectClass}'");
        }

        $method->invoke(null, $value);
    }

    private function isInvokableConstructor(ReflectionMethod $method): bool
    {
        return $method->isPublic()
            && $method->getNumberOfParameters() === 1
            && (string) $method->getParameters()[0]->getType() === 'string'
            && (
                $method->isConstructor() || \in_array((string) $method->getReturnType(), ['self', 'static'], true)
            );
    }
}
