<?php

declare(strict_types=1);

namespace App\Shared\Domain;

abstract class AggregateRoot
{
    protected array $events = [];

    protected function record(DomainEvent $event): void
    {
        $this->events[] = $event;
    }

    /** @return DomainEvent[] */
    public function pullRecordedEvents(): array
    {
        $events = $this->events;
        $this->events = [];
        return $events;
    }
}
