<?php

declare(strict_types=1);

namespace App\Shared\Domain\ValueObject;

class ValueError extends \ValueError
{
}
