<?php

declare(strict_types=1);

namespace App\Shared\Domain\ValueObject;

use InvalidArgumentException;

enum AuthScope: int
{
    case Admin = 0;
    case Trader = 1;

    public static function fromName(string $name): self
    {
        return match (mb_strtolower($name)) {
            'admin' => self::Admin,
            'trader' => self::Trader,
            default => throw new InvalidArgumentException(),
        };
    }

    public static function asAdmin(): array
    {
        return [
            self::Admin,
            self::Trader,
        ];
    }

    public static function asTrader(): array
    {
        return [
            self::Trader,
        ];
    }
}
