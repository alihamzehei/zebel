<?php

declare(strict_types=1);

namespace App\Shared\Domain\ValueObject;

class Mobile
{
    public readonly string $value;

    public function __construct(string $value)
    {
        if (!preg_match('/^(?:\+\d+|0)\d{10}$/', $value)) {
            throw new ValueError('Invalid mobile '.$value);
        }

        $this->value = $this->sanitizeValue($value);
    }

    private function sanitizeValue(string $value): string
    {
        if (mb_substr($value, 0, 1) === '0') {
            return '+98'.mb_substr($value, 1);
        }

        return $value;
    }

    public function asLocalNumber(): string
    {
        if (mb_strpos($this->value, '+98') === 0) {
            return str_replace('+98', '0', $this->value);
        }

        return str_replace('+', '00', $this->value);
    }

    public function isEqualsTo(self $mobile): bool
    {
        return $this->value === $mobile->value;
    }
}
