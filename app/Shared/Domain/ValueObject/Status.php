<?php

declare(strict_types=1);

namespace App\Shared\Domain\ValueObject;

class Status
{
    private const PENDING = 1;
    private const OPEN = 2;
    private const CLOSE = 3;

    public function __construct(public readonly int $value)
    {
    }

    public function is(self $status): bool
    {
        return $this->value === $status->value;
    }

    public static function pending(): self
    {
        return new self(self::PENDING);
    }

    public static function open(): self
    {
        return new self(self::OPEN);
    }

    public static function close(): self
    {
        return new self(self::CLOSE);
    }
}
