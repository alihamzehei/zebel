<?php

declare(strict_types=1);

namespace App\Shared\Domain\ValueObject;

class LoginToken
{
    public function __construct(
        public string $accessToken,
        public string $refreshToken,
    ) {
    }
}
