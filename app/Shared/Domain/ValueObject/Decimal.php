<?php

declare(strict_types=1);

namespace App\Shared\Domain\ValueObject;

/** To be used in combination with bcmath extension. */
abstract class Decimal
{
    public readonly string $value;
    protected readonly array $options;

    final public function __construct(
        mixed $value,
        ?array $options = null,
    ) {
        if (!\is_string($value)) {
            $value = (string) $value;
        }
        if (!is_numeric($value)) {
            throw new ValueError("Value must be numeric. '{$value}'");
        }

        $this->options = $this->sanitizeOptions(
            $value,
            array_replace(
                static::defaultOptions(),
                $options ?: [],
            )
        );
        $this->value = bcadd($value, '0', $this->getScale());
    }

    public function equalsTo(self|string $other): bool
    {
        return (float) $this->sub($other)->value === 0.0;
    }

    public function isZero(): bool
    {
        return $this->equalsTo('0');
    }

    protected function sanitizeOptions(string $value, array $options): array
    {
        return $options;
    }

    protected static function defaultOptions(): array
    {
        return [];
    }

    abstract public function getScale(): int;

    final public function eq(self|string $other): bool
    {
        return bccomp(
            $this->value,
            \is_string($other) ? $other : $other->value,
            \is_string($other) ? $this->getScale() : max($this->getScale(), $other->getScale()),
        ) === 0;
    }

    final public function gt(self|string $other): bool
    {
        return bccomp(
            $this->value,
            \is_string($other) ? $other : $other->value,
            \is_string($other) ? $this->getScale() : max($this->getScale(), $other->getScale()),
        ) > 0;
    }

    final public function gte(self|string $other): bool
    {
        return bccomp(
            $this->value,
            \is_string($other) ? $other : $other->value,
            \is_string($other) ? $this->getScale() : max($this->getScale(), $other->getScale()),
        ) >= 0;
    }

    final public function lt(self|string $other): bool
    {
        return bccomp(
            $this->value,
            \is_string($other) ? $other : $other->value,
            \is_string($other) ? $this->getScale() : max($this->getScale(), $other->getScale()),
        ) < 0;
    }

    final public function lte(self|string $other): bool
    {
        return bccomp(
            $this->value,
            \is_string($other) ? $other : $other->value,
            \is_string($other) ? $this->getScale() : max($this->getScale(), $other->getScale()),
        ) <= 0;
    }

    final public function isBetween(self|string $min, self|string $max): bool
    {
        return $this->gte($min) && $this->lte($max);
    }

    final public function sub(self|string $rhs): self
    {
        return new static(
            bcsub(
                $this->value,
                \is_string($rhs) ? $rhs : $rhs->value,
                \is_string($rhs) ? $this->getScale() : max($this->getScale(), $rhs->getScale())
            ),
            $this->options,
        );
    }

    final public function add(self|string $rhs): self
    {
        return new static(
            bcadd(
                $this->value,
                \is_string($rhs) ? $rhs : $rhs->value,
                \is_string($rhs) ? $this->getScale() : max($this->getScale(), $rhs->getScale())
            ),
            $this->options,
        );
    }

    final public function mul(self|string $rhs): self
    {
        return new static(
            bcmul(
                $this->value,
                \is_string($rhs) ? $rhs : $rhs->value,
                \is_string($rhs) ? $this->getScale() : max($this->getScale(), $rhs->getScale())
            ),
            $this->options,
        );
    }

    final public function div(self|string $rhs): self
    {
        return new static(
            bcdiv(
                $this->value,
                \is_string($rhs) ? $rhs : $rhs->value,
                \is_string($rhs) ? $this->getScale() : max($this->getScale(), $rhs->getScale())
            ),
            $this->options,
        );
    }

    final public static function avg(self ...$nums): self
    {
        $zero = new static('0');
        if (empty($nums)) {
            return $zero;
        }

        $sum = array_reduce($nums, fn ($carry, $num) => $carry->add($num), $zero);

        return $sum->div((string) \count($nums));
    }
}
