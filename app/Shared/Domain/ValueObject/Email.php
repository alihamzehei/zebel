<?php

declare(strict_types=1);

namespace App\Shared\Domain\ValueObject;

use const FILTER_FLAG_EMAIL_UNICODE;
use const FILTER_NULL_ON_FAILURE;
use const FILTER_VALIDATE_EMAIL;

class Email
{
    public function __construct(public readonly string $value)
    {
        $valid = filter_var($value, FILTER_VALIDATE_EMAIL, FILTER_FLAG_EMAIL_UNICODE | FILTER_NULL_ON_FAILURE);
        if (!$valid) {
            throw new ValueError("Email is not valid '{$value}'");
        }
    }
}
