<?php

declare(strict_types=1);

namespace App\Shared\Domain;

class AggregateRootId
{
    public function __construct(public readonly string $id)
    {
    }

    public function isEqualsTo(self $id): bool
    {
        return $this->id === $id->id;
    }
}
