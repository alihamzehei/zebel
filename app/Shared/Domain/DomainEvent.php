<?php

declare(strict_types=1);

namespace App\Shared\Domain;

use DateTimeImmutable;

abstract class DomainEvent
{
    protected DateTimeImmutable $occurredAt;

    public function occurredAt(): DateTimeImmutable
    {
        return $this->occurredAt;
    }

    protected function occurred(?DateTimeImmutable $at = null): void
    {
        $this->occurredAt = $at ?: new DateTimeImmutable();
    }
}
