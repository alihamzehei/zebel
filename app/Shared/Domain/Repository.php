<?php

declare(strict_types=1);

namespace App\Shared\Domain;

interface Repository
{
    /** @return AggregateRoot[] */
    public function seen(): array;
}
