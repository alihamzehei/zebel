<?php

declare(strict_types=1);

namespace App\Shared\Domain;

use Exception;

class DomainError extends Exception
{
}
