<?php

declare(strict_types=1);

namespace App\Target\Presentation\Module;

use App\Shared\Application\Query\QueryBus;
use App\Target\Application\Query\TargetQuery;


class TargetModule
{
    public function __construct(private readonly QueryBus $bus)
    {
    }

    public function getTargetById(int $id): array
    {
        return $this->bus->ask(new TargetQuery($id));
    }
}
