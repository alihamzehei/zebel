<?php

use App\Target\Presentation\Controllers\GetTargetController;
use App\Target\Presentation\Controllers\RegisterTargetController;
use Illuminate\Support\Facades\Route;

Route::post('targets', RegisterTargetController::class);
Route::get('targets/{id}', GetTargetController::class);

