<?php

declare(strict_types=1);

namespace App\Target\Presentation\Controllers;

use App\Target\Presentation\Requests\RegisterTargetRequest;
use Illuminate\Http\JsonResponse;
use App\Shared\Application\Command\CommandBus;
use App\Shared\Presentation\Http\Controllers\Controller;
use App\Target\Application\Command\RegisterTargetCommand;

class RegisterTargetController extends Controller
{
    public function __construct(private CommandBus $bus)
    {
    }

    public function __invoke(RegisterTargetRequest $request): JsonResponse
    {
        $payload = $request->validated();

        $command = new RegisterTargetCommand(
            $payload['id'],
            $payload['name'],
            $payload['email'],
            $payload['mobile'],
            $payload['najva_token'],
        );

        $this->bus->handle($command);

        return response()->json(['id' => $payload['id']], 201);
    }
}
