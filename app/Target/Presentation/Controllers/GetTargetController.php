<?php

declare(strict_types=1);

namespace App\Target\Presentation\Controllers;

use App\Shared\Application\Query\QueryBus;
use App\Target\Application\Query\TargetQuery;
use App\Shared\Presentation\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;

class GetTargetController extends Controller
{
    public function __construct(private readonly QueryBus $bus)
    {
    }

    public function __invoke(int $id): JsonResponse
    {
        return response()->json($this->bus->ask(new TargetQuery($id)));
    }
}
