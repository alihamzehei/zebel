<?php

declare(strict_types=1);

namespace App\Target\Infrastructure\Repository;

use App\Target\Domain\Model\Target;
use App\Target\Infrastructure\Model\TargetModel;
use App\Target\Domain\Repository\TargetRepository;

class EloquentTargetRepository implements TargetRepository
{
    private array $seen = [];

    public function persist(Target $target): void
    {
        TargetModel::query()->updateOrCreate(['id' => $target->id], [
            'id' => $target->id,
            'name' => $target->name,
            'email' => $target->email,
            'mobile' => $target->mobile,
            'najva_token' => $target->najvaToken,
        ]);

        $this->seen[] = $target;
    }

    public function findById(int $id): ?Target
    {
        return TargetModel::query()
            ->find($id)
            ?->toDomain();
    }

    public function nextIdentity(): int
    {
        return 0;
    }

    public function seen(): array
    {
        return $this->seen;
    }
}
