<?php

declare(strict_types=1);

namespace App\Target\Infrastructure\Model;

use Illuminate\Database\Eloquent\Model;
use App\Target\Domain\Model\Target;

class TargetModel extends Model
{
    protected $table = 'target_targets';

    protected $fillable = [
        'id',
        'name',
        'email',
        'mobile',
        'najva_token',
    ];

    public function toDomain(): Target
    {
        return new Target($this->id, $this->name, $this->email, $this->mobile, $this->najva_token);
    }
}
