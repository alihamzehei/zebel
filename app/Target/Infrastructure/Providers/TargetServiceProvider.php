<?php

namespace App\Target\Infrastructure\Providers;

use App\Target\Infrastructure\Repository\EloquentTargetRepository;
use App\Target\Domain\Repository\TargetRepository;
use Illuminate\Support\ServiceProvider;
class TargetServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->bind(TargetRepository::class, EloquentTargetRepository::class);
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        $this->loadMigrationsFrom(dirname(__DIR__ ) . '/Migrations');
    }
}
