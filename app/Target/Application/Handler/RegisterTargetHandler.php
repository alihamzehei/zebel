<?php

declare(strict_types=1);

namespace App\Target\Application\Handler;

use App\Target\Domain\Model\Target;
use App\Shared\Application\Command\CommandInterface;
use App\Target\Application\Command\RegisterTargetCommand;
use App\Shared\Application\Command\CommandHandlerInterface;
use App\Target\Domain\Repository\TargetRepository;

class RegisterTargetHandler implements CommandHandlerInterface
{
    public function __construct(private readonly TargetRepository $repository)
    {
        
    }

    public function handle(CommandInterface|RegisterTargetCommand $command)
    {
        $target = Target::register($command->id, $command->name, $command->email, $command->mobile, $command->najvaToken);
        $this->repository->persist($target);
    }
}
