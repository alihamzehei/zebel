<?php

declare(strict_types=1);

namespace App\Target\Application\Query;

use App\Shared\Application\Error\NoQueryResult;
use App\Shared\Application\Query\QueryInterface;
use App\Shared\Application\Query\QueryHandlerInterface;
use App\Target\Infrastructure\Model\TargetModel;
use JsonSerializable;

class TargetQueryHandler implements QueryHandlerInterface
{
    public function handle(QueryInterface|TargetQuery $query): JsonSerializable|array
    {
        $target = TargetModel::query()
            ->select('id', 'name', 'email', 'mobile', 'najva_token')
            ->find($query->target);

        if ($target === null) {
            throw new NoQueryResult;
        }

        return $target->toArray();
    }
}
