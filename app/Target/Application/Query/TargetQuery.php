<?php

declare(strict_types=1);

namespace App\Target\Application\Query;

use App\Shared\Application\Query\QueryInterface;

class TargetQuery implements QueryInterface
{
    public function __construct(public readonly int $target)
    {
    }
}
