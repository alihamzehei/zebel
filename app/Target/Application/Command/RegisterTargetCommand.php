<?php

declare(strict_types=1);

namespace App\Target\Application\Command;

use App\Shared\Application\Command\CommandInterface;

class RegisterTargetCommand implements CommandInterface
{
    public function __construct(public readonly int $id,
                                public readonly ?string $name,
                                public readonly ?string $email,
                                public readonly ?string $mobile,
                                public readonly ?string $najvaToken = null)
    {
    }
}
