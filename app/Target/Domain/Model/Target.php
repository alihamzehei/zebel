<?php

declare(strict_types=1);

namespace App\Target\Domain\Model;

use App\Shared\Domain\AggregateRoot;
use App\Target\Domain\Event\TargetRegistered;

class Target extends AggregateRoot
{
    public function __construct(public readonly int $id,
                                public readonly ?string $name,
                                public readonly ?string $email,
                                public readonly ?string $mobile,
                                public readonly ?string $najvaToken = null)
    {
    }

    public static function register(int $id, ?string $name, ?string $email, ?string $mobile, ?string $najvaToken = null): self
    {
        $target = new self($id, $name, $email, $mobile, $najvaToken);
        $target->record(new TargetRegistered($id, $name, $email, $mobile, $najvaToken));

        return $target;
    }
}
