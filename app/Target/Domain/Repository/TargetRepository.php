<?php

declare(strict_types=1);

namespace App\Target\Domain\Repository;

use App\Shared\Domain\Repository;
use App\Target\Domain\Model\Target;

interface TargetRepository extends Repository
{
    public function persist(Target $target): void;

    public function nextIdentity(): int;

    public function findById(int $id): ?Target;
}
