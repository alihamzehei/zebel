<?php

declare(strict_types=1);

namespace App\Target\Domain\Event;

use App\Shared\Domain\DomainEvent;

class TargetRegistered extends DomainEvent
{
    public function __construct(public readonly int $id,
                                public readonly ?string $name,
                                public readonly ?string $email,
                                public readonly ?string $mobile,
                                public readonly ?string $najvaToken)
    {
        $this->occurred();
    }
}
