<?php

namespace App\Schedule\Presentation\Console;

use App\Schedule\Application\Command\DispatchSchedulesCommand ;
use App\Shared\Application\Command\CommandBus;
use Illuminate\Console\Command;


class DispatchSchedulesConsoleCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:schedule:dispatch';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Execute the scheduled journeys';

    /**
     * Execute the console command.
     */
    public function handle(CommandBus $bus): void
    {
        $this->alert('dispatching schedules');
        $this->alert(sprintf("%d schedules have been dispatched", $bus->handle(new DispatchSchedulesCommand)));
    }
}
