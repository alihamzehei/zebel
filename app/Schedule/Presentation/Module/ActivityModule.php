<?php

declare(strict_types=1);

namespace App\Schedule\Presentation\Module;

use App\Collect\Application\Query\ActivityByIdsQuery;
use App\Shared\Application\Query\QueryBus;


class ActivityModule
{
    public function __construct(private readonly QueryBus $bus)
    {
    }
    public function getActivityByIds(array $activityIds): array
    {
        return $this->bus->ask(new ActivityByIdsQuery($activityIds));
    }
}
