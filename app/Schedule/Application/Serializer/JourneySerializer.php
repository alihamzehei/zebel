<?php

declare(strict_types=1);

namespace App\Schedule\Application\Serializer;

use DateTimeImmutable;
use Exception;
use App\Schedule\Domain\Model\Activity;
use App\Schedule\Domain\Model\Checkpoint\ActionCheckpoint;
use App\Schedule\Domain\Model\Checkpoint\CheckpointInterface;
use App\Schedule\Domain\Model\Checkpoint\CheckReachabilityCheckpoint;
use App\Schedule\Domain\Model\Checkpoint\EventOccurrenceCheckpoint;
use App\Schedule\Domain\Model\Checkpoint\IdleCheckpoint;
use App\Schedule\Domain\Model\Checkpoint\StopCheckpoint;
use App\Schedule\Domain\ValueObject\ActivityName;
use App\Schedule\Domain\ValueObject\IdleTime;

class JourneySerializer
{
    public function serialize(CheckpointInterface $checkpoint): array
    {
        return match (get_class($checkpoint)) {
            ActionCheckpoint::class => [
                'id' => $checkpoint->id(),
                'left_child_id' => $checkpoint->leftChildId(),
                'is_checked' => $checkpoint->isChecked(),
                'payload' => ['notification_id' => $checkpoint->notificationId],
                'type' => ActionCheckpoint::TYPE,
            ],
            StopCheckpoint::class => [
                'id' => $checkpoint->id(),
                'type' => StopCheckpoint::TYPE,
                'is_checked' => $checkpoint->isChecked(),
            ],
            IdleCheckpoint::class => [
                'id' => $checkpoint->id(),
                'left_child_id' => $checkpoint->leftChildId(),
                'right_child_id' => $checkpoint->rightChildId(),
                'is_checked' => $checkpoint->isChecked(),
                'payload' => ['time' => $checkpoint->until->minutes],
                'type' => IdleCheckpoint::TYPE,
            ],
            EventOccurrenceCheckpoint::class => [
                'id' => $checkpoint->id(),
                'left_child_id' => $checkpoint->leftChildId(),
                'right_child_id' => $checkpoint->rightChildId(),
                'is_checked' => $checkpoint->isChecked(),
                'payload' => ['activity_name' => $checkpoint->activity->name->value , 'compare_condition' => $checkpoint->compareCondition],
                'type' => EventOccurrenceCheckpoint::TYPE,
            ],
            CheckReachabilityCheckpoint::class => [
                'id' => $checkpoint->id(),
                'payload' => ['channel' => $checkpoint->channel],
                'type' => CheckReachabilityCheckpoint::TYPE,
                'left_child_id' => $checkpoint->leftChildId(),
                'right_child_id' => $checkpoint->rightChildId(),
                'is_checked' => $checkpoint->isChecked(),
            ],
        };
    }

    public function deserialize(array $checkpoint): CheckpointInterface
    {
        return match ($checkpoint['type']) {
            ActionCheckpoint::TYPE => new ActionCheckpoint(
                $checkpoint['id'],
                $checkpoint['left_child_id'],
                $checkpoint['is_checked'],
                $checkpoint['payload']['notification_id'],
            ),
            StopCheckpoint::TYPE => new StopCheckpoint($checkpoint['id'], $checkpoint['is_checked']),
            IdleCheckpoint::TYPE => new IdleCheckpoint(
                $checkpoint['id'],
                $checkpoint['left_child_id'],
                $checkpoint['is_checked'],
                new IdleTime((int)$checkpoint['payload']['time']),
            ),
            EventOccurrenceCheckpoint::TYPE => new EventOccurrenceCheckpoint(
                $checkpoint['id'],
                $checkpoint['left_child_id'],
                $checkpoint['right_child_id'],
                $checkpoint['is_checked'],
                new Activity(
                    uniqid(),
                    new ActivityName($checkpoint['payload']['activity_name']),
                    new DateTimeImmutable()
                ),
                ($checkpoint['payload']['compare_condition']) ?? []
            ),
            CheckReachabilityCheckpoint::TYPE => new CheckReachabilityCheckpoint(
                $checkpoint['id'],
                $checkpoint['left_child_id'],
                $checkpoint['right_child_id'],
                $checkpoint['is_checked'],
                $checkpoint['payload']['channel'],
            ),
            default => throw new Exception(sprintf('%s checkpoint type not recognized', $checkpoint['type']))
        };
    }
}
