<?php

declare(strict_types=1);

namespace App\Schedule\Application\Listener;

use App\Plan\Domain\Event\JourneyRemoved;
use App\Schedule\Domain\Repository\JourneyRepository;

class RemoveJourneyListener
{
    public function __construct(private readonly JourneyRepository $repository)
    {
    }

    public function __invoke(JourneyRemoved $event): void
    {
        $this->repository->deleteById($event->id);
    }
}
