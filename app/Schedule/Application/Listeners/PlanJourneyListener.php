<?php

declare(strict_types=1);

namespace App\Schedule\Application\Listeners;

use App\Plan\Domain\Event\JourneyPlanned;
use App\Schedule\Application\Serializer\JourneySerializer;
use App\Schedule\Domain\Model\Journey;
use App\Schedule\Domain\Repository\JourneyRepository;

class PlanJourneyListener
{
    public function __construct(
        private readonly JourneySerializer $serializer,
        private readonly JourneyRepository $repository,
    )
    {
    }

    public function handle(JourneyPlanned $event): void
    {
        if ($this->repository->findById($event->id) !== null) {
            $this->repository->deleteById($event->id);
        }

        $this->repository->persist($this->createJourneyFromEvent($event));
    }

    private function createJourneyFromEvent(JourneyPlanned $event): Journey
    {
        return new Journey(
            $event->id, $event->name, $event->root, $event->root, $event->trigger,
            $this->checkpointsAsObjects($event->checkpoints), $event->enable
        );
    }

    private function checkpointsAsObjects(array $checkpoints): array
    {
        return array_map([$this->serializer, 'deserialize'], $checkpoints);
    }
}
