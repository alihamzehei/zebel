<?php

namespace App\Schedule\Application\Listeners;

use App\Collect\Domain\Event\ActivityCollected;
use App\Schedule\Application\Command\CreateScheduleCommand;
use App\Schedule\Domain\Repository\JourneyRepository;
use App\Shared\Application\Command\CommandBus;
use App\Target\Presentation\Module\TargetModule;

class CreateScheduleListener
{
    public function __construct(
        private readonly JourneyRepository $journeys,
        private readonly CommandBus        $bus,
        private readonly TargetModule      $targets,
    )
    {
    }

    public function handle(ActivityCollected $event): void
    {
        $journeys = $this->journeys->findByTrigger($event->title);

        if (empty($journeys)) {
            return;
        }

        foreach ($journeys as $journey) {
            $this->bus->handle(new CreateScheduleCommand(
                    $event->causer,
                    $this->appendChannelToSchedule($event->causer),
                    $journey->id,
                    $event->id,
                    $event->payload)
            );
        }
    }

    public function appendChannelToSchedule(int $causer): array
    {
        $target = $this->targets->getTargetById($causer);

        $channels = [];

        if ($target['mobile']) {
            $channels[] = 'sms';
        }

        if ($target['email']) {
            $channels[] = 'email';
        }

        return $channels;
    }
}
