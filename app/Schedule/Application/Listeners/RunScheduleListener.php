<?php

namespace App\Schedule\Application\Listeners;

use App\Schedule\Application\Command\RunScheduleCommand;
use App\Schedule\Domain\Event\ScheduleDispatched;
use App\Shared\Application\Command\CommandBus;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class RunScheduleListener implements ShouldQueue
{
    use InteractsWithQueue;

    public function __construct(private readonly CommandBus $bus)
    {
    }

    public function handle(ScheduleDispatched $event): void
    {
        $this->bus->handle(new RunScheduleCommand($event->scheduleId));
    }
}
