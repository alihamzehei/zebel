<?php

namespace App\Schedule\Application\Handler;

use App\Collect\Presentation\Module\ActivityModule;
use App\Schedule\Domain\Model\Activity;
use App\Schedule\Domain\Model\Schedule;
use App\Schedule\Domain\ValueObject\ActivityName;
use App\Schedule\Domain\ValueObject\ScheduleId;
use App\Shared\Application\Command\CommandInterface;
use App\Schedule\Domain\Repository\ScheduleRepository;
use App\Schedule\Application\Command\RunScheduleCommand;
use App\Shared\Application\Command\CommandHandlerInterface;
use App\Shared\Application\Error\NoQueryResult;
use App\Shared\Domain\DomainError;
use DateTimeImmutable;

class RunScheduleHandler implements CommandHandlerInterface
{
    public function __construct(
        private readonly ScheduleRepository $repository,
        private readonly ActivityModule     $activities

    )
    {
    }

    /**
     * @throws DomainError
     */
    public function handle(CommandInterface|RunScheduleCommand $command): bool
    {
        if ($schedule = $this->repository->findById(new ScheduleId($command->id))) {
            $this->appendTriggerToActivities($schedule);

            $schedule->run(function (Schedule $schedule) {
                if ($schedule->expectant()) {
                    $this->appendActivitiesToSchedule($schedule);
                }

            });

            $this->repository->persist($schedule);

            return true;
        }

        return false;
    }


    private function appendActivitiesToSchedule(Schedule $schedule): void
    {
        try {
            $activities = $this->activities->getActivities(
                $schedule->target->identifier,
                $schedule->expectant()->activity->value,
                $schedule->expectant()->since
            );

        } catch (NoQueryResult) {
            $activities = $schedule->activities();
        }

        foreach ($activities as $activity) {
            $activityInstance = new Activity(
                $activity['uuid'],
                new ActivityName($activity['title']),
                new DateTimeImmutable(),
                $activity['payload']
            );

            if ($schedule->validateActivity($activityInstance)) {
                $schedule->addActivity($activityInstance);
            };
        }
    }

    private function appendTriggerToActivities(Schedule $schedule): void
    {
        try {
            $trigger = $this->activities->getActivityById($schedule->id->activityId);

            $activity = new Activity(
                $trigger['uuid'],
                new ActivityName($trigger['title']), new DateTimeImmutable($trigger['occurred_at']),
                $trigger['payload'],
                isTrigger: true
            );

            $schedule->addActivity($activity);
        } catch (NoQueryResult) {
            $trigger = null;
        }
    }
}
