<?php

declare(strict_types=1);

namespace App\Schedule\Application\Handler;

use App\Schedule\Application\Command\CreateJourneyCommand;
use App\Schedule\Application\UnitOfWork\JourneyUnitOfWork;
use App\Schedule\Domain\Model\Journey;
use App\Shared\Application\CommandHandlerError;
use App\Shared\Application\Command\CommandHandlerInterface;
use App\Shared\Application\CommandInterface;
use App\Shared\Application\UnitOfWork;

class CreateJourneyHandler implements CommandHandlerInterfa
{
    public function handle(CommandInterface|CreateJourneyCommand $command, UnitOfWork|JourneyUnitOfWork $uow)
    {
        if ($uow->repository()->findById($command->id) !== null) {
            $journey = $uow->repository()->deleteById($command->id);

            if ($journey === null) {
                throw new CommandHandlerError(sprintf('No journey found with Id %s', $command->id));
            }
        }

        $uow->repository()->persist(
            new Journey($command->id, $command->name, $command->root, $command->root, $command->trigger, $command->checkpointsAsObjects())
        );
    }
}
