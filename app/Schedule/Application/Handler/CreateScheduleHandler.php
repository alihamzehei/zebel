<?php

declare(strict_types=1);

namespace App\Schedule\Application\Handler;

use App\Schedule\Domain\Model\Target;
use App\Schedule\Domain\Model\Schedule;
use App\Schedule\Domain\ValueObject\Status;
use App\Schedule\Domain\ValueObject\ScheduleId;
use App\Shared\Application\Command\CommandInterface;
use App\Schedule\Application\Command\CreateScheduleCommand;
use App\Schedule\Domain\Repository\JourneyRepository;
use App\Schedule\Domain\Repository\ScheduleRepository;
use App\Shared\Application\Command\CommandHandlerInterface;
use App\Shared\Application\Error\ApplicationError;

class CreateScheduleHandler implements CommandHandlerInterface
{
    public function __construct(
        private JourneyRepository  $journeys,
        private ScheduleRepository $schedules,
    )
    {
    }

    public function handle(CommandInterface|CreateScheduleCommand $command): void
    {
        $journey = $this->journeys->findById($command->journeyId);

        if ($journey === null) {
            throw new ApplicationError("No such journey with id [$command->journeyId]");
        }

        $schedule = Schedule::initialize(
            new ScheduleId(uniqid(), $command->activityId),
            new Target($command->targetId, $command->targetChannels),
            $journey,
            Status::init(),
            payload: $command->payload
        );

        $this->schedules->persist($schedule);
    }
}
