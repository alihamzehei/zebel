<?php

declare(strict_types=1);

namespace App\Schedule\Application\Handler;

use App\Shared\Application\Command\CommandInterface;
use App\Schedule\Domain\Repository\ScheduleRepository;
use App\Schedule\Application\Command\DispatchSchedulesCommand ;
use App\Shared\Application\Command\CommandHandlerInterface;

class DispatchSchedulesHandler implements CommandHandlerInterface
{
    public function __construct(
        private readonly ScheduleRepository $repository,
    )
    {
    }

    public function handle(CommandInterface|DispatchSchedulesCommand  $command): int
    {
        foreach ($schedules = $this->repository->findRunnable() as $schedule) {
            $schedule->dispatch();
            $this->repository->persist($schedule);
        }

        return count($schedules);
    }
}
