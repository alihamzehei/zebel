<?php

declare(strict_types=1);

namespace App\Schedule\Application\Command;

use App\Shared\Application\Command\CommandInterface;

class DispatchSchedulesCommand  implements CommandInterface
{
}
