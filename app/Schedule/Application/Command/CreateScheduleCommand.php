<?php

declare(strict_types=1);

namespace App\Schedule\Application\Command;

use App\Shared\Application\Command\CommandInterface;

class CreateScheduleCommand implements CommandInterface
{
    public function __construct(public readonly int    $targetId,
                                public readonly array  $targetChannels,
                                public readonly string $journeyId,
                                public readonly string $activityId,
                                public readonly array  $payload)
    {
    }
}
