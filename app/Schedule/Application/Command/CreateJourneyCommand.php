<?php

declare(strict_types=1);

namespace App\Schedule\Application\Command;

use App\Schedule\Application\Serializer\JourneySerializer;
use App\Shared\Application\Command\CommandInterface;

class CreateJourneyCommand implements CommandInterface
{
    public function __construct(public readonly string $id,
                                public readonly string $name,
                                public readonly string $root,
                                public readonly string $trigger,
                                public readonly array $checkpoints,
                                public readonly JourneySerializer $journeySerializer)
    {
    }

    public function checkpointsAsObjects(): array
    {
        return array_map([$this->journeySerializer, 'deserialize'], $this->checkpoints);
    }
}
