<?php

namespace App\Schedule\Application\Command;

use App\Shared\Application\Command\CommandInterface;

class RunScheduleCommand implements CommandInterface
{
    public function __construct(public readonly string $id)
    {
        
    }
}
