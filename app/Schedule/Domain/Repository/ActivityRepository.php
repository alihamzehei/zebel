<?php

declare(strict_types=1);

namespace App\Schedule\Domain\Repository;

use App\Schedule\Domain\Model\Activity;

interface ActivityRepository
{
    public function persist(Activity $activity): void;
}
