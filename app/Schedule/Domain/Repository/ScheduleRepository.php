<?php

declare(strict_types=1);

namespace App\Schedule\Domain\Repository;

use App\Shared\Domain\Repository;
use App\Schedule\Domain\Model\Schedule;
use App\Schedule\Domain\ValueObject\ScheduleId;

interface ScheduleRepository extends Repository
{
    public function findById(ScheduleId $id): ?Schedule;

    /**  @return Schedule[] */
    public function findRunnable(): array;

    public function persist(Schedule $schedule): void;

    public function nextIdentity(): ScheduleId;
}
