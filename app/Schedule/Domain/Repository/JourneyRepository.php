<?php

declare(strict_types=1);

namespace App\Schedule\Domain\Repository;

use App\Shared\Domain\Repository;
use App\Schedule\Domain\Model\Journey;

interface JourneyRepository extends Repository
{
    public function findById(string $id): ?Journey;

    public function deleteById(string $id): ?string;

    public function persist(Journey $journey): void;

    /** @return Journey[] */
    public function findByTrigger(string $trigger): array;
}
