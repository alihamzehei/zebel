<?php

declare(strict_types=1);

namespace App\Schedule\Domain\ValueObject;

class CheckpointType
{
    public const IDLE = 1;
    public const ACTION = 2;
    public const CONDITION = 3;
    public const STOP = 4;

    private function __construct(public readonly int $value)
    {
    }

    public static function idle(): self
    {
        return new self(self::IDLE);
    }

    public static function action(): self
    {
        return new self(self::ACTION);
    }

    public static function condition(): self
    {
        return new self(self::CONDITION);
    }

    public static function stop(): self
    {
        return new self(self::STOP);
    }

    public function isEqualsTo(self $type): bool
    {
        return $this->value === $type->value;
    }
}
