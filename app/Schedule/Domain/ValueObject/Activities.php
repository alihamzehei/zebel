<?php

declare(strict_types=1);

namespace App\Schedule\Domain\ValueObject;

use App\Schedule\Domain\Model\Activity;

class Activities
{
    private array $activities = [];

    public function __construct(Activity ...$activities)
    {
        $this->activities = $activities;
    }

    public function add(Activity $activity): self
    {
        return new self(...array_merge($this->activities, [$activity]));
    }

    public function exists(Activity $lookingFor): bool
    {
        foreach ($this->activities as $activity) {
            if ($activity->name->isEqualsTo($lookingFor->name) && !$activity->isTrigger) {
                return true;
            }
        }

        return false;
    }

    public function existsById(Activity $lookingFor): bool
    {
        foreach ($this->activities as $activity) {
            if ($activity->id === $lookingFor->id) {
                return true;
            }
        }

        return false;
    }

    public function asArray(): array
    {
        return $this->activities;
    }
}
