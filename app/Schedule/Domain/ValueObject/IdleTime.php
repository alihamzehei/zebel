<?php

declare(strict_types=1);

namespace App\Schedule\Domain\ValueObject;

use DateTimeImmutable;
use App\Shared\Domain\ValueObject\ValueError;

class IdleTime
{
    public const MINIMUM_TIMER_IN_MINUTES = 1;

    public function __construct(public readonly int $minutes)
    {
        if ($minutes < self::MINIMUM_TIMER_IN_MINUTES) {
            throw new ValueError('Idle time must be at least '.self::MINIMUM_TIMER_IN_MINUTES.' minutes');
        }
    }

    public function takesSameAs(self $idleTime): bool
    {
        return $this->minutes === $idleTime->minutes;
    }

    public function toDateTimeImmutable(): DateTimeImmutable
    {
        return new DateTimeImmutable(sprintf('+%d minutes', $this->minutes));
    }
}
