<?php

declare(strict_types=1);

namespace App\Schedule\Domain\ValueObject;

use ValueError;

class ActivityName
{
    public function __construct(public readonly string $value)
    {
        if (mb_strlen($value) < 5) {
            throw new ValueError('Activity name must contain 5 letters at least');
        }
    }

    public function isEqualsTo(self $name): bool
    {
        return $this->value === $name->value;
    }
}
