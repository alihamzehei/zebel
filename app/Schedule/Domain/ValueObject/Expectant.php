<?php

declare(strict_types=1);

namespace App\Schedule\Domain\ValueObject;

use DateTimeImmutable;

class Expectant
{
    public function __construct(public readonly ActivityName $activity,
                                public readonly DateTimeImmutable $since)
    {
    }

    public function isMatchWith(self $anOther): bool
    {
        if (!$this->activity->isEqualsTo($anOther->activity)) {
            return false;
        }

        return $this->since <= $anOther->since;
    }
}
