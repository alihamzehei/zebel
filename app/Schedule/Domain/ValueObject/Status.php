<?php

declare(strict_types=1);

namespace App\Schedule\Domain\ValueObject;

use ValueError;

class Status
{
    public const CLOSE = 'close';
    public const IDLE = 'idle';
    public const INIT = 'init';
    public const DISPATCH = 'dispatch';

    private const STATES = [
        self::IDLE,
        self::CLOSE,
        self::INIT,
        self::DISPATCH,
    ];

    public function __construct(public readonly string $state = self::INIT)
    {
        if (!\in_array($state, self::STATES, true)) {
            throw new ValueError("Invalid state $state");
        }
    }

    public static function init(): self
    {
        return new self();
    }

    public static function idle(): self
    {
        return new self(self::IDLE);
    }

    public static function close(): self
    {
        return new self(self::CLOSE);
    }

    public static function dispatch(): self
    {
        return new self(self::DISPATCH);
    }

    public function is(self $status): bool
    {
        return $this->state === $status->state;
    }
}
