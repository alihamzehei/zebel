<?php

declare(strict_types=1);

namespace App\Schedule\Domain\ValueObject;

class ScheduleId
{
    public function __construct(public readonly string $id,
                                public readonly ?string $activityId = null)
    {
    }

    public function __toString()
    {
        return $this->id;
    }

    public function isEqualsTo(self $id): bool
    {
        return $this->id === $id->id;
    }
}
