<?php

declare(strict_types=1);

namespace App\Schedule\Domain\ValueObject;

use DateTimeImmutable;

class ExpectantCondition
{
    public function __construct(public readonly string $type,
                                public readonly string $field,
                                public readonly string $target,
                                public readonly string $operator)
    {
    }
}
