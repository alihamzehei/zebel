<?php

declare(strict_types=1);

namespace App\Schedule\Domain\Exception;

use Exception;

class NoMoreReachableException extends Exception
{
}
