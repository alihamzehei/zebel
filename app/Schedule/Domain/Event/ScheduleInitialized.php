<?php

declare(strict_types=1);

namespace App\Schedule\Domain\Event;

use App\Schedule\Domain\Model\Schedule;
use App\Shared\Domain\DomainEvent;

class ScheduleInitialized extends DomainEvent
{
    public function __construct(public readonly string $id,
                                public readonly int $target,
                                public readonly string $journeyId)
    {
        $this->occurred();
    }

    public static function fromDomain(Schedule $schedule): self
    {
        return new self($schedule->id->id, $schedule->target->identifier, $schedule->journey()->id);
    }
}
