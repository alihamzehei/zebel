<?php

declare(strict_types=1);

namespace App\Schedule\Domain\Event;

class EventOccurrenceCheckpointReached extends CheckpointReached
{
    public function __construct(public readonly string $scheduleId,
                                public readonly string $checkpointId,
                                public readonly string $activity)
    {
        $this->occurred();
    }
}
