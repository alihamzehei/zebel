<?php

namespace App\Schedule\Domain\Event;

use App\Shared\Domain\DomainEvent;

class ScheduleDispatched extends DomainEvent
{
    public function __construct(public readonly string $scheduleId)
    {
        $this->occurred();
    }    
}
