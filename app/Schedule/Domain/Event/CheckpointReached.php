<?php

declare(strict_types=1);

namespace App\Schedule\Domain\Event;

use App\Shared\Domain\DomainEvent;

class CheckpointReached extends DomainEvent
{
    public function __construct(public readonly string $scheduleId,
                                public readonly string $checkpointId)
    {
        $this->occurred();
    }
}
