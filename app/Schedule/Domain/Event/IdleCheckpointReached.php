<?php

declare(strict_types=1);

namespace App\Schedule\Domain\Event;

class IdleCheckpointReached extends CheckpointReached
{
    public function __construct(string $scheduleId,
                                string $checkpointId,
                                public readonly string $journeyId,
                                public readonly int $targetId,
                                public readonly ?string $expectant = null)
    {
        parent::__construct($scheduleId, $checkpointId);
    }
}
