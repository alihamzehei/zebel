<?php

declare(strict_types=1);

namespace App\Schedule\Domain\Event;

class ActionCheckpointReached extends CheckpointReached
{
    public const NAME = 'action.checkpoint.reached';

    public function __construct(public readonly string $scheduleId,
                                public readonly string $checkpointId,
                                public readonly string $notificationId,
                                public readonly int $targetId,
                                public readonly array $activities = [])
    {
        $this->occurred();
    }
}
