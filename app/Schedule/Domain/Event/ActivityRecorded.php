<?php

declare(strict_types=1);

namespace App\Schedule\Domain\Event;

use App\Shared\Domain\DomainEvent;

class ActivityRecorded extends DomainEvent
{
    public function __construct(public readonly string $name)
    {
    }
}
