<?php

declare(strict_types=1);

namespace App\Schedule\Domain\Model;

use App\Plan\Domain\Model\ConditionType;
use App\Schedule\Domain\ValueObject\ExpectantCondition;
use Closure;
use DateTimeImmutable;
use App\Shared\Domain\DomainError;
use App\Shared\Domain\AggregateRoot;
use App\Schedule\Domain\ValueObject\Status;
use App\Schedule\Domain\ValueObject\Expectant;
use App\Schedule\Domain\ValueObject\Activities;
use App\Schedule\Domain\ValueObject\ScheduleId;
use App\Schedule\Domain\Event\ScheduleDispatched;
use App\Schedule\Domain\Event\ScheduleInitialized;
use App\Schedule\Domain\Event\IdleCheckpointReached;
use App\Schedule\Domain\Event\StopCheckpointReached;
use App\Schedule\Domain\Event\ActionCheckpointReached;
use App\Schedule\Domain\Model\Checkpoint\IdleCheckpoint;
use App\Schedule\Domain\Model\Checkpoint\StopCheckpoint;
use App\Schedule\Domain\Model\Checkpoint\ActionCheckpoint;
use App\Schedule\Domain\Event\EventOccurrenceCheckpointReached;
use App\Schedule\Domain\Event\CheckReachabilityCheckpointReached;
use App\Schedule\Domain\Model\Checkpoint\EventOccurrenceCheckpoint;
use App\Schedule\Domain\Model\Checkpoint\CheckReachabilityCheckpoint;

class Schedule extends AggregateRoot
{
    private Activities $activities;

    public function __construct(
        public readonly ScheduleId $id,
        public readonly Target     $target,
        private readonly Journey   $journey,
        private Status             $status,
        ?Activities                $activities = null,
        private ?DateTimeImmutable $idleUntil = null,
        private ?Expectant         $expectant = null,
        private readonly array     $triggerPayload = [],
        private array              $expectantConditions = [],
    )
    {
        $this->activities = $activities ?: new Activities();
    }

    public static function initialize(
        ScheduleId         $id,
        Target             $target,
        Journey            $journey,
        Status             $status,
        ?Activities        $activities = null,
        ?DateTimeImmutable $idleUntil = null,
        ?Expectant         $expectant = null,
        array              $payload = []
    ): self
    {
        $schedule = new self($id, $target, $journey, $status, $activities, $idleUntil, $expectant, $payload);

        $schedule->record(ScheduleInitialized::fromDomain($schedule));

        return $schedule;
    }

    public function dispatch(): void
    {
        $this->status = Status::dispatch();
        $this->record(new ScheduleDispatched($this->id->id));
    }

    /**
     * @throws DomainError
     */
    public function run(Closure $closure): void
    {
        if ($this->status->is(Status::close())) {
            throw new DomainError('Cannot run a closed schedule');
        }

        while ($this->journey->hasReachableCheckpoint()) {
            $current = $this->journey->current();

            $closure($this);

            if (($next = $this->journey->findById($current->leftChildId())) instanceof EventOccurrenceCheckpoint) {
                $this->expectant = new Expectant($next->activity->name, new DateTimeImmutable());

                $this->expectantConditions = array_map(fn($item) => new ExpectantCondition(
                    $item['target_type'],
                    $item['event_field'],
                    $item['target_field'],
                    $item['operator']
                ), $next->compareCondition);
            } else {
                $this->expectant = null;
            }

            if ($current instanceof IdleCheckpoint) {
                $this->record(
                    new IdleCheckpointReached(
                        $this->id->id,
                        $current->id(),
                        $this->journey()->id,
                        $this->target->identifier,
                        $this->expectant()?->activity->value
                    )
                );

                $this->status = Status::idle();

                $this->idleUntil = $current->until->toDateTimeImmutable();

                $this->journey->goTo($current->leftChildId());

                return;
            }
            if ($current instanceof ActionCheckpoint) {
                $this->record(
                    new ActionCheckpointReached(
                        $this->id->id,
                        $current->id(),
                        $current->notificationId,
                        $this->target->identifier,
                        $this->activities->asArray()
                    )
                );

                $this->journey->goTo($current->leftChildId());
            } elseif ($current instanceof EventOccurrenceCheckpoint) {

                $this->record(
                    new EventOccurrenceCheckpointReached(
                        $this->id->id,
                        $current->id(),
                        $current->activity->name->value
                    )
                );

                if ($this->activities->exists($current->activity)) {
                    $this->journey->goTo($current->leftChildId());
                } else {
                    $this->journey->goTo($current->rightChildId());
                }
            } elseif ($current instanceof CheckReachabilityCheckpoint) {
                $this->record(
                    new CheckReachabilityCheckpointReached(
                        $this->id->id,
                        $current->id(),
                        $current->channel
                    )
                );

                if ($this->target->isReachableWith($current->channel)) {
                    $this->journey->goTo($current->leftChildId());
                } else {
                    $this->journey->goTo($current->rightChildId());
                }
            } elseif ($current instanceof StopCheckpoint) {
                break;
            }
        }

        if ($this->journey->current() instanceof StopCheckpoint) {
            $this->status = Status::close();
            $this->record(new StopCheckpointReached($this->id->id, $this->journey->current()->id()));
        }
    }

    public function journey(): Journey
    {
        return $this->journey;
    }

    public function activities(): Activities
    {
        return $this->activities;
    }

    public function status(): Status
    {
        return $this->status;
    }

    public function triggerPayload(): array
    {
        return $this->triggerPayload;
    }

    public function idleUntil(): ?DateTimeImmutable
    {
        return $this->idleUntil;
    }

    public function expectant(): ?Expectant
    {
        return $this->expectant;
    }

    public function expectantConditions(): array
    {
        return $this->expectantConditions;
    }

    public function addActivity(Activity $activity): void
    {
        if (!$this->activities->existsById($activity)) {
            $this->activities = $this->activities->add($activity);
        }
    }

    public function validateActivity(Activity $activity): bool
    {
        /** @var ExpectantCondition $expectantCondition */

        foreach ($this->expectantConditions() as $expectantCondition) {
            if ($expectantCondition->type === ConditionType::Trigger->name) {
                if (!$activity->validatePayload(
                    $expectantCondition->field,
                    $this->triggerPayload[$expectantCondition->target] ?? null,
                    $expectantCondition->operator
                )) {
                    return false;
                }
            }
        }

        return true;
    }
}
