<?php

declare(strict_types=1);

namespace App\Schedule\Domain\Model;

use DateTimeImmutable;
use App\Schedule\Domain\ValueObject\ActivityName;

class Activity
{
    public function __construct(
        public readonly string            $id,
        public readonly ActivityName      $name,
        public readonly DateTimeImmutable $occurredAt,
        public readonly array             $payload = [],
        public readonly bool              $isTrigger = false
    )
    {
    }

    public function validatePayload(string $key, mixed $value, string $operator): bool
    {
        return $this->{'check' . $operator}($key, $value);
    }

    private function checkEqual(string $key, mixed $value): bool
    {
        return ($this->payload[$key] ?? null) === $value;
    }
}
