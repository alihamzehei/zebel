<?php

declare(strict_types=1);

namespace App\Schedule\Domain\Model;

use App\Shared\Domain\DomainError;
use App\Schedule\Domain\Model\Checkpoint\CheckpointInterface;

class Journey
{
    public function __construct(public readonly string $id,
                                public readonly string $name,
                                public readonly string $rootId,
                                private string $currentId,
                                public readonly string $trigger,
                                public readonly array $checkpoints,
                                public readonly bool $enable = true)
    {
    }

    public function goTo(string $id): void
    {
        $new = $this->findByIdOrThrow($id);
        $this->findByIdOrThrow($this->currentId)->check();
        $this->currentId = $new->id();
    }

    public function hasReachableCheckpoint(): bool
    {
        return $this->findByIdOrThrow($this->currentId)->leftChildId() !== null;
    }

    public function current(): CheckpointInterface
    {
        return $this->findByIdOrThrow($this->currentId);
    }

    public function findById(string $id): ?CheckpointInterface
    {
        foreach ($this->checkpoints as $checkpoint) {
            if ($checkpoint->id() === $id) {
                return $checkpoint;
            }
        }

        return null;
    }

    public function findByIdOrThrow(string $id): ?CheckpointInterface
    {
        return $this->findById($id) ?: throw new DomainError("No such checkpoint with id $id");
    }
}
