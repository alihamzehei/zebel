<?php

declare(strict_types=1);

namespace App\Schedule\Domain\Model\Checkpoint;

use App\Schedule\Domain\ValueObject\IdleTime;

class IdleCheckpoint implements CheckpointInterface
{
    public const TYPE = 'idle';

    public function __construct(private readonly string $id,
                                private readonly ?string $leftChildId,
                                private bool $isChecked,
                                public readonly IdleTime $until)
    {
    }

    public function id(): string
    {
        return $this->id;
    }

    public function leftChildId(): ?string
    {
        return $this->leftChildId;
    }

    public function rightChildId(): ?string
    {
        return null;
    }

    public function check(): void
    {
        $this->isChecked = true;
    }

    public function isChecked(): bool
    {
        return $this->isChecked;
    }
}
