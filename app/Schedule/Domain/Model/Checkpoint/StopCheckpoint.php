<?php

declare(strict_types=1);

namespace App\Schedule\Domain\Model\Checkpoint;

class StopCheckpoint implements CheckpointInterface
{
    public const TYPE = 'stop';

    public function __construct(private readonly string $id, private bool $isChecked)
    {
    }

    public function id(): string
    {
        return $this->id;
    }

    public function leftChildId(): ?string
    {
        return null;
    }

    public function rightChildId(): ?string
    {
        return null;
    }

    public function check(): void
    {
        $this->isChecked = true;
    }

    public function isChecked(): bool
    {
        return $this->isChecked;
    }
}
