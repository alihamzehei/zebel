<?php

declare(strict_types=1);

namespace App\Schedule\Domain\Model\Checkpoint;

class CheckReachabilityCheckpoint implements CheckpointInterface
{
    public const TYPE = 'reachabilityCondition';

    public function __construct(private readonly string $id,
                                private readonly string $leftChildId,
                                private readonly string $rightChildId,
                                private bool $isChecked,
                                public readonly string $channel)
    {
    }

    public function id(): string
    {
        return $this->id;
    }

    public function leftChildId(): ?string
    {
        return $this->leftChildId;
    }

    public function rightChildId(): ?string
    {
        return $this->rightChildId;
    }

    public function check(): void
    {
        $this->isChecked = true;
    }

    public function isChecked(): bool
    {
        return $this->isChecked;
    }
}
