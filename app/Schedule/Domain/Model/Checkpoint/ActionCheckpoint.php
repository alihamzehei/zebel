<?php

declare(strict_types=1);

namespace App\Schedule\Domain\Model\Checkpoint;

class ActionCheckpoint implements CheckpointInterface
{
    public const TYPE = 'action';

    public function __construct(private readonly string $id,
                                private readonly ?string $leftChildId,
                                private bool $isChecked,
                                public readonly string $notificationId)
    {
    }

    public function id(): string
    {
        return $this->id;
    }

    public function leftChildId(): ?string
    {
        return $this->leftChildId;
    }

    public function rightChildId(): ?string
    {
        return null;
    }

    public function check(): void
    {
        $this->isChecked = true;
    }

    public function isChecked(): bool
    {
        return $this->isChecked;
    }
}
