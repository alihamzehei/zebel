<?php

declare(strict_types=1);

namespace App\Schedule\Domain\Model\Checkpoint;

interface CheckpointInterface
{
    public function id(): string;

    public function leftChildId(): ?string;

    public function rightChildId(): ?string;

    public function check(): void;

    public function isChecked(): bool;
}
