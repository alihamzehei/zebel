<?php

declare(strict_types=1);

namespace App\Schedule\Domain\Model;

class Target
{
    public function __construct(public readonly int $identifier,
                                public readonly array $channels)
    {
    }

    public function isReachableWith(string $channel): bool
    {
        return in_array($channel, $this->channels, true);
    }
}
