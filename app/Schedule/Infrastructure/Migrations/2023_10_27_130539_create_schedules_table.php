<?php

use App\Schedule\Domain\ValueObject\Status;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('schedule_journeys', function (Blueprint $table) {
            $table->id();
            $table->uuid()->unique()->index();
            $table->string('name');
            $table->string('trigger');
            $table->uuid('root_id');
            $table->uuid('current_id');
            $table->boolean('enable')->default(false);
            $table->json('checkpoints');
            $table->timestamps();
        });

        Schema::create('schedule_schedules', function (Blueprint $table) {
            $table->id();
            $table->uuid()->unique()->index();
            $table->uuid('journey_id');
            $table->string('journey_name');
            $table->uuid('journey_root_id');
            $table->string('journey_trigger');
            $table->uuid('journey_current_id');
            $table->json('journey_checkpoints');
            $table->json('trigger_payload')->nullable();
            $table->text('target_channels');
            $table->enum('status', [Status::INIT, Status::IDLE, Status::CLOSE, Status::DISPATCH]);
            $table->dateTime('idle_unit')->nullable();
            $table->string('expectant_name')->nullable();
            $table->datetime('expectant_date')->nullable();
            $table->json('expectant_conditions')->nullable();
            $table->uuid('activity_id')->nullable();
            $table->json('activities')->nullable();
            $table->integer('target_id');
            $table->dateTime('idle_until')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('schedule_schedules');
        Schema::dropIfExists('schedule_journeys');
    }
};
