<?php

declare(strict_types=1);

namespace App\Schedule\Infrastructure\Model;

use Illuminate\Database\Eloquent\Model;

class ScheduleJourneyModel extends Model
{
    protected $table = 'schedule_journeys';

    protected $fillable = [
        'uuid',
        'name',
        'trigger',
        'root_id',
        'current_id',
        'enable',
        'checkpoints',
    ];

    protected $casts = [
        'enable' => 'boolean',
        'checkpoints' => 'array',
    ];
}
