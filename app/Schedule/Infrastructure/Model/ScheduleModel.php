<?php

declare(strict_types=1);

namespace App\Schedule\Infrastructure\Model;

use App\Schedule\Domain\Model\Journey;
use App\Schedule\Domain\Model\Target;
use App\Schedule\Domain\Model\Schedule;
use Illuminate\Database\Eloquent\Model;
use App\Schedule\Domain\ValueObject\ScheduleId;

class ScheduleModel extends Model
{
    protected $table = 'schedule_schedules';

    protected $fillable = [
        'uuid',
        'target_channels',
        'target_id',
        'journey_name',
        'journey_root_id',
        'journey_trigger',
        'journey_current_id',
        'journey_checkpoints',
        'trigger_payload',
        'status',
        'idle_until',
        'expectant_name',
        'expectant_date',
        'expectant_conditions',
        'created_at',
        'activity_id',
        'journey_id',
        'activities'
    ];

    protected $casts = [
        'expectant_date' => 'immutable_datetime',
        'idle_until' => 'immutable_datetime',
        'target_channels' => 'array',
        'journey_checkpoints' => 'array',
        'trigger_payload' => 'array',
        'expectant_conditions' => 'array',
        'activities' => 'array'
    ];

    public function toDomain(): Schedule
    {
        // return new Schedule(
        //     new ScheduleId($this->uuid, $this->activity_id),
        //     new Target(
        //         $this->target_id,
        //         $this->target_channels,
        //     ),
        //     new Journey()
        // );
        // return new Schedule(
        //     new ScheduleId($schedule['uuid'], $schedule['activity_id']),
        //     new Target(
        //         $schedule['target_id'],
        //         $schedule['target_channels']
        //     ),
        //     $this->deserializeJourney($schedule['journey'], true),
        //     new Status($schedule['status']),
        //     null,
        //     $schedule['idle_until'],
        //     $schedule['expectant_name'] ? new Expectant(new ActivityName($schedule['expectant_name']), $schedule['expectant_date']) : null,
        // );

    }
}
