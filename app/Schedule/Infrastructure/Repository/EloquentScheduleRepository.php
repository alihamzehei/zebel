<?php

declare(strict_types=1);

namespace App\Schedule\Infrastructure\Repository;

use App\Schedule\Domain\Model\Activity;
use App\Schedule\Domain\ValueObject\Activities;
use App\Schedule\Domain\ValueObject\ExpectantCondition;
use App\Schedule\Presentation\Module\ActivityModule;
use DateTimeImmutable;
use Exception;
use Ramsey\Uuid\Uuid;
use App\Schedule\Domain\Model\Target;
use App\Schedule\Domain\Model\Journey;
use App\Schedule\Domain\Model\Schedule;
use App\Schedule\Domain\ValueObject\Status;
use App\Schedule\Infrastructure\Model\ScheduleModel;
use App\Schedule\Domain\ValueObject\Expectant;
use App\Schedule\Domain\ValueObject\ScheduleId;
use App\Schedule\Domain\ValueObject\ActivityName;
use App\Schedule\Domain\Repository\ScheduleRepository;
use App\Schedule\Application\Serializer\JourneySerializer;

class EloquentScheduleRepository implements ScheduleRepository
{
    private array $seen = [];

    public function __construct(
        private readonly JourneySerializer $serializer,
        private readonly ActivityModule    $activityModule
    )
    {
    }

    public function nextIdentity(): ScheduleId
    {
        return new ScheduleId(Uuid::uuid4()->toString());
    }

    public function findRunnable(): array
    {
        return ScheduleModel::query()->where('status', Status::init()->state)
            ->orWhere([
                ['status', Status::idle()->state],
                ['idle_until', '<>', null],
                ['idle_until', '<=', now()],
            ])
            ->get()
            ->map(fn(ScheduleModel $schedule) => $this->eloquentToDomain($schedule))
            ->toArray();
    }

    /**
     * @return ScheduleId[]
     */
    public function findRunnableIds(): array
    {
        return ScheduleModel::query()->where('status', Status::init()->state)
            ->orWhere([
                ['status', Status::idle()->state],
                ['idle_until', '<>', null],
                ['idle_until', '<=', now()],
            ])
            ->get()
            ->map(fn(ScheduleModel $schedule) => $this->eloquentToDomain($schedule))
            ->toArray();
    }


    public function seen(): array
    {
        return $this->seen;
    }

    /**
     * @throws Exception
     */
    private function eloquentToDomain(ScheduleModel $schedule): Schedule
    {
        $activities = $this->activityModule->getActivityByIds($schedule['activities'] ?? []);

        $activityModel = [];
        foreach ($activities as $activity) {
            $activityModel[] = new Activity(
                $activity['uuid'],
                new ActivityName($activity['title']),
                new DateTimeImmutable($activity['occurred_at']),
                $activity['payload'],
                $activity['uuid'] === $schedule['activity_id']
            );
        }

        return new Schedule(
            new ScheduleId($schedule['uuid'], $schedule['activity_id']),
            new Target(
                $schedule['target_id'],
                $schedule['target_channels']
            ),
            new Journey(
                $schedule->journey_id,
                $schedule->journey_name,
                $schedule->journey_root_id,
                $schedule->journey_current_id,
                $schedule->journey_trigger,
                array_map([$this->serializer, 'deserialize'], $schedule->journey_checkpoints),
            ),
            new Status($schedule['status']),
            new Activities(...$activityModel),
            $schedule['idle_until'],
            $schedule['expectant_name'] ?
                new Expectant(new ActivityName($schedule['expectant_name']), $schedule['expectant_date']) :
                null,
            triggerPayload: $schedule->trigger_payload,
            expectantConditions: array_map(fn($item) => new ExpectantCondition(
                $item['target_type'],
                $item['target_field'],
                $item['operator'],
                $item['event_field']
            ),
                $schedule['expectant_conditions']
            )
        );
    }

    public function findById(ScheduleId $id): ?Schedule
    {
        if ($schedule = ScheduleModel::query()->firstWhere('uuid', $id->id)) {
            return $this->eloquentToDomain($schedule);
        }

        return null;
    }

    public function persist(Schedule $schedule): void
    {
        ScheduleModel::query()->updateOrCreate(['uuid' => $schedule->id->id], [
            'target_channels' => $schedule->target->channels,
            'target_id' => $schedule->target->identifier,
            'journey_id' => $schedule->journey()->id,
            'journey_name' => $schedule->journey()->name,
            'journey_trigger' => $schedule->journey()->trigger,
            'journey_root_id' => $schedule->journey()->rootId,
            'journey_current_id' => $schedule->journey()->current()->id(),
            'journey_checkpoints' => array_map(
                [$this->serializer, 'serialize'],
                $schedule->journey()->checkpoints
            ),
            'trigger_payload' => $schedule->triggerPayload(),
            'status' => $schedule->status()->state,
            'idle_until' => $schedule->idleUntil(),
            'expectant_name' => $schedule->expectant()?->activity->value,
            'expectant_date' => $schedule->expectant()?->since,
            'expectant_conditions' => array_map(fn(ExpectantCondition $item) => [
                'target_type' => $item->type,
                'event_field' => $item->field,
                'target_field' => $item->target,
                'operator' => $item->operator,
            ], $schedule->expectantConditions()),
            'activity_id' => $schedule->id->activityId,
            'activities' => array_map(fn($item) => $item->id, $schedule->activities()->asArray()),
        ]);

        $this->seen[] = $schedule;
    }
}
