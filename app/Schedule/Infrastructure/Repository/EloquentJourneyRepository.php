<?php

declare(strict_types=1);

namespace App\Schedule\Infrastructure\Repository;

use App\Schedule\Domain\Model\Journey;
use App\Schedule\Infrastructure\Model\ScheduleJourneyModel;
use App\Schedule\Domain\Repository\JourneyRepository;
use App\Schedule\Application\Serializer\JourneySerializer;

class EloquentJourneyRepository implements JourneyRepository
{
    public function __construct(private readonly JourneySerializer $serializer)
    {
    }

    public function findById(string $id): ?Journey
    {
        $journey = ScheduleJourneyModel::query()->where('uuid', $id)->first();

        if ($journey === null) {
            return null;
        }

        return new Journey(
            $journey['uuid'],
            $journey['name'],
            $journey['root_id'],
            $journey['current_id'],
            $journey['trigger'],
            array_map([$this->serializer, 'deserialize'], $journey['checkpoints']),
            (bool) $journey['enable']
        );
    }

    public function deleteById(string $id): ?string
    {
        $response = ScheduleJourneyModel::query()->firstWhere('uuid', $id)->delete();

        if (!$response) {
            return null;
        }

        return $id;
    }

    public function findByTrigger(string $trigger): array
    {
        $journeys = ScheduleJourneyModel::query()
            ->where('trigger', $trigger)
            ->where('enable', true)
            ->get();

        if (empty($journeys)) {
            return [];
        }

        $journeysArray = [];

        foreach ($journeys as $journey) {
            $journeysArray[] = new Journey(
                $journey['uuid'],
                $journey['name'],
                $journey['root_id'],
                $journey['current_id'],
                $journey['trigger'],
                array_map([$this->serializer, 'deserialize'], $journey['checkpoints']),
                (bool) $journey['enable']
            );
        }

        return $journeysArray;
    }

    public function persist(Journey $journey): void
    {
        ScheduleJourneyModel::query()->create([
            'uuid' => $journey->id,
            'name' => $journey->name,
            'root_id' => $journey->rootId,
            'current_id' => $journey->current()->id(),
            'trigger' => $journey->trigger,
            'checkpoints' => array_map([$this->serializer, 'serialize'], $journey->checkpoints),
            'enable' => $journey->enable,
        ]);
    }

    public function nextIdentity()
    {
    }

    public function seen(): array
    {
        return [];
    }
}
