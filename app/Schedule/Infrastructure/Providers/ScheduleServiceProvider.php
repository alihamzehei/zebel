<?php

namespace App\Schedule\Infrastructure\Providers;

use Illuminate\Support\ServiceProvider;
use App\Schedule\Domain\Repository\JourneyRepository;
use App\Schedule\Domain\Repository\ScheduleRepository;
use App\Schedule\Infrastructure\Repository\EloquentJourneyRepository;
use App\Schedule\Infrastructure\Repository\EloquentScheduleRepository;


class ScheduleServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->bind(JourneyRepository::class, EloquentJourneyRepository::class);
        $this->app->bind(ScheduleRepository::class, EloquentScheduleRepository::class);
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        $this->loadMigrationsFrom(dirname(__DIR__ ) . '/Migrations');
    }
}
