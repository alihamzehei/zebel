<?php

declare(strict_types=1);

namespace App\Plan\Infrastructure\Model;

use App\Plan\Domain\Model\Journey;
use App\Plan\Domain\Model\Trigger;
use App\Plan\Domain\ValueObject\Name;
use Illuminate\Database\Eloquent\Model;
use App\Plan\Domain\ValueObject\JourneyId;
use App\Plan\Domain\ValueObject\CheckpointAggregate;
use App\Plan\Domain\Factory\CheckpointAggregateFactory;
use App\Plan\Domain\Model\Checkpoint\EventOccurrenceConditionalCheckpoint;
use App\Plan\Domain\Model\Checkpoint\StopCheckpoint;
use App\Plan\Domain\ValueObject\CheckpointId;

// use App\Notification\Infrastructure\Eloquent\Model\NotificationModel;

class JourneyModel extends Model
{
    protected $fillable = [
        'uuid',
        'name',
        'trigger',
        'root',
        'meta',
        'enable',
    ];

    protected $casts = [
        'enable' => 'bool',
        'meta' => 'array',
    ];

    protected $table = 'plan_journeys';

    public function checkpoints()
    {
        return $this->hasMany(CheckpointModel::class, 'journey_id');
    }

    // public function notifications()
    // {
    //     return $this->hasMany(NotificationModel::class, 'journey_id');
    // }

    public function toDomain(): Journey
    {
        return new Journey(
            new JourneyId($this->uuid),
            new Name($this->name),
            Trigger::fromString($this->trigger),
            $this->meta,
            $this->checkpointAggregateFromArray(
                $this->root,
                $this->transformPersistedCheckpoints($this->checkpoints->toArray())
            ),
            (bool) $this->enable
        );
    }

    private function checkpointAggregateFromArray(string $root, array $checkpoints): CheckpointAggregate
    {
        $factory = new CheckpointAggregateFactory($root, $checkpoints, false);

        return $factory->create();
    }

    private function transformPersistedCheckpoints(array $checkpoints): array
    {
        return array_map(function ($checkpoint) {
            $checkpoint['next'] = [];
            if ($checkpoint['left_child_id']) {
                $checkpoint['next'][] = $checkpoint['left_child_id'];
            }
            if ($checkpoint['right_child_id']) {
                $checkpoint['next'][] = $checkpoint['right_child_id'];
            }

            $checkpoint['id'] = $checkpoint['uuid'];

            return $checkpoint;
        }, $checkpoints);
    }
}
