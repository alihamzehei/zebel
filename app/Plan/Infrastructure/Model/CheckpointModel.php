<?php

declare(strict_types=1);

namespace App\Plan\Infrastructure\Model;

use Illuminate\Database\Eloquent\Model;

class CheckpointModel extends Model
{
    protected $fillable = [
        'uuid',
        'journey_id',
        'journey_uuid',
        'type',
        'payload',
        'meta',
        'left_child_id',
        'right_child_id',
    ];

    protected $table = 'plan_checkpoints';

    public $timestamps = false;

    protected $casts = [
        'meta' => 'array',
        'payload' => 'array',
    ];

    public function journey()
    {
        return $this->belongsTo(JourneyModel::class, 'journey_id');
    }
}
