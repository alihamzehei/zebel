<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('plan_journeys', function (Blueprint $table) {
            $table->id();
            $table->uuid()->unique()->index();
            $table->string('name');
            $table->string('trigger');
            $table->string('root');
            $table->json('meta')->nullable();
            $table->boolean('enable');
            $table->timestamps();
        });

        Schema::create('plan_checkpoints', function (Blueprint $table) {
            $table->id();
            $table->uuid()->unique()->index();
            $table->integer('journey_id');
            $table->uuid('journey_uuid');
            $table->string('type');
            $table->json('meta')->nullable();
            $table->json('payload')->nullable();
            $table->string('left_child_id')->nullable();
            $table->string('right_child_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('plan_journeys');
        Schema::dropIfExists('plan_checkpoints');
    }
};
