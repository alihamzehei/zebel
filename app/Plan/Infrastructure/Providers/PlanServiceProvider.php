<?php

namespace App\Plan\Infrastructure\Providers;

use App\Plan\Infrastructure\Repository\EloquentJourneyRepository;
use App\Plan\Domain\Repository\JourneyRepository;
use Illuminate\Support\ServiceProvider;


class PlanServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->bind(JourneyRepository::class, EloquentJourneyRepository::class);
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        $this->loadMigrationsFrom(dirname(__DIR__ ) . '/Migrations');
    }
}
