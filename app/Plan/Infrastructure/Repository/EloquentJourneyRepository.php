<?php

declare(strict_types=1);

namespace App\Plan\Infrastructure\Repository;

use App\Plan\Domain\Model\Journey;
use App\Plan\Infrastructure\Model\JourneyModel;
use App\Plan\Domain\ValueObject\JourneyId;
use App\Plan\Domain\Model\Checkpoint\Checkpoint;
use App\Plan\Domain\Repository\JourneyRepository;
use App\Plan\Application\Dto\CheckpointDtoFactory;

class EloquentJourneyRepository implements JourneyRepository
{
    private array $seen = [];

    public function persist(Journey $journey): void
    {
        $eloquentJourney = JourneyModel::query()->updateOrCreate(['uuid' => $journey->id->id], [
            'uuid' => $journey->id->id,
            'name' => $journey->name->value,
            'trigger' => $journey->trigger->name,
            'root' => $journey->checkpoints->root->id,
            'meta' => $journey->metaData,
            'enable' => (int) $journey->enable,
        ]);

        foreach ($journey->checkpoints->checkpoints() as $checkpoint) {
            $eloquentJourney->checkpoints()->updateOrCreate(['uuid' => $checkpoint->id()->id], [
                'uuid' => $checkpoint->id()->id,
                'journey_id' => $eloquentJourney->id,
                'journey_uuid' => $eloquentJourney->uuid,
                'type' => $checkpoint->type(),
                'meta' => $checkpoint->metaData,
                'payload' => $this->payloadFor($checkpoint),
                'left_child_id' => $checkpoint->id()->leftChildId?->id,
                'right_child_id' => $checkpoint->id()->rightChildId?->id,
            ]);
        }

        $this->seen[] = $journey;
    }

    private function payloadFor(Checkpoint $checkpoint): ?array
    {
        return CheckpointDtoFactory::from($checkpoint)->asArray()['payload'] ?? null;
    }

    public function findById(JourneyId $id): ?Journey
    {
        $journey = JourneyModel::query()
            ->where('uuid', $id->id)
            ->first()
            ?->toDomain();

        if (!$journey) {
            return null;
        }
        $journey->meta['persisted'] = true;

        return $journey;
    }

    public function nextIdentity(): JourneyId
    {
        return new JourneyId(uniqid());
    }

    public function seen(): array
    {
        return $this->seen;
    }

    public function deleteById(JourneyId $id): void
    {
        $journey = JourneyModel::query()->firstWhere('uuid', $id->id);
        $journey->delete();

        $journey->checkpoints()->delete();
    }
}
