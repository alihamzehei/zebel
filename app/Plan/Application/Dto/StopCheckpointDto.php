<?php

declare(strict_types=1);

namespace App\Plan\Application\Dto;

use App\Plan\Domain\Model\Checkpoint\Checkpoint;
use App\Plan\Domain\Model\Checkpoint\StopCheckpoint;

class StopCheckpointDto extends CheckpointDto
{
    public function __construct(private StopCheckpoint $checkpoint)
    {
    }

    protected function checkpoint(): Checkpoint
    {
        return $this->checkpoint;
    }

    public function asArray(): array
    {
        return [
            'id' => $this->id(),
            'type' => $this->type(),
            'meta' => $this->checkpoint->metaData,
        ];
    }
}
