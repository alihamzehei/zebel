<?php

declare(strict_types=1);

namespace App\Plan\Application\Dto;

use App\Plan\Domain\Model\Trigger;
use App\Shared\Application\Dto;

class TriggerDto extends Dto
{
    public function asArray(): array
    {
        return [
            'items' => array_map(
                fn (Trigger $trigger) => ['name' => $trigger->name, 'value' => $trigger->name],
                Trigger::cases()
            ),
        ];
    }
}
