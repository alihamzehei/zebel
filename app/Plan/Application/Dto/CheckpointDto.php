<?php

declare(strict_types=1);

namespace App\Plan\Application\Dto;

use App\Shared\Application\Dto\Dto;
use App\Plan\Domain\Model\Checkpoint\Checkpoint;

abstract class CheckpointDto extends Dto
{
    public function id(): string
    {
        return $this->checkpoint()->id()->id;
    }

    public function type(): string
    {
        return $this->checkpoint()->type();
    }

    abstract protected function checkpoint(): Checkpoint;
}
