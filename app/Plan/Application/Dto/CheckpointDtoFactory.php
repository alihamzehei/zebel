<?php

declare(strict_types=1);

namespace App\Plan\Application\Dto;

use App\Plan\Domain\Model\Checkpoint\ActionCheckpoint;
use App\Plan\Domain\Model\Checkpoint\Checkpoint;
use App\Plan\Domain\Model\Checkpoint\EventOccurrenceConditionalCheckpoint;
use App\Plan\Domain\Model\Checkpoint\IdleCheckpoint;
use App\Plan\Domain\Model\Checkpoint\ReachabilityConditionalCheckpoint;
use App\Plan\Domain\Model\Checkpoint\StopCheckpoint;

class CheckpointDtoFactory
{
    public static function from(Checkpoint $checkpoint): CheckpointDto
    {
        return (new self())->createDto($checkpoint);
    }

    public function createDto(Checkpoint $checkpoint): CheckpointDto
    {
        return match (get_class($checkpoint)) {
            IdleCheckpoint::class => new IdleCheckpointDto($checkpoint),
            ActionCheckpoint::class => new ActionCheckpointDto($checkpoint),
            ReachabilityConditionalCheckpoint::class => new ReachabilityConditionalCheckpointDto($checkpoint),
            EventOccurrenceConditionalCheckpoint::class => new EventOccurrenceConditionalCheckpointDto($checkpoint),
            StopCheckpoint::class => new StopCheckpointDto($checkpoint),
        };
    }
}
