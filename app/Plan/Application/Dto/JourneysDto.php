<?php

declare(strict_types=1);

namespace App\Plan\Application\Dto;

use App\Shared\Application\Dto;

readonly class JourneysDto extends Dto
{
    public function __construct(public array $journeys)
    {
    }

    public function asArray(): array
    {
        return [
            'total_page' => $this->journeys['total_page'],
            'journeys' => array_map(fn ($journeys): array => [
                'id' => $journeys['id'],
                'name' => $journeys['name'],
                'trigger' => $journeys['trigger'],
                'enable' => (bool) $journeys['enable'],
                'started_at' => $journeys['created_at'] ? strtotime($journeys['created_at']) : null,
            ], $this->journeys['items']),
        ];
    }
}
