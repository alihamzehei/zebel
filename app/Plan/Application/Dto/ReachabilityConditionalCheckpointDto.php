<?php

declare(strict_types=1);

namespace App\Plan\Application\Dto;

use App\Plan\Domain\Model\Checkpoint\Checkpoint;
use App\Plan\Domain\Model\Checkpoint\ReachabilityConditionalCheckpoint;

class ReachabilityConditionalCheckpointDto extends CheckpointDto
{
    public function __construct(private readonly ReachabilityConditionalCheckpoint $checkpoint)
    {
    }

    protected function checkpoint(): Checkpoint
    {
        return $this->checkpoint;
    }

    public function asArray(): array
    {
        return [
            'id' => $this->id(),
            'type' => $this->type(),
            'meta' => $this->checkpoint->metaData,
            'payload' => [
                'channel' => $this->checkpoint->userReachabilityCondition,
            ],
            'next' => [$this->checkpoint->id()->leftChildId?->id, $this->checkpoint->id()->rightChildId?->id],
        ];
    }
}
