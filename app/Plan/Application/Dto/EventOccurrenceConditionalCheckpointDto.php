<?php

declare(strict_types=1);

namespace App\Plan\Application\Dto;

use App\Plan\Domain\Model\Checkpoint\Checkpoint;
use App\Plan\Domain\Model\Checkpoint\EventOccurrenceConditionalCheckpoint;

class EventOccurrenceConditionalCheckpointDto extends CheckpointDto
{
    public function __construct(private readonly EventOccurrenceConditionalCheckpoint $checkpoint)
    {
    }

    protected function checkpoint(): Checkpoint
    {
        return $this->checkpoint;
    }

    public function asArray(): array
    {
        return [
            'id' => $this->id(),
            'type' => $this->type(),
            'meta' => $this->checkpoint->metaData,
            'payload' => [
                'condition' => $this->checkpoint->eventOccurrenceCondition,
                'compare_condition' => $this->checkpoint->compareConditions,
            ],
            'next' => [$this->checkpoint->id()->leftChildId?->id, $this->checkpoint->id()->rightChildId?->id],
        ];
    }
}
