<?php

declare(strict_types=1);

namespace App\Plan\Application\Dto;

use App\Plan\Domain\Model\Checkpoint\Checkpoint;
use App\Plan\Domain\Model\Journey;
use App\Shared\Application\Dto;

readonly class JourneyDto extends Dto
{
    public function __construct(public Journey $journey)
    {
    }

    public function asArray(): array
    {
        return [
            'id' => $this->journey->id->id,
            'name' => $this->journey->name->value,
            'root' => $this->journey->checkpoints->root->id,
            'trigger' => $this->journey->trigger->name,
            'meta' => $this->journey->metaData,
            'checkpoints' => array_map(fn (Dto $dto) => $dto->asArray(), array_values($this->checkpoints())),
            'enable' => $this->journey->enable,
        ];
    }

    public function id(): string
    {
        return $this->journey->id->id;
    }

    public function name(): string
    {
        return $this->journey->name->value;
    }

    public function root(): string
    {
        return $this->journey->checkpoints->root->id;
    }

    public function checkpoints(): array
    {
        return array_map(
            fn (Checkpoint $checkpoint) => CheckpointDtoFactory::from($checkpoint),
            $this->journey->checkpoints->checkpoints()
        );
    }
}
