<?php

declare(strict_types=1);

namespace App\Plan\Application\Dto;

use App\Plan\Domain\Model\Checkpoint\Checkpoint;
use App\Plan\Domain\Model\Checkpoint\IdleCheckpoint;

class IdleCheckpointDto extends CheckpointDto
{
    public function __construct(private IdleCheckpoint $checkpoint)
    {
    }

    protected function checkpoint(): Checkpoint
    {
        return $this->checkpoint;
    }

    public function asArray(): array
    {
        return [
            'id' => $this->id(),
            'type' => $this->type(),
            'meta' => $this->checkpoint->metaData,
            'payload' => $this->payload(),
            'next' => [$this->checkpoint->id()->leftChildId?->id],
        ];
    }

    public function payload(): array
    {
        return [
            'time' => $this->checkpoint->timer->asMinutes(),
        ];
    }
}
