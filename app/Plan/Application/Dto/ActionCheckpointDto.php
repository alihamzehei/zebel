<?php

declare(strict_types=1);

namespace App\Plan\Application\Dto;

use App\Plan\Domain\Model\Checkpoint\ActionCheckpoint;
use App\Plan\Domain\Model\Checkpoint\Checkpoint;

class ActionCheckpointDto extends CheckpointDto
{
    public function __construct(private readonly ActionCheckpoint $checkpoint)
    {
    }

    protected function checkpoint(): Checkpoint
    {
        return $this->checkpoint;
    }

    public function asArray(): array
    {
        return [
            'id' => $this->id(),
            'type' => $this->checkpoint->type(),
            'meta' => $this->checkpoint->metaData,
            'payload' => ['notification_id' => $this->checkpoint->notificationId],
            'next' => [$this->checkpoint->id()->leftChildId?->id],
        ];
    }
}
