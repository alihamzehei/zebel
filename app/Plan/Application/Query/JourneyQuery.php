<?php

declare(strict_types=1);

namespace App\Plan\Application\Query;

use App\Shared\Application\Query\QueryInterface;

class JourneyQuery implements QueryInterface
{
    public function __construct(public readonly string $id)
    {
    }
}
