<?php

declare(strict_types=1);

namespace App\Plan\Application\Query;

use App\Plan\Infrastructure\Model\JourneyModel;
use App\Plan\Application\Query\JourneyQuery;
use App\Shared\Application\Query\QueryInterface;
use App\Shared\Application\Query\QueryHandlerInterface;
use Illuminate\Support\Collection;

class JourneyQueryHandler implements QueryHandlerInterface
{
    public function handle(QueryInterface|JourneyQuery $query): array
    {
        $journey = JourneyModel::query()
            ->with('checkpoints')
            ->where('uuid', $query->id)
            ->firstOrFail();

        return [
            'id' => $journey->uuid,
            'name' => $journey->name,
            'root' => $journey->root,
            'trigger' => $journey->trigger,
            'meta' => $journey->meta,
            'enable' => $journey->enable,
            'checkpoints' => $this->checkpointsToArray($journey->checkpoints),
        ];
    }

    private function checkpointsToArray(Collection $checkpoints): array
    {
        $results = [];

        foreach ($checkpoints as $checkpoint) {
            $result = [
                'id' => $checkpoint->uuid,
                'type' => $checkpoint->type,
                'meta' => $checkpoint->meta,
                'payload' => $checkpoint->payload,
                'next' => []
            ];

            if ($checkpoint->left_child_id) {
                $result['next'][] = $checkpoint->left_child_id;
            }

            if ($checkpoint->right_child_id) {
                $result['next'][] = $checkpoint->right_child_id;
            }

            $results []= $result;
        }

        return $results;
    }
}
