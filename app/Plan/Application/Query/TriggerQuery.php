<?php

declare(strict_types=1);

namespace App\Plan\Application\Query;

use App\Shared\Application\Query\QueryInterface;

class TriggerQuery implements QueryInterface
{
}
