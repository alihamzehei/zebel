<?php

declare(strict_types=1);

namespace App\Plan\Application\Query;

use App\Plan\Infrastructure\Model\JourneyModel;
use App\Plan\Application\Query\JourneysQuery;
use App\Shared\Application\Query\QueryInterface;
use App\Shared\Application\Query\QueryHandlerInterface;
use JsonSerializable;

class JourneysQueryHandler implements QueryHandlerInterface
{
    public function handle(QueryInterface|JourneysQuery $query): JsonSerializable|array
    {
        $builder = JourneyModel::query();

        if ($query->name) {
            $builder->where('name', 'like', "%{$query->name}%");
        }

        return $builder->paginate(page: $query->pageNumber)->toArray();
    }
}
