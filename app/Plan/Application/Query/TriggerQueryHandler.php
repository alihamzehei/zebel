<?php

declare(strict_types=1);

namespace App\Plan\Application\Query;

use JsonSerializable;
use App\Plan\Domain\Model\Trigger;
use App\Plan\Application\Query\TriggerQuery;
use App\Shared\Application\Query\QueryInterface;
use App\Shared\Application\Query\QueryHandlerInterface;

class TriggerQueryHandler implements QueryHandlerInterface
{
    public function handle(QueryInterface|TriggerQuery $query): JsonSerializable|array
    {
        $customerData = ['name', 'lastname', 'email', 'mobile'];
        $productData = ['product_id', 'product_link'];

        $eventAttributesMap = [
            Trigger::CustomerRegistered->name => $customerData,
            Trigger::CartCreated->name => [...$customerData, 'cart_id', ...$productData],
            Trigger::ProductSeen->name => [...$customerData, ...$productData],
            Trigger::ProductAddedToWishList->name => [...$customerData, ...$productData],
            Trigger::ProductRemovedFromWishList->name => [...$customerData, ...$productData],
            Trigger::CartRemoved->name => [...$customerData, 'cart_id'],
            Trigger::OrderPaid->name => [...$customerData, 'order_id', 'product_id'],
            Trigger::OrderStored->name => [...$customerData, 'order_id'],
            Trigger::OrderLineCanceled->name => [...$customerData, 'order_id', 'order_item_id', 'cancel_reason', ...$productData],
        ];

        return [
            'items' => array_map(
                fn(Trigger $trigger) => ['name' => $trigger->name, 'value' => $trigger->name, 'fields' => $eventAttributesMap[$trigger->name]],
                Trigger::cases()
            ),
        ];
    }
}
