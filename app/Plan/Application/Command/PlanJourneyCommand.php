<?php

declare(strict_types=1);

namespace App\Plan\Application\Command;

use App\Plan\Domain\Model\Checkpoint\IdleCheckpoint;
use App\Plan\Domain\Model\Checkpoint\StopCheckpoint;
use App\Plan\Domain\Model\ConditionType;
use App\Plan\Domain\Model\Operator;
use App\Shared\Application\Command\CommandInterface;
use App\Plan\Domain\Model\Checkpoint\ActionCheckpoint;
use App\Shared\Application\Error\InvalidCommandSyntaxException;
use App\Plan\Domain\Model\Checkpoint\ReachabilityConditionalCheckpoint;
use App\Plan\Domain\Model\Checkpoint\EventOccurrenceConditionalCheckpoint;

class PlanJourneyCommand implements CommandInterface
{
    protected const CHECKPOINT_TYPES = [
        IdleCheckpoint::TYPE,
        ActionCheckpoint::TYPE,
        ReachabilityConditionalCheckpoint::TYPE,
        EventOccurrenceConditionalCheckpoint::TYPE,
        StopCheckpoint::TYPE,
    ];

    protected const OPERATOR = [
        Operator::Equal->name,
    ];

    protected const CONDITION_TYPES = [
        ConditionType::Trigger->name,
    ];

    public function __construct(public readonly string $name,
                                public readonly string $trigger,
                                public readonly string $root,
                                public readonly array  $metaData,
                                public readonly array  $checkpoints)
    {
        $this->validateIdentifier($root);
        $this->validateCheckpoints();
    }

    protected function validateCheckpoints(): void
    {
        foreach ($this->checkpoints as $checkpoint) {
            $type = $checkpoint['type'];
            $this->validateCheckpointName($type);

            switch ($type) {
                case IdleCheckpoint::TYPE:
                    $this->validateIdle($checkpoint);
                    break;
                case ActionCheckpoint::TYPE:
                    $this->validateAction($checkpoint);
                    break;
                case ReachabilityConditionalCheckpoint::TYPE:
                    $this->validateChannel($checkpoint);
                    break;
                case EventOccurrenceConditionalCheckpoint::TYPE:
                    $this->validateCondition($checkpoint);
                    break;
                default:
                    break;
            }
        }
    }

    protected function validateCheckpointName(string $name): void
    {
        if (!\in_array($name, self::CHECKPOINT_TYPES, true)) {
            throw new InvalidCommandSyntaxException("Invalid checkpoint type $name");
        }
    }

    protected function validateIdle(array $checkpoint): void
    {
        if (!isset($checkpoint['payload']['time'])) {
            throw new InvalidCommandSyntaxException('Idle checkpoint must contains time');
        }

        if (!isset($checkpoint['next'])) {
            throw new InvalidCommandSyntaxException('Idle checkpoint must contains next');
        }

        $this->validateIdentifier($checkpoint['next'][0]);
    }

    protected function validateAction(array $checkpoint): void
    {
        if (!isset($checkpoint['payload']['notification_id'])) {
            throw new InvalidCommandSyntaxException('Action checkpoint myst contains notification_id');
        }

        $this->validateIdentifier($checkpoint['next'][0]);
    }

    /**
     * @throws InvalidCommandSyntaxException
     */
    protected function validateCondition(array $checkpoint): void
    {
        if (!isset($checkpoint['payload']['condition'])) {
            throw new InvalidCommandSyntaxException('Condition checkpoint must contains condition expression');
        }

        $compareConditions = $checkpoint['payload']['compare_condition'] ?? false;

        if ($compareConditions) {
            if (!is_array($compareConditions)) {
                throw new InvalidCommandSyntaxException('compare condition of checkpoint must be a array');
            }

            foreach ($compareConditions as $compareCondition) {
                $type = $compareCondition['target_type'] ?? null;
                $name = $compareCondition['target_name'] ?? null;
                $targetField = $compareCondition['target_field'] ?? null;
                $operator = $compareCondition['operator'] ?? null;
                $filed = $compareCondition['event_field'] ?? null;

                if (!$type || !$targetField || !$operator || !$filed || !$name) {
                    throw new InvalidCommandSyntaxException('compare conditions are invalid');
                }

                if (!in_array($operator, $this::OPERATOR)) {
                    throw new InvalidCommandSyntaxException('operator is invalid');
                }

                if (!in_array($type, $this::CONDITION_TYPES)) {
                    throw new InvalidCommandSyntaxException('condition type is invalid');
                }
            }

        }

        $this->validateIdentifier($checkpoint['next'][0]);
        $this->validateIdentifier($checkpoint['next'][1]);
    }

    protected function validateChannel(array $checkpoint): void
    {
        if (!isset($checkpoint['payload']['channel'])) {
            throw new InvalidCommandSyntaxException('Invalid channel');
        }

        $this->validateIdentifier($checkpoint['next'][0]);
        $this->validateIdentifier($checkpoint['next'][1]);
    }

    protected function validateIdentifier(string $id): void
    {
        $valid = false;
        foreach ($this->checkpoints as $checkpoint) {
            if ($checkpoint['id'] === $id) {
                $valid = true;
            }
        }

        if (!$valid) {
            throw new InvalidCommandSyntaxException("Invalid checkpoint id $id");
        }
    }
}
