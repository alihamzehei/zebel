<?php

declare(strict_types=1);

namespace App\Plan\Application\Command;

use App\Shared\Application\Command\CommandInterface;


class RemoveJourneyCommand implements CommandInterface
{
    public function __construct(public readonly string $journeyId)
    {
    }
}
