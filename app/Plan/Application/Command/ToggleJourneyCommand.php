<?php

declare(strict_types=1);

namespace App\Plan\Application\Command;

use App\Shared\Application\Command\CommandInterface;

class ToggleJourneyCommand implements CommandInterface
{
    public function __construct(public readonly string $id)
    {
    }
}
