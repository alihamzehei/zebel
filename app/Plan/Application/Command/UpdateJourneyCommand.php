<?php

declare(strict_types=1);

namespace App\Plan\Application\Command;

use App\Plan\Domain\Model\Checkpoint\IdleCheckpoint;
use App\Plan\Domain\Model\Checkpoint\StopCheckpoint;
use App\Shared\Application\Command\CommandInterface;
use App\Plan\Domain\Model\Checkpoint\ActionCheckpoint;
use App\Shared\Application\Error\InvalidCommandSyntaxException;
use App\Plan\Domain\Model\Checkpoint\ReachabilityConditionalCheckpoint;
use App\Plan\Domain\Model\Checkpoint\EventOccurrenceConditionalCheckpoint;

class UpdateJourneyCommand extends PlanJourneyCommand
{
    // private const CHECKPOINT_TYPES = [
    //     IdleCheckpoint::TYPE,
    //     ActionCheckpoint::TYPE,
    //     ReachabilityConditionalCheckpoint::TYPE,
    //     EventOccurrenceConditionalCheckpoint::TYPE,
    //     StopCheckpoint::TYPE,
    // ];

    public function __construct(public readonly string $id,
                                public readonly string $name,
                                public readonly string $trigger,
                                public readonly string $root,
                                public readonly array $metaData,
                                public readonly array $checkpoints)
    {
        $this->validateIdentifier($root);
        $this->validateCheckpoints();
    }

    // private function validateCheckpoints(): void
    // {
    //     foreach ($this->checkpoints as $checkpoint) {
    //         $type = $checkpoint['type'];
    //         $this->validateCheckpointName($type);

    //         switch ($type) {
    //             case IdleCheckpoint::TYPE:
    //                 $this->validateIdle($checkpoint);
    //                 break;
    //             case ActionCheckpoint::TYPE:
    //                 $this->validateAction($checkpoint);
    //                 break;
    //             case ReachabilityConditionalCheckpoint::TYPE:
    //                 $this->validateChannel($checkpoint);
    //                 break;
    //             case EventOccurrenceConditionalCheckpoint::TYPE:
    //                 $this->validateCondition($checkpoint);
    //                 break;
    //             default:
    //                 break;
    //         }
    //     }
    // }

    // private function validateCheckpointName(string $name): void
    // {
    //     if (!\in_array($name, self::CHECKPOINT_TYPES, true)) {
    //         throw new InvalidCommandSyntaxException("Invalid checkpoint type $name");
    //     }
    // }

    // private function validateIdle(array $checkpoint): void
    // {
    //     if (!isset($checkpoint['payload']['time'])) {
    //         throw new InvalidCommandSyntaxException('Idle checkpoint must contains time');
    //     }

    //     if (!isset($checkpoint['next'])) {
    //         throw new InvalidCommandSyntaxException('Idle checkpoint must contains next');
    //     }

    //     $this->validateIdentifier($checkpoint['next'][0]);
    // }

    // private function validateAction(array $checkpoint): void
    // {
    //     if (!isset($checkpoint['payload']['notification_id'])) {
    //         throw new InvalidCommandSyntaxException('Action checkpoint must contains notification_id');
    //     }

    //     $this->validateIdentifier($checkpoint['next'][0]);
    // }

    // private function validateCondition(array $checkpoint): void
    // {
    //     if (!isset($checkpoint['payload']['condition'])) {
    //         throw new InvalidCommandSyntaxException('Invalid condition');
    //     }

    //     $this->validateIdentifier($checkpoint['next'][0]);
    //     $this->validateIdentifier($checkpoint['next'][1]);
    // }

    // private function validateChannel(array $checkpoint): void
    // {
    //     if (!isset($checkpoint['payload']['channel'])) {
    //         throw new InvalidCommandSyntaxException('Invalid channel');
    //     }

    //     $this->validateIdentifier($checkpoint['next'][0]);
    //     $this->validateIdentifier($checkpoint['next'][1]);
    // }

    // private function validateIdentifier(string $id): void
    // {
    //     $valid = false;
    //     foreach ($this->checkpoints as $checkpoint) {
    //         if ($checkpoint['id'] === $id) {
    //             $valid = true;
    //         }
    //     }

    //     if (!$valid) {
    //         throw new InvalidCommandSyntaxException("Invalid checkpoint id $id");
    //     }
    // }
}
