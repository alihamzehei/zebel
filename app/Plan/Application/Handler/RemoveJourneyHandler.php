<?php

declare(strict_types=1);

namespace App\Plan\Application\Handler;

use App\Plan\Domain\ValueObject\JourneyId;
use App\Plan\Domain\Repository\JourneyRepository;
use App\Shared\Application\Error\ApplicationError;
use App\Shared\Application\Command\CommandInterface;
use App\Plan\Application\Command\RemoveJourneyCommand;
use App\Shared\Application\Command\CommandHandlerInterface;

class RemoveJourneyHandler implements CommandHandlerInterface
{
    public function __construct(private readonly JourneyRepository $repository)
    {
    }

    public function handle(CommandInterface|RemoveJourneyCommand $command): void
    {
        $journey = $this->repository->findById(
            new JourneyId($command->journeyId)
        );

        if ($journey === null) {
            throw new ApplicationError("no such journey with id $command->journeyId");
        }

        $journey->remove();

        $this->repository->deleteById($journey->id);
    }
}
