<?php

declare(strict_types=1);

namespace App\Plan\Application\Handler;

use App\Plan\Domain\ValueObject\JourneyId;
use App\Shared\Application\Command\CommandInterface;
use App\Plan\Application\Command\ToggleJourneyCommand;
use App\Plan\Domain\Repository\JourneyRepository;
use App\Shared\Application\Command\CommandHandlerInterface;
use App\Shared\Application\Error\ApplicationError;

class ToggleJourneyHandler implements CommandHandlerInterface
{
    public function __construct(private readonly JourneyRepository $repository)
    {
    }

    public function handle(CommandInterface|ToggleJourneyCommand $command)
    {
        $journey = $this->repository->findById(new JourneyId($command->id));

        if ($journey === null) {
            throw new ApplicationError("No such journey with id $command->id");
        }

        if ($journey->enable) {
            $journey->disable();
        } else {
            $journey->enable();
        }

        $this->repository->persist($journey);
    }
}
