<?php

declare(strict_types=1);

namespace App\Plan\Application\Handler;

use App\Plan\Domain\Model\Journey;
use App\Plan\Domain\Model\Trigger;
use App\Plan\Domain\ValueObject\Name;
use App\Plan\Application\Command\PlanJourneyCommand;
use App\Shared\Application\Command\CommandInterface;
use App\Plan\Domain\Factory\CheckpointAggregateFactory;
use App\Plan\Domain\Repository\JourneyRepository;
use App\Shared\Application\Command\CommandHandlerInterface;

class PlanJourneyHandler implements CommandHandlerInterface
{
    public function __construct(private readonly JourneyRepository $repository)
    {
    }

    public function handle(CommandInterface|PlanJourneyCommand $command): string
    {
        $factory = new CheckpointAggregateFactory($command->root, $command->checkpoints);

        $journey = Journey::plan(
            $this->repository->nextIdentity(),
            new Name($command->name),
            Trigger::fromString($command->trigger),
            $command->metaData,
            $factory->create()
        );

        $this->repository->persist($journey);

        return $journey->id->id;
    }
}
