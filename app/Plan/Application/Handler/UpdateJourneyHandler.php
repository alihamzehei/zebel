<?php

declare(strict_types=1);

namespace App\Plan\Application\Handler;

use App\Plan\Domain\Model\Journey;
use App\Plan\Domain\Model\Trigger;
use App\Plan\Domain\ValueObject\Name;
use App\Plan\Domain\ValueObject\JourneyId;
use App\Plan\Application\Command\UpdateJourneyCommand;
use App\Shared\Application\Command\CommandInterface;
use App\Plan\Domain\Factory\CheckpointAggregateFactory;
use App\Plan\Domain\Repository\JourneyRepository;
use App\Shared\Application\Command\CommandHandlerInterface;
use App\Shared\Application\Error\ApplicationError;

class UpdateJourneyHandler implements CommandHandlerInterface
{
    public function __construct(private readonly JourneyRepository $repository)
    {
    }

    public function handle(CommandInterface|UpdateJourneyCommand $command): string
    {
        $factory = new CheckpointAggregateFactory($command->root, $command->checkpoints);

        $journey = $this->repository->findById(new JourneyId($command->id));

        if ($journey === null) {
            throw new ApplicationError("no such journey with id $command->id");
        }

        $this->repository->deleteById(new JourneyId($command->id));

        $journey = Journey::plan(
            new JourneyId($command->id),
            new Name($command->name),
            Trigger::fromString($command->trigger),
            $command->metaData,
            $factory->create()
        );

        $this->repository->persist($journey);

        return $journey->id->id;
    }
}
