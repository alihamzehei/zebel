<?php

declare(strict_types=1);

namespace App\Plan\Application\Handler;

use App\Plan\Domain\ValueObject\JourneyId;
use App\Shared\Application\Command\CommandInterface;
use App\Plan\Application\Command\PublishJourneyCommand;
use App\Plan\Domain\Repository\JourneyRepository;
use App\Shared\Application\Command\CommandHandlerInterface;
use App\Shared\Application\Error\ApplicationError;

class PublishJourneyHandler implements CommandHandlerInterface
{
    public function __construct(private readonly JourneyRepository $repository)
    {
    }

    public function handle(CommandInterface|PublishJourneyCommand $command): void
    {
        $journey = $this->repository->findById(new JourneyId($command->journeyId));

        if ($journey === null) {
            throw new ApplicationError("No journey found with id $command->journeyId");
        }

        $journey->publish();

        $this->repository->persist($journey);
    }
}
