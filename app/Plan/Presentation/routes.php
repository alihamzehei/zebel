<?php

use Illuminate\Support\Facades\Route;
use App\Plan\Presentation\Controllers\GetJourneyController;
use App\Plan\Presentation\Controllers\GetJourneysController;
use App\Plan\Presentation\Controllers\GetTriggersController;
use App\Plan\Presentation\Controllers\PlanJourneyController;
use App\Plan\Presentation\Controllers\RemoveJourneyController;
use App\Plan\Presentation\Controllers\ToggleJourneyController;
use App\Plan\Presentation\Controllers\UpdateJourneyController;
use App\Plan\Presentation\Controllers\PublishJourneyController;

Route::get('journeys', GetJourneysController::class);
Route::post('journeys', PlanJourneyController::class);
Route::get('journeys/{id}', GetJourneyController::class);
Route::post('journeys/{id}', UpdateJourneyController::class);
Route::delete('journeys/{id}', RemoveJourneyController::class);
Route::post('journeys/{id}/publish', PublishJourneyController::class);
Route::post('journeys/{id}/toggle', ToggleJourneyController::class);
Route::get('triggers', GetTriggersController::class);
