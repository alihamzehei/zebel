<?php

declare(strict_types=1);

namespace App\Plan\Presentation\Controllers;

use App\Shared\Application\Query\QueryBus;
use App\Plan\Application\Query\TriggerQuery;
use App\Shared\Presentation\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;

class GetTriggersController extends Controller
{
    public function __construct(private readonly QueryBus $bus)
    {
    }

    public function __invoke(): JsonResponse
    {
        return response()->json($this->bus->ask(new TriggerQuery));
    }
}
