<?php

declare(strict_types=1);

namespace App\Plan\Presentation\Controllers;

use App\Plan\Application\Query\JourneysQuery;
use App\Shared\Application\Query\QueryBus;
use App\Shared\Presentation\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GetJourneysController extends Controller
{
    public const DEFAULT_PAGE_NUMBER = 1;

    public function __construct(private QueryBus $bus)
    {
    }

    public function __invoke(Request $request)
    {
        $request->validate([
            'name' => ['string'],
            'page' => ['integer']
        ]);

        return response()->json($this->bus->ask(new JourneysQuery($request['name'], $request['page'])));
    }
}
