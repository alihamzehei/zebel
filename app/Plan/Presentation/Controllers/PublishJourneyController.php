<?php

declare(strict_types=1);

namespace App\Plan\Presentation\Controllers;

use App\Shared\Application\Command\CommandBus;
use App\Shared\Presentation\Http\Controllers\Controller;
use App\Plan\Application\Command\PublishJourneyCommand;

class PublishJourneyController extends Controller
{
    public function __construct(private readonly CommandBus $bus)
    {
    }

    public function __invoke($id)
    {
        $this->bus->handle(new PublishJourneyCommand($id));
        return response()->noContent();
    }
}
