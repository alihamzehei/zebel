<?php

declare(strict_types=1);

namespace App\Plan\Presentation\Controllers;

use App\Shared\Application\Command\CommandBus;
use App\Plan\Application\Command\UpdateJourneyCommand;
use App\Shared\Presentation\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UpdateJourneyController extends Controller
{
    public function __construct(private CommandBus $bus)
    {
    }

    public function __invoke(Request $request, $id)
    {
        $payload = $request->validate([
            'name' => ['required'],
            'meta' => ['required', 'array'],
            'trigger' => ['required'],
            'root' => ['required'],
            'checkpoints' => ['required', 'array'],
        ]);

        $command = new UpdateJourneyCommand(
            $id,
            $payload['name'],
            $payload['trigger'],
            $payload['root'],
            $payload['meta'],
            $payload['checkpoints']
        );

        $this->bus->handle($command);

        return response()->noContent();
    }
}
