<?php

declare(strict_types=1);

namespace App\Plan\Presentation\Controllers;

use App\Shared\Application\Command\CommandBus;
use App\Plan\Application\Command\RemoveJourneyCommand;
use App\Shared\Presentation\Http\Controllers\Controller;

class RemoveJourneyController extends Controller
{
    public function __construct(private CommandBus $bus)
    {
    }

    public function __invoke($id)
    {
        $this->bus->handle(new RemoveJourneyCommand($id));
        return response()->noContent();
    }
}
