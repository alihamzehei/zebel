<?php

declare(strict_types=1);

namespace App\Plan\Presentation\Controllers;

use App\Shared\Application\Command\CommandBus;
use App\Plan\Application\Command\ToggleJourneyCommand;
use App\Shared\Presentation\Http\Controllers\Controller;

class ToggleJourneyController extends Controller
{
    public function __construct(private CommandBus $bus)
    {
    }

    public function __invoke($id)
    {
        $this->bus->handle(new ToggleJourneyCommand($id));
        return response()->noContent();
    }
}
