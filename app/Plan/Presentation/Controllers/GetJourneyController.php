<?php

declare(strict_types=1);

namespace App\Plan\Presentation\Controllers;

use App\Plan\Application\Query\JourneyQuery;
use App\Shared\Application\Query\QueryBus;
use App\Shared\Presentation\Http\Controllers\Controller;

class GetJourneyController extends Controller
{
    public function __construct(private QueryBus $bus)
    {
    }

    public function __invoke($id)
    {
        $journeys = $this->bus->ask(new JourneyQuery($id));
        return response()->json($journeys);
    }
}
