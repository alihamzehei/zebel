<?php

declare(strict_types=1);

namespace App\Plan\Domain\ValueObject;

class TriggerId
{
    public function __construct(public readonly string $id)
    {
    }
}
