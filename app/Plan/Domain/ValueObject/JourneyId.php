<?php

declare(strict_types=1);

namespace App\Plan\Domain\ValueObject;

class JourneyId
{
    public function __construct(public readonly string $id)
    {
    }
}
