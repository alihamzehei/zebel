<?php

declare(strict_types=1);

namespace App\Plan\Domain\ValueObject;

use App\Shared\Domain\ValueObject\ValueError;

class Timer
{
    private const MINIMUM_TIMER_IN_MINUTES = 1;
    private const ON_MINUTE_IN_SECONDS = 60;

    public function __construct(public readonly int $minutes)
    {
        if (self::MINIMUM_TIMER_IN_MINUTES > $minutes) {
            throw new ValueError(sprintf('Timers must take %d minutes at least', self::MINIMUM_TIMER_IN_MINUTES));
        }
    }

    public static function fromSeconds(int $seconds): self
    {
        return new self($seconds / self::ON_MINUTE_IN_SECONDS);
    }

    public static function fromMinutes(int $minutes): self
    {
        return new self($minutes);
    }

    public static function fromMinimumMinutes(): self
    {
        return self::fromMinutes(self::MINIMUM_TIMER_IN_MINUTES);
    }

    public function asMinutes(): int
    {
        return $this->minutes;
    }

    public function asSeconds(): int
    {
        return $this->minutes * self::ON_MINUTE_IN_SECONDS;
    }

    public function isEquals(self $timer): bool
    {
        return $this->minutes === $timer->minutes;
    }
}
