<?php

declare(strict_types=1);

namespace App\Plan\Domain\ValueObject;

use App\Shared\Domain\ValueObject\ValueError;

class Name
{
    public function __construct(public readonly string $value)
    {
        if (mb_strlen($value) < 5) {
            throw new ValueError('Name is too short');
        }
    }
}
