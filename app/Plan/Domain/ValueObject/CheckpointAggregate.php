<?php

declare(strict_types=1);

namespace App\Plan\Domain\ValueObject;

use App\Plan\Domain\Model\Checkpoint\Checkpoint;
use App\Plan\Domain\Model\Checkpoint\EventOccurrenceConditionalCheckpoint;
use App\Plan\Domain\Model\Checkpoint\IdleCheckpoint;
use App\Plan\Domain\Model\Checkpoint\ReachabilityConditionalCheckpoint;
use App\Plan\Domain\Model\Checkpoint\StopCheckpoint;
use App\Shared\Domain\ValueObject\ValueError;

class CheckpointAggregate
{
    private array $checkpoints;

    public function __construct(public readonly CheckpointId $root, Checkpoint ...$checkpoints)
    {
        $this->checkpoints = $checkpoints;
        $this->validateRoot();
        $this->validateCheckpoints();
    }

    public function findCheckpoint(CheckpointId $id): ?Checkpoint
    {
        foreach ($this->checkpoints as $checkpoint) {
            if ($checkpoint->id()->isEqualsTo($id)) {
                return $checkpoint;
            }
        }

        return null;
    }

    public function checkpoints(): array
    {
        return $this->checkpoints;
    }

    private function validateRoot(): void
    {
        $root = $this->findCheckpoint($this->root);

        if ($root === null) {
            throw new ValueError('The root checkpoint does not exist in checkpoints');
        }

        if ($root instanceof StopCheckpoint) {
            throw new ValueError('A root checkpoint cannot be instance of '.StopCheckpoint::class);
        }
    }

    private function validateCheckpoints(): void
    {
        foreach ($this->checkpoints as $checkpoint) {
            if ($checkpoint instanceof IdleCheckpoint && $this->findCheckpoint($checkpoint->id()->leftChildId) instanceof IdleCheckpoint) {
                throw new ValueError('Idle checkpoint can not have idle checkpoint as child');
            }
            if ($checkpoint->id()->leftChildId && $this->findCheckpoint($checkpoint->id()->leftChildId) === null) {
                throw new ValueError(sprintf('The checkpoint [%s] defined as child but does not exists in the aggregate', $checkpoint->id()->leftChildId->id));
            }
            if ($checkpoint->id()->rightChildId && $this->findCheckpoint($checkpoint->id()->rightChildId) === null) {
                throw new ValueError(sprintf('The checkpoint [%s] defined as child but does not exists in the aggregate', $checkpoint->id()->rightChildId->id));
            }
            if (($checkpoint instanceof ReachabilityConditionalCheckpoint) &&
             ($this->findCheckpoint($checkpoint->id()->leftChildId) instanceof ReachabilityConditionalCheckpoint
             || $this->findCheckpoint($checkpoint->id()->rightChildId) instanceof ReachabilityConditionalCheckpoint)) {
                throw new ValueError('reachability conditional checkpoint can not has reachability conditional checkpoint as child');
            }
            if (($checkpoint instanceof EventOccurrenceConditionalCheckpoint) &&
             ($this->findCheckpoint($checkpoint->id()->leftChildId) instanceof EventOccurrenceConditionalCheckpoint
             || $this->findCheckpoint($checkpoint->id()->rightChildId) instanceof EventOccurrenceConditionalCheckpoint)) {
                throw new ValueError('event occurrence conditional checkpoint can not has event occurrence conditional checkpoint as child');
            }
        }
    }

    public function asArray(): array
    {
        return $this->checkpoints;
    }
}
