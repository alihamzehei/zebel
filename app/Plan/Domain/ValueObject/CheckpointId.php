<?php

declare(strict_types=1);

namespace App\Plan\Domain\ValueObject;

use App\Shared\Domain\ValueObject\ValueError;

class CheckpointId
{
    public function __construct(public readonly string $id,
                                public readonly ?self $leftChildId = null,
                                public readonly ?self $rightChildId = null)
    {
        if ($leftChildId === null && $rightChildId instanceof self) {
            throw new ValueError('Checkpoints with one child, cannot have right child');
        }
    }

    public function isEqualsTo(self $id): bool
    {
        if ($this->id !== $id->id) {
            return false;
        }

        if ($id->leftChildId && !$id->leftChildId->isEqualsTo($this->leftChildId)) {
            return false;
        }

        return !($id->rightChildId && !$id->rightChildId->isEqualsTo($this->rightChildId));
    }
}
