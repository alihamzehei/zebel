<?php

declare(strict_types=1);

namespace App\Plan\Domain\Model;

use App\Shared\Domain\DomainError;


enum Trigger
{
    case CartCreated;
    case CartRemoved;
    case CustomerRegistered;
    case OrderStored;
    case OrderPaid;
    case OrderLineCanceled;
    case ProductSeen;
    case ProductAddedToWishList;
    case ProductRemovedFromWishList;
    public static function fromString(string $name): self
    {
        foreach (self::cases() as $enumerate) {
            if ($enumerate->name === $name) {
                return $enumerate;
            }
        }

        throw new DomainError("Invalid case $name for Trigger");
    }
}
