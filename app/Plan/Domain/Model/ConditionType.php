<?php

declare(strict_types=1);

namespace App\Plan\Domain\Model;

use App\Shared\Domain\DomainError;


enum ConditionType
{
    case Trigger;
    case Event;

    public static function fromString(string $name): self
    {
        foreach (self::cases() as $enumerate) {
            if ($enumerate->name === $name) {
                return $enumerate;
            }
        }

        throw new DomainError("Invalid case $name for Operator");
    }
}
