<?php

declare(strict_types=1);

namespace App\Plan\Domain\Model;

use App\Shared\Domain\AggregateRoot;
use App\Plan\Domain\ValueObject\Name;
use App\Shared\Domain\DomainError;
use App\Plan\Domain\Event\JourneyEnabled;
use App\Plan\Domain\Event\JourneyPlanned;
use App\Plan\Domain\Event\JourneyRemoved;
use App\Plan\Domain\Event\JourneyDisabled;
use App\Plan\Domain\ValueObject\JourneyId;
use App\Plan\Domain\Event\JourneyPublished;
use App\Plan\Domain\ValueObject\CheckpointAggregate;

class Journey extends AggregateRoot
{
    public function __construct(public readonly JourneyId $id,
                                public readonly Name $name,
                                public readonly Trigger $trigger,
                                public readonly array $metaData,
                                public readonly CheckpointAggregate $checkpoints,
                                public bool $enable = false)
    {
    }

    public static function plan(JourneyId $id,
                                Name $name,
                                Trigger $trigger,
                                array $metaData,
                                CheckpointAggregate $checkpoints,
                                bool $enable = true): self
    {
        $journey = new self($id, $name, $trigger, $metaData, $checkpoints, $enable);
        $journey->record(JourneyPlanned::fromDomain($journey));

        return $journey;
    }

    public function enable()
    {
        if ($this->enable === true) {
            throw new DomainError('journey was already enabled');
        }
        $this->enable = true;
        $this->record(new JourneyEnabled($this->id->id));
    }

    public function disable()
    {
        if ($this->enable === false) {
            throw new DomainError('journey was already disabled');
        }
        $this->enable = false;
        $this->record(new JourneyDisabled($this->id->id));
    }

    public function publish()
    {
        $this->record(new JourneyPublished($this->id->id));
    }

    public function remove()
    {
        $this->record(new JourneyRemoved($this->id->id));
    }
}
