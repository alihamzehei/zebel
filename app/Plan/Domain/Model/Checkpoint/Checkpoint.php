<?php

declare(strict_types=1);

namespace App\Plan\Domain\Model\Checkpoint;

use App\Plan\Domain\ValueObject\CheckpointId;
use App\Shared\Domain\DomainError;

abstract class Checkpoint
{
    public function __construct(protected CheckpointId $id, public array $metaData)
    {
        $this->validate();
    }

    public function id(): CheckpointId
    {
        return $this->id;
    }

    protected function validate(): void
    {
        if (!$this->isValid()) {
            throw new DomainError('Invalid node state');
        }
    }

    abstract public function type(): string;

    // abstract public function payload(): string;

    abstract protected function isValid(): bool;

    // public function toArray(): array
    // {
    //     return [
    //         'id' => $this->id->id,
    //         'type' => $this->type(),
    //         'payload' => $this->payload(),
    //         'left_child_id' => $this->id->leftChildId->id,
    //         'right_child_id' => $this->id->rightChildId->id,
    //     ];
    // }
}
