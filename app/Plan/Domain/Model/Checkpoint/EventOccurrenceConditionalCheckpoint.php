<?php

declare(strict_types=1);

namespace App\Plan\Domain\Model\Checkpoint;

use App\Plan\Domain\ValueObject\CheckpointId;

class EventOccurrenceConditionalCheckpoint extends Checkpoint
{
    public const TYPE = 'eventOccurrenceCondition';

    public function __construct(
        CheckpointId           $id,
        array                  $metaData,
        public readonly string $eventOccurrenceCondition,
        public readonly ?array $compareConditions = []
    )
    {
        parent::__construct($id, $metaData);
    }

    public function type(): string
    {
        return self::TYPE;
    }

    protected function isValid(): bool
    {
        if (!$this->id()->leftChildId instanceof CheckpointId) {
            return false;
        }

        if ($this->id()->leftChildId instanceof self) {
            return false;
        }

        return !(!$this->id()->rightChildId instanceof CheckpointId);
    }
}
