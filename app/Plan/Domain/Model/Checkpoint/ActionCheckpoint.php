<?php

declare(strict_types=1);

namespace App\Plan\Domain\Model\Checkpoint;

use App\Plan\Domain\ValueObject\CheckpointId;

class ActionCheckpoint extends Checkpoint
{
    public const TYPE = 'action';

    public function __construct(CheckpointId $id, array $metaData, public readonly string $notificationId)
    {
        parent::__construct($id, $metaData);
    }

    public function type(): string
    {
        return self::TYPE;
    }

    protected function isValid(): bool
    {
        if (!$this->id->leftChildId instanceof CheckpointId) {
            return false;
        }

        return !($this->id()->rightChildId !== null);
    }
}
