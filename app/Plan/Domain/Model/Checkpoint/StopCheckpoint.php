<?php

declare(strict_types=1);

namespace App\Plan\Domain\Model\Checkpoint;

use App\Plan\Domain\ValueObject\CheckpointId;

class StopCheckpoint extends Checkpoint
{
    public const TYPE = 'stop';

    public function __construct(CheckpointId $id, array $metaData)
    {
        parent::__construct($id, $metaData);
    }

    public function type(): string
    {
        return self::TYPE;
    }

    protected function isValid(): bool
    {
        if ($this->id->leftChildId instanceof CheckpointId) {
            return false;
        }

        return !($this->id->rightChildId instanceof CheckpointId);
    }
}
