<?php

declare(strict_types=1);

namespace App\Plan\Domain\Repository;

use App\Plan\Domain\Model\Journey;
use App\Plan\Domain\ValueObject\JourneyId;
use App\Shared\Domain\Repository;

interface JourneyRepository extends Repository
{
    public function nextIdentity(): JourneyId;

    public function findById(JourneyId $id): ?Journey;

    public function deleteById(JourneyId $id): void;

    public function persist(Journey $journey): void;
}
