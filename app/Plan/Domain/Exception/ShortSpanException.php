<?php

declare(strict_types=1);

namespace App\Plan\Domain\Exception;

use InvalidArgumentException;

class ShortSpanException extends InvalidArgumentException
{
    public static function fromHours(int $want, int $got): self
    {
        return new self(sprintf('Span range must be at least %d hourse, %d got', $want, $got));
    }
}
