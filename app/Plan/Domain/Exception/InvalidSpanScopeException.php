<?php

declare(strict_types=1);

namespace App\Plan\Domain\Exception;

use InvalidArgumentException;

class InvalidSpanScopeException extends InvalidArgumentException
{
}
