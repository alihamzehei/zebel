<?php

declare(strict_types=1);

namespace App\Plan\Domain\Event;

use App\Shared\Domain\DomainEvent;

class JourneyEnabled extends DomainEvent
{
    public const NAME = 'journey.enabled';

    public function __construct(public readonly string $id)
    {
    }
}
