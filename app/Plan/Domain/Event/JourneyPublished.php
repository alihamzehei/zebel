<?php

declare(strict_types=1);

namespace App\Plan\Domain\Event;

use App\Shared\Domain\DomainEvent;


class JourneyPublished extends DomainEvent
{
    public const NAME = 'journey.published';

    public function __construct(public readonly string $id)
    {
        $this->occurred();
    }
}
