<?php

declare(strict_types=1);

namespace App\Plan\Domain\Event;

use InvalidArgumentException;
use App\Plan\Domain\Model\Journey;
use App\Shared\Domain\DomainEvent;
use App\Plan\Domain\Model\Checkpoint\Checkpoint;
use App\Plan\Domain\Model\Checkpoint\IdleCheckpoint;
use App\Plan\Domain\Model\Checkpoint\StopCheckpoint;
use App\Plan\Domain\Model\Checkpoint\ActionCheckpoint;
use App\Plan\Domain\Model\Checkpoint\ReachabilityConditionalCheckpoint;
use App\Plan\Domain\Model\Checkpoint\EventOccurrenceConditionalCheckpoint;

class JourneyPlanned extends DomainEvent
{
    public const NAME = 'journey.planned';

    public function __construct(public readonly string $id,
                                public readonly string $name,
                                public readonly string $trigger,
                                public readonly string $root,
                                public readonly array  $checkpoints,
                                public readonly bool   $enable)
    {
        $this->occurred();
    }

    public static function fromDomain(Journey $journey): self
    {
        return new self(
            $journey->id->id,
            $journey->name->value,
            $journey->trigger->name,
            $journey->checkpoints->root->id,
            array_map(
                fn(Checkpoint $checkpoint): array => [
                    'id' => $checkpoint->id()->id,
                    'type' => $checkpoint->type(),
                    'payload' => self::serializeCheckpoint($checkpoint),
                    'left_child_id' => $checkpoint->id()->leftChildId?->id,
                    'right_child_id' => $checkpoint->id()->rightChildId?->id,
                    'is_checked' => false,
                ],
                array_values($journey->checkpoints->asArray())
            ),
            $journey->enable
        );
    }

    private static function serializeCheckpoint(Checkpoint $checkpoint): array
    {
        return match (get_class($checkpoint)) {
            IdleCheckpoint::class => ['time' => $checkpoint->timer->asMinutes()],
            ReachabilityConditionalCheckpoint::class => ['channel' => $checkpoint->userReachabilityCondition],
            EventOccurrenceConditionalCheckpoint::class => [
                'activity_name' => $checkpoint->eventOccurrenceCondition,
                'compare_condition' => $checkpoint->compareConditions
            ],
            ActionCheckpoint::class => ['notification_id' => $checkpoint->notificationId],
            StopCheckpoint::class => [],
            default => throw new InvalidArgumentException('Unknown checkpoint type ' . \get_class($checkpoint))
        };
    }
}
