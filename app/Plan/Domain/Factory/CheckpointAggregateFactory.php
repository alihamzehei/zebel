<?php

declare(strict_types=1);

namespace App\Plan\Domain\Factory;

use App\Plan\Domain\ValueObject\Timer;
use App\Plan\Domain\ValueObject\CheckpointId;
use App\Plan\Domain\Model\Checkpoint\IdleCheckpoint;
use App\Plan\Domain\Model\Checkpoint\StopCheckpoint;
use App\Plan\Domain\ValueObject\CheckpointAggregate;
use App\Plan\Domain\Model\Checkpoint\ActionCheckpoint;
use App\Shared\Application\Error\InvalidCommandSyntaxException;
use App\Plan\Domain\Model\Checkpoint\ReachabilityConditionalCheckpoint;
use App\Plan\Domain\Model\Checkpoint\EventOccurrenceConditionalCheckpoint;

class CheckpointAggregateFactory
{
    private array $checkpoints = [];

    public function __construct(
        private string $root,
        private array $checkpointsMap,
        private readonly bool $replaceIds = true
    ) {
    }

    public function create(): CheckpointAggregate
    {
        $this->traverse($this->root);

        return new CheckpointAggregate($this->checkpoints[$this->root]->id(), ...array_values($this->checkpoints));
    }

    private function traverse(?string $checkpointId): void
    {
        if ($checkpointId) {
            $checkpoint = $this->findCheckpoint($checkpointId);

            if (isset($checkpoint['next'][0])) {
                $this->traverse($checkpoint['next'][0]);
            }

            if (isset($checkpoint['next'][1])) {
                $this->traverse($checkpoint['next'][1]);
            }

            $this->operateOnCheckpoint($checkpointId, $checkpoint);
        }
    }

    private function findCheckpoint(string $id): array
    {
        foreach ($this->checkpointsMap as $checkpoint) {
            if ($checkpoint['id'] === $id) {
                return $checkpoint;
            }
        }

        return [];
    }

    private function operateOnCheckpoint(string $id, array $checkpoint): void
    {
        $checkpointId = new CheckpointId(
            $this->replaceIds ? uniqid() : $id,
            isset($checkpoint['next'][0]) ? $this->checkpoints[$checkpoint['next'][0]]->id() : null,
            isset($checkpoint['next'][1]) ? $this->checkpoints[$checkpoint['next'][1]]->id() : null,
        );

        $this->checkpoints[$id] = match ($checkpoint['type']) {
            'idle' => new IdleCheckpoint(
                $checkpointId,
                $checkpoint['meta'],
                Timer::fromMinutes($checkpoint['payload']['time'])
            ),
            'action' => new ActionCheckpoint(
                $checkpointId,
                $checkpoint['meta'],
                $checkpoint['payload']['notification_id']
            ),
            'reachabilityCondition' => new ReachabilityConditionalCheckpoint(
                $checkpointId,
                $checkpoint['meta'],
                $checkpoint['payload']['channel']
            ),
            'eventOccurrenceCondition' => value(function () use ($checkpointId, $checkpoint) {
                $conditions = [];
                $compareCondition = $checkpoint['payload']['compare_condition'] ?? false;

                if ($compareCondition) {
                    foreach ($compareCondition as $condition) {
                        $conditions[] = [
                            'target_type' => $condition['target_type'],
                            'target_name' => $condition['target_name'],
                            'target_field' => $condition['target_field'],
                            'operator' => $condition['operator'],
                            'event_field' => $condition['event_field'],
                        ];
                    }
                }

                return new EventOccurrenceConditionalCheckpoint(
                    $checkpointId,
                    $checkpoint['meta'],
                    $checkpoint['payload']['condition'],
                    $conditions
                );
            }),
            'stop' => new StopCheckpoint($checkpointId, $checkpoint['meta']),
            default => throw new InvalidCommandSyntaxException('Invalid checkpoint type '.$checkpoint['type'])
        };
    }
}
