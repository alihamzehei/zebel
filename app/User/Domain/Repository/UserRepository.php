<?php

declare(strict_types=1);

namespace App\User\Domain\Repository;

use App\User\Domain\Model\User;
use App\Shared\Domain\Repository;
use App\User\Domain\ValueObject\Email;

interface UserRepository extends Repository
{
    public function nextIdentity(): string;

    public function persist(User $user): void;

    public function findByEmail(Email $email): ?User;
}
