<?php

declare(strict_types=1);

namespace App\User\Domain\ValueObject;

class UserId
{
    public function __construct(public readonly string $id)
    {
    }
}
