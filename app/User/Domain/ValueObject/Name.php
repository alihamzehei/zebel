<?php

declare(strict_types=1);

namespace App\User\Domain\ValueObject;

use ValueError;

class Name
{
    public function __construct(public readonly string $firstname, public readonly string $lastname)
    {
        $this->validate($this->firstname);
        $this->validate($this->lastname);
    }

    private function validate(string $name): void
    {
        if (mb_strlen($name) < 2) {
            throw new ValueError("Invalid name $name");
        }
    }

    public function fullName(): string
    {
        return sprintf('%s %s', $this->firstname, $this->lastname);
    }
}
