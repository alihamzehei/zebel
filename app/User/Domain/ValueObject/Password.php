<?php

declare(strict_types=1);

namespace App\User\Domain\ValueObject;

use App\Shared\Domain\ValueObject\ValueError;

class Password
{
    public const MIN_LENGTH = 8;

    public function __construct(public readonly string $value)
    {
        if (mb_strlen($value) <= self::MIN_LENGTH) {
            throw new ValueError('Password cant be smaller than '.self::MIN_LENGTH.' characters');
        }
    }
}
