<?php

declare(strict_types=1);

namespace App\User\Domain\ValueObject;

use const FILTER_VALIDATE_EMAIL;
use App\Shared\Domain\ValueObject\ValueError;

class Email
{
    public function __construct(public readonly string $value)
    {
        if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
            throw new ValueError('Invalid email');
        }
    }

    public function isEquals(self $other): bool
    {
        return $other->value === $this->value;
    }
}
