<?php

declare(strict_types=1);

namespace App\User\Domain\Model;

use App\Shared\Domain\AggregateRoot;
use App\User\Domain\ValueObject\Email;
use App\User\Domain\ValueObject\Name;
use App\User\Domain\ValueObject\Password;
use App\User\Domain\ValueObject\UserId;

class User extends AggregateRoot
{
    public function __construct(
        public readonly UserId $id,
        public readonly Email $email,
        public readonly Name $name,
        public readonly Password $password
    ) {
    }

    public static function register(string $id, string $firstname, string $lastname, string $email, string $password): self
    {
        return new self(new UserId($id), new Email($email), new Name($firstname, $lastname), new Password($password));
    }
}
