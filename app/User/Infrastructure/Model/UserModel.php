<?php

declare(strict_types=1);

namespace App\User\Infrastructure\Model;

use Illuminate\Database\Eloquent\Model;
use App\User\Domain\Model\User;
use App\User\Domain\ValueObject\Email;
use App\User\Domain\ValueObject\Name;
use App\User\Domain\ValueObject\Password;
use App\User\Domain\ValueObject\UserId;
use Laravel\Sanctum\HasApiTokens;

class UserModel extends Model
{
    use HasApiTokens;

    protected $fillable = [
        'uuid',
        'first_name',
        'last_name',
        'email',
        'password'
    ];
    protected $table = 'user_users';
    protected $keyType = 'string';
    public $timestamps = false;

    public function toDomain(): User
    {
        return new User(
            new UserId($this->id),
            new Email($this->email),
            new Name($this->first_name, $this->last_name),
            new Password($this->password)
        );
    }
}
