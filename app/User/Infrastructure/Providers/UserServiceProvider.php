<?php

namespace App\User\Infrastructure\Providers;

use App\User\Infrastructure\Auth\SanctumTokenManager;
use App\User\Infrastructure\Repository\EloquentUserRepository;
use App\User\Application\Auth\TokenManager;
use App\User\Domain\Repository\UserRepository;
use Illuminate\Support\ServiceProvider;

class UserServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->bind(UserRepository::class, EloquentUserRepository::class);
        $this->app->bind(TokenManager::class, SanctumTokenManager::class);
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        $this->loadMigrationsFrom(dirname(__DIR__ ) . '/Migrations');
    }
}
