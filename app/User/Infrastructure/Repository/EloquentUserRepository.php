<?php

declare(strict_types=1);

namespace App\User\Infrastructure\Repository;

use Ramsey\Uuid\Uuid;
use App\User\Domain\Model\User;
use App\User\Infrastructure\Model\UserModel;
use App\User\Domain\ValueObject\Email;
use App\User\Domain\Repository\UserRepository;

class EloquentUserRepository implements UserRepository
{
    private array $seen = [];

    public function persist(User $user): void
    {
        UserModel::query()->create([
            'uuid' => $user->id->id,
            'first_name' => $user->name->firstname,
            'last_name' => $user->name->lastname,
            'email' => $user->email->value,
            'password' => $user->password->value,
        ]);

        $this->seen[] = $user;
    }

    public function nextIdentity(): string
    {
        return uniqid();
    }

    public function findByEmail(Email $email): ?User
    {
        return UserModel::query()
            ->where('email', $email->value)
            ->first()
            ?->toDomain();
    }

    public function seen(): array
    {
        return $this->seen;
    }
}
