<?php

declare(strict_types=1);

namespace App\User\Infrastructure\Auth;

use App\User\Domain\Model\User;
use App\User\Infrastructure\Model\UserModel;
use App\User\Application\Auth\TokenManager;

class SanctumTokenManager implements TokenManager
{
    public function generate(User $user): string
    {
        return UserModel::query()
            ->where('email', $user->email->value)
            ->first()
            ->createToken('auth')
            ->plainTextToken;
    }
}
