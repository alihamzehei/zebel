<?php

declare(strict_types=1);

namespace App\User\Presentation\Controllers;

use App\User\Presentation\Requests\RegisterUserRequest;
use App\Shared\Application\Command\CommandBus;
use App\User\Application\Command\RegisterUserCommand;
use App\Shared\Presentation\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;

class RegisterUsersController extends Controller
{
    public function __construct(private readonly CommandBus $bus)
    {
    }

    public function __invoke(RegisterUserRequest $request): JsonResponse
    {
        $payload = $request->validated();

        $command = new RegisterUserCommand(
            $payload['first_name'],
            $payload['last_name'],
            $payload['email'],
            $payload['password'],
        );

        $token = $this->bus->handle($command);

        return response()->json(compact('token'));
    }
}
