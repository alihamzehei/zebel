<?php

declare(strict_types=1);

namespace App\User\Presentation\Controllers;

use App\Shared\Application\Command\CommandBus;
use App\Shared\Presentation\Http\Controllers\Controller;
use App\User\Application\Command\LoginUserCommand;
use App\User\Presentation\Requests\LoginUserRequest;
use Illuminate\Http\JsonResponse;

class LoginUserController extends Controller
{
    public function __construct(private readonly CommandBus $bus)
    {
    }

    public function __invoke(LoginUserRequest $request): JsonResponse
    {
        $payload = $request->validated();

        $token = $this->bus->handle(
            new LoginUserCommand($payload['email'], $payload['password'])
        );

        return response()->json(compact('token'));
    }
}
