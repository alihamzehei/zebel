<?php

use Illuminate\Support\Facades\Route;
use App\User\Presentation\Controllers\LoginUserController;
use App\User\Presentation\Controllers\RegisterUsersController;

Route::post('/auth/register', RegisterUsersController::class);
Route::post('/auth/login', LoginUserController::class);
