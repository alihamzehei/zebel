<?php

declare(strict_types=1);

namespace App\User\Application\Exception;

use App\Shared\Application\Error\ApplicationError;

class UserNotFoundException extends ApplicationError
{
}
