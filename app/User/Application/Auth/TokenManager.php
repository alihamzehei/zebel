<?php

declare(strict_types=1);

namespace App\User\Application\Auth;

use App\User\Domain\Model\User;

interface TokenManager
{
    public function generate(User $user): string;
}
