<?php

declare(strict_types=1);

namespace App\User\Application\Handler;

use App\User\Domain\Model\User;
use App\Shared\Application\Command\CommandInterface;
use App\User\Application\Command\RegisterUserCommand;
use App\Shared\Application\Command\CommandHandlerInterface;
use App\User\Application\Auth\TokenManager;
use App\User\Domain\Repository\UserRepository;
use Illuminate\Support\Facades\Hash;

class RegisterUserHandler implements CommandHandlerInterface
{
    public function __construct(
        private readonly UserRepository $users,
        private readonly TokenManager $tokens,
    )
    {
    }

    public function handle(CommandInterface|RegisterUserCommand $command)
    {
        $user = User::register(
            $this->users->nextIdentity(),
            $command->firstName,
            $command->lastName,
            $command->email,
            Hash::make($command->password),
        );

        $this->users->persist($user);

        return $this->tokens->generate($user);
    }
}
