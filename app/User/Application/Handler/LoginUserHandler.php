<?php

declare(strict_types=1);

namespace App\User\Application\Handler;

use Illuminate\Support\Facades\Hash;
use App\User\Domain\ValueObject\Email;
use App\User\Application\Auth\TokenManager;
use App\User\Domain\Repository\UserRepository;
use App\User\Application\Command\LoginUserCommand;
use App\Shared\Application\Command\CommandInterface;
use App\User\Application\Exception\UserNotFoundException;
use App\User\Application\Exception\WrongPasswordException;
use App\Shared\Application\Command\CommandHandlerInterface;

class LoginUserHandler implements CommandHandlerInterface
{
    public function __construct(private TokenManager $tokens,
                                private UserRepository $users)
    {
    }

    public function handle(CommandInterface|LoginUserCommand $command): string
    {
        $user = $this->users->findByEmail(new Email($command->email));

        if ($user === null) {
            throw new UserNotFoundException("No such user with email $command->email");
        }

        if (! Hash::check($command->password, $user->password->value)) {
            throw new WrongPasswordException();
        }

        return $this->tokens->generate($user);
    }
}
