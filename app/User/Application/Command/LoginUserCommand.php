<?php

declare(strict_types=1);

namespace App\User\Application\Command;

use App\Shared\Application\Command\CommandInterface;

class LoginUserCommand implements CommandInterface
{
    public function __construct(public readonly string $email, public readonly string $password)
    {
    }
}
