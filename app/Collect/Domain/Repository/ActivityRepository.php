<?php

declare(strict_types=1);

namespace App\Collect\Domain\Repository;

use App\Collect\Domain\Model\Activity;
use App\Shared\Domain\Repository;

interface ActivityRepository extends Repository
{
    public function persist(Activity $activity): void;
}
