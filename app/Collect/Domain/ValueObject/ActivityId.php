<?php

declare(strict_types=1);

namespace App\Collect\Domain\ValueObject;

class ActivityId
{
    public function __construct(public readonly string $value)
    {
    }
}
