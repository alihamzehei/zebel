<?php

declare(strict_types=1);

namespace App\Collect\Domain\Event;

use App\Shared\Domain\DomainEvent;
use App\Collect\Domain\Model\Activity;

class ActivityCollected extends DomainEvent
{
    public function __construct(public readonly string $id,
                                public readonly string $title,
                                public readonly int $causer,
                                public readonly array $payload)
    {
        $this->occurred();
    }

    public static function fromDomain(Activity $activity): self
    {
        return new self($activity->id->value, $activity->title, $activity->causer, $activity->payload);
    }
}
