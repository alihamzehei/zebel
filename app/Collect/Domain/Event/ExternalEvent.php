<?php

declare(strict_types=1);

namespace App\Collect\Domain\Event;

use DateTimeImmutable;
use App\Shared\Domain\DomainEvent;

class ExternalEvent extends DomainEvent
{
    // Do not confuse DomainEvent::occurredOn and self::occurredAt,
    // The DomainEvent::occurredOn is a general property for every single domain event,
    // while the occurredAt is a specific property for the ExternalEvent class
    public function __construct(public readonly string $id,
                                public readonly string $title,
                                public readonly array $customer,
                                public readonly DateTimeImmutable $occurredAt,
                                public readonly array $payload)
    {
        $this->occurred();
    }
}
