<?php

declare(strict_types=1);

namespace App\Collect\Domain\Model;

use DateTimeImmutable;
use App\Shared\Domain\AggregateRoot;
use App\Collect\Domain\ValueObject\ActivityId;
use App\Collect\Domain\Event\ActivityCollected;

class Activity extends AggregateRoot
{
    public function __construct(public readonly ActivityId $id,
                                public readonly string $title,
                                public readonly int $causer,
                                public readonly DateTimeImmutable $occurredAt,
                                public readonly array $payload)
    {
        $this->record(ActivityCollected::fromDomain($this));
    }
}
