<?php

declare(strict_types=1);

namespace App\Collect\Presentation\Message;

use App\Collect\Application\Command\CaptureActivityCommand;
use App\Collect\Domain\Event\ExternalEvent;
use App\Shared\Application\CommandBus;
use App\Shared\Domain\Event\DomainEvent;
use App\Shared\Presentation\Message\Subscriber;
use App\Target\Application\Command\RegisterTargetCommand;

class ExternalEventSubscriber extends Subscriber
{
    public function __construct(private CommandBus $bus)
    {
    }

    public function __invoke(DomainEvent|ExternalEvent $event): void
    {
        $this->bus->handle(
            new RegisterTargetCommand($event->customer['id'], $event->customer['name'], $event->customer['email'], $event->customer['mobile'])
        );

        $this->bus->handle(
            new CaptureActivityCommand($event->id, $event->customer['id'], $event->title, $event->payload, $event->occurredAt)
        );
    }
}
