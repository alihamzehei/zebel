<?php

namespace App\Collect\Presentation\Console;

use App\Plan\Domain\Model\Trigger;
use App\Target\Application\Command\RegisterTargetCommand;
use DateTimeImmutable;
use Illuminate\Support\Arr;
use Illuminate\Console\Command;
use PhpAmqpLib\Message\AMQPMessage;
use App\Shared\Application\Command\CommandBus;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use App\Collect\Application\Command\CaptureActivityCommand;

class CollectActivityConsoleCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:collect';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Subscribe on amqp to consume external events';

    /**
     * Execute the console command.
     */
    public function handle(AMQPStreamConnection $connection): void
    {
        $channel = $connection->channel();

        foreach (Trigger::cases() as $event) {

            $channel->queue_declare(
                queue: $event = $event->name,
                durable: true,
            );

            $channel->basic_qos(0, 1, false);

            $channel->queue_bind($event, 'amq.direct', $event);

            $channel->basic_consume(
                queue: $event,
                callback: function (AMQPMessage $message) {
                    $this->handleEvent($event = json_decode($message->body, true));

                    $this->info(
                        sprintf(
                            "%s #%s has been triggered",
                            $this->argument('event'),
                            $event['id']
                        )
                    );

                    $message->ack();
                }
            );

        }
        while ($channel->is_consuming()) {
            $channel->wait();
        }

        $channel->close();
    }

    private function handleEvent(array $event): void
    {
        tap(app(CommandBus::class), function (CommandBus $bus) use ($event) {
            $bus->handle(
                new RegisterTargetCommand(
                    $customerID = $event['data']['customer']['id'],
                    $event['data']['customer']['name'],
                    $event['data']['customer']['email'],
                    $event['data']['customer']['mobile']
                )
            );

            $bus->handle(
                new CaptureActivityCommand(
                    $event['id'],
                    $customerID,
                    $this->argument('event'),
                    Arr::except($event['data'], 'customer'),
                    new DateTimeImmutable($event['publish_at']),
                )
            );
        });
    }
}
