<?php

declare(strict_types=1);

namespace App\Collect\Presentation\Module;

use DateTimeImmutable;
use App\Shared\Application\Query\QueryBus;
use App\Collect\Application\Query\ActivitiesQuery;
use App\Collect\Application\Query\ActivityByIdQuery;


class ActivityModule
{
    public function __construct(private readonly QueryBus $bus)
    {
    }

    public function getActivities(int $targetId,
                                  string $activityName,
                                  ?DateTimeImmutable $fromDate = null,
                                  ?DateTimeImmutable $toDate = null): array
    {
        return $this->bus->ask(new ActivitiesQuery($targetId, $activityName, $fromDate, $toDate));
    }

    public function getActivityById(string $activityId): array
    {
        return $this->bus->ask(new ActivityByIdQuery($activityId));
    }
}
