<?php

declare(strict_types=1);

namespace App\Collect\Application\Query;

use App\Shared\Application\Query\QueryInterface;

class ActivityByIdsQuery implements QueryInterface
{
    public function __construct(public readonly array $activityIds)
    {
    }
}
