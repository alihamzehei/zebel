<?php

declare(strict_types=1);

namespace App\Collect\Application\Query;

use App\Shared\Application\Query\QueryInterface;

class ActivityByIdQuery implements QueryInterface
{
    public function __construct(public readonly string $activityId)
    {
    }
}
