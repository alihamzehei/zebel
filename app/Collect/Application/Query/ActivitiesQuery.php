<?php

declare(strict_types=1);

namespace App\Collect\Application\Query;

use DateTimeImmutable;
use App\Shared\Application\Query\QueryInterface;


class ActivitiesQuery implements QueryInterface
{
    public function __construct(public readonly int $targetId,
                                public readonly string $activityName,
                                public readonly ?DateTimeImmutable $fromDate = null,
                                public readonly ?DateTimeImmutable $toDate = null)
    {
    }
}
