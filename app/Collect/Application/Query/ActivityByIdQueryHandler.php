<?php

declare(strict_types=1);

namespace App\Collect\Application\Query;

use JsonSerializable;
use App\Shared\Application\Query\QueryInterface;
use App\Collect\Infrastructure\Model\CollectActivityModel;
use App\Collect\Application\Query\ActivityByIdQuery;
use App\Shared\Application\Query\QueryHandlerInterface;
use Illuminate\Support\Arr;

class ActivityByIdQueryHandler implements QueryHandlerInterface
{
    public function handle(QueryInterface|ActivityByIdQuery $query): JsonSerializable|array
    {
        $result = CollectActivityModel::firstWhere('uuid', $query->activityId)?->toArray();

        if ($result) {
            $result['id'] = $result['uuid'];
        }

        return $result;
    }
}
