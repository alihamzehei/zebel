<?php

declare(strict_types=1);

namespace App\Collect\Application\Query;

use JsonSerializable;
use App\Shared\Application\Query\QueryInterface;
use App\Collect\Infrastructure\Model\CollectActivityModel;
use App\Shared\Application\Query\QueryHandlerInterface;

class ActivitiesQueryHandler implements QueryHandlerInterface
{
    public function handle(QueryInterface|ActivitiesQuery $query): JsonSerializable|array
    {
        $builder = CollectActivityModel::where(['causer' => $query->targetId, 'title' => $query->activityName]);

        if ($query->fromDate) {
            $builder->where('occurred_at', '>=', $query->fromDate);
        }

        if ($query->toDate) {
            $builder->where('occurred_at', '<=', $query->toDate);
        }

        return $builder->get()->toArray();
    }
}
