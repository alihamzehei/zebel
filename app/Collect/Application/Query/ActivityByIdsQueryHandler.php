<?php

declare(strict_types=1);

namespace App\Collect\Application\Query;

use JsonSerializable;
use App\Shared\Application\Query\QueryInterface;
use App\Collect\Infrastructure\Model\CollectActivityModel;
use App\Shared\Application\Query\QueryHandlerInterface;

class ActivityByIdsQueryHandler implements QueryHandlerInterface
{
    public function handle(QueryInterface|ActivityByIdsQuery $query): JsonSerializable|array
    {
        $result = CollectActivityModel::whereIn('uuid', $query->activityIds)->get()?->toArray();

        return $result ?? [];
    }
}
