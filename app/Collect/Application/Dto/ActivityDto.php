<?php

declare(strict_types=1);

namespace App\Collect\Application\Dto;

use App\Shared\Application\Dto;

class ActivityDto extends Dto
{
    public function __construct(public readonly array $activity)
    {
    }

    public function asArray(): array
    {
        return [
            'id' => $this->activity['id'],
            'title' => $this->activity['title'],
            'causer' => $this->activity['causer'],
            'payload' => json_decode($this->activity['payload'], true),
            'occurredAt' => $this->activity['occurred_at'],
        ];
    }
}
