<?php

declare(strict_types=1);

namespace App\Collect\Application\Dto;

use App\Shared\Application\Dto;

class ActivitiesDto extends Dto
{
    public function __construct(public readonly array $activities)
    {
    }

    public function asArray(): array
    {
        return array_map(fn (array $activity): array => [
            'id' => $activity['id'],
            'title' => $activity['title'],
            'causer' => $activity['causer'],
            'payload' => json_decode($activity['payload'], true),
            'occurredAt' => $activity['occurred_at'],
        ], $this->activities);
    }
}
