<?php

declare(strict_types=1);

namespace App\Collect\Application\UnitOfWork;

use App\Collect\Domain\Repository\ActivityRepository;
use App\Shared\Application\UnitOfWork;

interface ActivityUnitOfWork extends UnitOfWork
{
    public function repository(): ActivityRepository;
}
