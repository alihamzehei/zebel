<?php

declare(strict_types=1);

namespace App\Collect\Application\Command;

use DateTimeImmutable;
use App\Shared\Application\Command\CommandInterface;

class CaptureActivityCommand implements CommandInterface
{
    public function __construct(public readonly string $id,
                                public readonly int $causer,
                                public readonly string $name,
                                public readonly array $payload,
                                public readonly DateTimeImmutable $occurredAt)
    {
    }
}
