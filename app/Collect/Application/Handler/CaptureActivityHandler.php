<?php

declare(strict_types=1);

namespace App\Collect\Application\Handler;

use App\Collect\Domain\Model\Activity;
use App\Collect\Domain\ValueObject\ActivityId;
use App\Shared\Application\Command\CommandInterface;
use App\Collect\Application\Command\CaptureActivityCommand;
use App\Collect\Domain\Repository\ActivityRepository;
use App\Shared\Application\Command\CommandHandlerInterface;

class CaptureActivityHandler implements CommandHandlerInterface
{
    public function __construct(private readonly ActivityRepository $repository)
    {
    }

    public function handle(CommandInterface|CaptureActivityCommand $command): void
    {
        $this->repository->persist(
            new Activity(new ActivityId($command->id), $command->name, $command->causer, $command->occurredAt, $command->payload)
        );
    }
}
