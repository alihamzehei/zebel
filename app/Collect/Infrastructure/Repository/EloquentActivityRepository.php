<?php

declare(strict_types=1);

namespace App\Collect\Infrastructure\Repository;

use App\Collect\Domain\Model\Activity;
use App\Collect\Infrastructure\Model\CollectActivityModel;
use App\Collect\Domain\Repository\ActivityRepository;

class EloquentActivityRepository implements ActivityRepository
{
    private array $seen = [];

    public function persist(Activity $activity): void
    {
        CollectActivityModel::query()->create([
            'uuid' => $activity->id->value,
            'title' => $activity->title,
            'causer' => $activity->causer,
            'occurred_at' => $activity->occurredAt->format('Y-m-d H:i:s'),
            'payload' => $activity->payload,
        ]);

        $this->seen[] = $activity;
    }

    public function seen(): array
    {
        return $this->seen;
    }

    public function nextIdentity()
    {
    }
}
