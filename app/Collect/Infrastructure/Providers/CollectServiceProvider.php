<?php

namespace App\Collect\Infrastructure\Providers;

use App\Collect\Infrastructure\Repository\EloquentActivityRepository;
use App\Collect\Domain\Repository\ActivityRepository;
use Illuminate\Support\ServiceProvider;


class CollectServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->bind(ActivityRepository::class, EloquentActivityRepository::class);
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        $this->loadMigrationsFrom(dirname(__DIR__ ) . '/Migrations');
    }
}
