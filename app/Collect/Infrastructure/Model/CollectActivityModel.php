<?php

declare(strict_types=1);

namespace App\Collect\Infrastructure\Model;

use Illuminate\Database\Eloquent\Model;

class CollectActivityModel extends Model
{
    protected $table = 'collect_activities';

    protected $fillable = [
        'uuid',
        'title',
        'causer',
        'payload',
        'occurred_at',
    ];

    protected $casts = [
        'payload' => 'array',
        'occurred_at' => 'immutable_datetime',
    ];
}
