<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('analytic_schedules', function (Blueprint $table) {
            $table->id();
            $table->uuid('schedule_id')->unique();
            $table->uuid('journey_id');
            $table->integer('target_id');
            $table->boolean('active')->default(true);
            $table->string('expectant')->nullable();
            $table->dateTime('occurred_at');
        });

        Schema::create('analytic_checkpoints', function (Blueprint $table) {
            $table->id();
            $table->uuid('schedule_id');
            $table->uuid('journey_id');
            $table->uuid('checkpoint_id');
            $table->string('type');
            $table->dateTime('occurred_at');
        });

        Schema::create('analytic_activities', function (Blueprint $table) {
            $table->id();
            $table->uuid('event_id');
            $table->integer('schedule_id');
            $table->string('title');
            $table->dateTime('occurred_at');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('analytic_activities');
        Schema::dropIfExists('analytic_checkpoints');
        Schema::dropIfExists('analytic_schedules');
    }
};
