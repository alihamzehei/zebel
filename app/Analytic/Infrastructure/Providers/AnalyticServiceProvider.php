<?php

namespace App\Analytic\Infrastructure\Providers;

use Illuminate\Support\ServiceProvider;

class AnalyticServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        $this->loadMigrationsFrom(dirname(__DIR__ ) . '/Migrations');
    }
}
