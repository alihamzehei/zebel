<?php

declare(strict_types=1);

namespace App\Analytic\Infrastructure\Model;

use Illuminate\Database\Eloquent\Model;

class AnalyticActivityModel extends Model
{
    protected $fillable = [
        'event_id',
        'schedule_id',
        'title',
        'occurred_at',
    ];

    public $incrementing = false;

    protected $table = 'analytic_activities';

    protected $primaryKey = 'event_id';

    public $timestamps = false;
}
