<?php

declare(strict_types=1);

namespace App\Analytic\Infrastructure\Model;

use Illuminate\Database\Eloquent\Model;

class AnalyticCheckpointModel extends Model
{
    protected $fillable = [
        'id',
        'schedule_id',
        'journey_id',
        'checkpoint_id',
        'type',
        'occurred_at',
    ];

    protected $table = 'analytic_checkpoints';

    public $timestamps = false;
}
