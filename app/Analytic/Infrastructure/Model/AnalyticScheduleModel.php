<?php

declare(strict_types=1);

namespace App\Analytic\Infrastructure\Model;

use Illuminate\Database\Eloquent\Model;

class AnalyticScheduleModel extends Model
{
    protected $fillable = [
        'id',
        'schedule_id',
        'journey_id',
        'target_id',
        'active',
        'expectant',
        'occurred_at',
    ];

    protected $table = 'analytic_schedules';

    public $timestamps = false;
}
