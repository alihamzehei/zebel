<?php

declare(strict_types=1);

use App\Analytic\Presentation\Controllers\CheckpointUserCountController;
use App\Analytic\Presentation\Controllers\JourneyConversionController;
use App\Analytic\Presentation\Controllers\JourneyUserCountController;
use Illuminate\Support\Facades\Route;

Route::prefix('analytics/journey/{id}')->group(function () {
    Route::get('conversion', JourneyConversionController::class);
    Route::get('user', JourneyUserCountController::class);
    Route::get('checkpoints/user', CheckpointUserCountController::class);
});
