<?php

declare(strict_types=1);

namespace App\Analytic\Presentation\Controllers;

use App\Analytic\Application\Query\JourneyUserCountQuery;
use App\Shared\Application\Query\QueryBus;
use App\Shared\Presentation\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class JourneyUserCountController extends Controller
{
    public function __construct(private readonly QueryBus $bus)
    {
    }

    public function __invoke($id, Request $request): JsonResponse
    {
        return response()->json($this->bus->ask(new JourneyUserCountQuery($id, $request->has('now'))));
    }
}
