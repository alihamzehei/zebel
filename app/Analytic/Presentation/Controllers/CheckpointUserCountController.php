<?php

declare(strict_types=1);

namespace App\Analytic\Presentation\Controllers;

use App\Analytic\Application\Query\CheckpointUserCountQuery;
use App\Shared\Application\Query\QueryBus;
use App\Shared\Presentation\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;

class CheckpointUserCountController extends Controller
{
    public function __construct(private readonly QueryBus $bus)
    {
    }

    public function __invoke($id): JsonResponse
    {
        return response()->json($this->bus->ask(new CheckpointUserCountQuery($id)));
    }
}
