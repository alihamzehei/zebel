<?php

declare(strict_types=1);

namespace App\Analytic\Presentation\Controllers;

use App\Analytic\Application\Query\JourneyConversionQuery;
use App\Analytic\Presentation\Requests\JourneyConversionRequest;
use App\Shared\Application\Query\QueryBus;
use App\Shared\Presentation\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class JourneyConversionController extends Controller
{
    public function __construct(private readonly QueryBus $bus)
    {
    }

    public function __invoke($id, JourneyConversionRequest $request): JsonResponse
    {
        $payload = $request->validated();

        $query = new JourneyConversionQuery(
            $id,
            $payload['event'],
            $request->filled('deadline') ? (int) $request['deadline'] : null,
            (int) $payload['fromDate'],
            (int) $payload['toDate'],
        );

        return response()->json($this->bus->ask($query));
    }
}
