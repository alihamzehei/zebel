<?php

declare(strict_types=1);

namespace App\Analytic\Application\Query;

use App\Analytic\Infrastructure\Model\AnalyticCheckpointModel;
use App\Shared\Application\Query\QueryHandlerInterface;
use App\Shared\Application\Query\QueryInterface;
use Illuminate\Support\Facades\DB;
use JsonSerializable;

class CheckpointUserCountQueryHandler implements QueryHandlerInterface
{
    public function handle(QueryInterface|CheckpointUserCountQuery $query): array|JsonSerializable
    {
        return $this->checkpointAggregateOf($query->id);
    }

    private function checkpointAggregateOf(string $journeyId): array
    {
        return AnalyticCheckpointModel::query()
            ->select('checkpoint_id as checkpoint', DB::raw('count(id) as count'))
            ->where('journey_id', $journeyId)
            ->groupBy('checkpoint_id')
            ->get()
            ->toArray();
    }
}
