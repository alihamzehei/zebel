<?php

declare(strict_types=1);

namespace App\Analytic\Application\Query;

use App\Analytic\Infrastructure\Model\AnalyticScheduleModel;
use App\Shared\Application\Query\QueryHandlerInterface;
use App\Shared\Application\Query\QueryInterface;
use Illuminate\Support\Facades\DB;
use JsonSerializable;

class JourneyUserCountQueryHandler implements QueryHandlerInterface
{
    public function handle(QueryInterface|JourneyUserCountQuery $query): array|JsonSerializable
    {
        $sql = AnalyticScheduleModel::query()
            ->select(DB::raw('count(*) as count'))
            ->where('journey_id', $query->id);

        if ($query->onlyActives) {
            $sql->where('active', 1);
        }

        $journeys = $sql->get()->first()->toArray();

        return ['count' => $journeys['count']];
    }
}
