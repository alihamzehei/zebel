<?php

declare(strict_types=1);

namespace App\Analytic\Application\Query;

use App\Shared\Application\Query\QueryInterface;

class CheckpointUserCountQuery implements QueryInterface
{
    public function __construct(public readonly string $id)
    {
    }
}
