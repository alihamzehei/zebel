<?php

declare(strict_types=1);

namespace App\Analytic\Application\Query;

use App\Shared\Application\Query\QueryInterface;

class JourneyConversionQuery implements QueryInterface
{
    public function __construct(public readonly string $id,
                                public readonly string $event,
                                public readonly ?int $deadline,
                                public readonly int $from,
                                public readonly int $to)
    {
    }
}
