<?php

declare(strict_types=1);

namespace App\Analytic\Application\Query;

use App\Shared\Application\Query\QueryHandlerInterface;
use App\Shared\Application\Query\QueryInterface;
use Carbon\Carbon;
use DateTimeImmutable;
use Illuminate\Support\Facades\DB;
use JsonSerializable;

class JourneyConversionQueryHandler implements QueryHandlerInterface
{
    public function handle(QueryInterface|JourneyConversionQuery $query): array|JsonSerializable
    {
        $schedulesCount = $this->fetchSchedulesCountByJourney($query->id);

        if ($query->deadline) {
            $schedulesCountByCondition = $this->fetchSchedulesCountByJourneyAndDateWithDeadline(
                $query->id,
                $query->event,
                $query->deadline,
                $query->from,
                $query->to,
            );
        } else {
            $schedulesCountByCondition = $this->fetchSchedulesCountByJourneyAndDate(
                $query->id,
                $query->event,
                Carbon::createFromTimestamp($query->from)->toDateTimeImmutable(),
                Carbon::createFromTimestamp($query->to)->toDateTimeImmutable(),
            );
        }

        return ['rate' => $this->calculateConversionRate($schedulesCount, $schedulesCountByCondition)];
    }

    private function fetchSchedulesCountByJourney(string $id): int
    {
        return DB::table('analytic_schedules')->where('journey_id', $id)->count('id');
    }

    private function fetchSchedulesCountByJourneyAndDateWithDeadline(string $id, string $event, int $deadline, int $from, int $to): int
    {
        $rawSQL = <<<EOD
            SELECT COUNT(*) AS `count`
            FROM (
                SELECT COUNT(*) AS `count`
                FROM `analytic_schedules`
                INNER JOIN `analytic_activities`
                ON `analytic_schedules`.`id` = `analytic_activities`.`schedule_id`
                WHERE `analytic_schedules`.`journey_id` = ?
                AND `analytic_activities`.`occurred_at` <= (SELECT DATE_ADD(`analytic_schedules`.`occurred_at`, INTERVAL ? SECOND))
                AND `analytic_activities`.`title` = ?
                AND `analytic_schedules`.`occurred_at` BETWEEN ? AND ?
                GROUP BY `analytic_activities`.`event_id`
            ) src
        EOD;

        $totalScheduleByCondition = DB::select($rawSQL, [$id, $deadline, $event, $from, $to]);

        if (empty($totalScheduleByCondition)) {
            return 0;
        }

        return $totalScheduleByCondition[0]->count;
    }

    private function fetchSchedulesCountByJourneyAndDate(string $id, string $event, DateTimeImmutable $fromDate, DateTimeImmutable $toDate): int
    {
        $rawSQL = <<<EOD
            SELECT COUNT(*) AS `count`
            FROM (
                SELECT COUNT(*) AS `count`
                FROM `analytic_schedules`
                INNER JOIN `analytic_activities`
                ON `analytic_schedules`.`id` = `analytic_activities`.`schedule_id`
                WHERE `analytic_schedules`.`journey_id` = ?
                AND `analytic_activities`.`title` = ?
                AND `analytic_schedules`.`occurred_at` BETWEEN ? AND ?
                GROUP BY `analytic_activities`.`event_id`
            ) src
        EOD;


        $totalScheduleByCondition = DB::select($rawSQL, [$id, $event, $fromDate, $toDate]);

        if (empty($totalScheduleByCondition)) {
            return 0;
        }

        return $totalScheduleByCondition[0]->count;
    }

    private function calculateConversionRate(int $totalSchedule, int $totalScheduleByCondition): float
    {
        if ($totalSchedule === 0 || $totalScheduleByCondition === 0) {
            return 0;
        }

        $rate = ($totalScheduleByCondition * 100) / $totalSchedule;

        return (float) number_format((float) $rate, 2, '.', '');
    }
}
