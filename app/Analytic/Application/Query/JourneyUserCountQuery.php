<?php

declare(strict_types=1);

namespace App\Analytic\Application\Query;

use App\Shared\Application\Query\QueryInterface;

class JourneyUserCountQuery implements QueryInterface
{
    public function __construct(
        public readonly string $id,
        public readonly bool $onlyActives = true)
    {
    }
}
