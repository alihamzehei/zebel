<?php

declare(strict_types=1);

namespace App\Analytic\Application\Listener;


use App\Plan\Domain\Event\JourneyRemoved;
use Illuminate\Support\Facades\DB;

class RemoveJourneyListener
{
    public function handle(JourneyRemoved $event): void
    {
        DB::table('analytic_schedules')->where('journey_id', $event->id)->delete();
        DB::table('analytic_checkpoints')->where('journey_id', $event->id)->delete();
    }
}
