<?php

declare(strict_types=1);

namespace App\Analytic\Application\Listener;

use App\Collect\Domain\Event\ActivityCollected;
use Illuminate\Support\Facades\DB;

class ActivityCollectedListener
{
    public function handle(ActivityCollected $event): void
    {
        // Check if there's a schedule related to the event,
        // looking for a schedule with the same target, and expectant event [since is important]
        // if the condition matches, store the activity in the context, leave on else.
        if ($id = $this->findScheduleLocalIdFrom($event)) {
            $this->storeActivityForSchedule($id, $event);
        }
    }

    private function findScheduleLocalIdFrom(ActivityCollected $event): ?int
    {
        return DB::table('analytic_schedules')
            ->where('active', 1)
            ->where('expectant', $event->title)
            ->where('target_id', $event->causer)
            ->first()?->id;
    }

    private function storeActivityForSchedule(int $scheduleId, ActivityCollected $event): void
    {
        DB::table('analytic_activities')->insert([
            'event_id' => $event->id,
            'schedule_id' => $scheduleId,
            'title' => $event->title,
            'occurred_at' => $event->occurredAt(),
        ]);
    }
}
