<?php

declare(strict_types=1);

namespace App\Analytic\Application\Listener;

use App\Schedule\Domain\Event\CheckReachabilityCheckpointReached;
use App\Schedule\Domain\Model\Checkpoint\CheckReachabilityCheckpoint;

class PersistCheckReachabilityCheckpointListener extends CheckpointReachedListener
{
    public function handle(CheckReachabilityCheckpointReached $event): void
    {
        $this->persist($event, CheckReachabilityCheckpoint::TYPE);
    }
}
