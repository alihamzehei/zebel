<?php

declare(strict_types=1);

namespace App\Analytic\Application\Listener;

use App\Analytic\Infrastructure\Model\AnalyticScheduleModel;
use App\Schedule\Domain\Event\IdleCheckpointReached;
use App\Schedule\Domain\Model\Checkpoint\IdleCheckpoint;

class PersistIdleCheckpointListener extends CheckpointReachedListener
{
    public function handle(IdleCheckpointReached $event): void
    {
        AnalyticScheduleModel::query()->updateOrCreate(['schedule_id' => $event->scheduleId], [
            'schedule_id' => $event->scheduleId,
            'journey_id' => $event->journeyId,
            'target_id' => $event->targetId,
            'expectant' => $event->expectant,
            'occurred_at' => $event->occurredAt()->format('Y-m-d H:i:s'),
        ]);

        $this->persist($event, IdleCheckpoint::TYPE);
    }
}
