<?php

declare(strict_types=1);

namespace App\Analytic\Application\Listener;

use App\Schedule\Domain\Event\EventOccurrenceCheckpointReached;
use App\Schedule\Domain\Model\Checkpoint\EventOccurrenceCheckpoint;

class PersistEventOccurrenceCheckpointListener extends CheckpointReachedListener
{
    public function handle(EventOccurrenceCheckpointReached $event): void
    {
        $this->persist($event, EventOccurrenceCheckpoint::TYPE);
    }
}
