<?php

declare(strict_types=1);

namespace App\Analytic\Application\Listener;


use App\Schedule\Domain\Event\StopCheckpointReached;
use App\Schedule\Domain\Model\Checkpoint\StopCheckpoint;
use Illuminate\Support\Facades\DB;

class PersistStopCheckpointListener extends CheckpointReachedListener
{
    public function handle(StopCheckpointReached $event): void
    {
        $this->persist($event, StopCheckpoint::TYPE);
        $this->deactivateSchedule($event->scheduleId);
    }

    protected function deactivateSchedule(string $scheduleId): void
    {
        DB::table('analytic_schedules')
            ->where('schedule_id', $scheduleId)
            ->update(['active' => 0]);
    }
}
