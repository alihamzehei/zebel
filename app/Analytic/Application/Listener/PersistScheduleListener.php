<?php

declare(strict_types=1);

namespace App\Analytic\Application\Listener;

use App\Schedule\Domain\Event\ScheduleInitialized;
use Illuminate\Support\Facades\DB;

class PersistScheduleListener
{
    public function handle(ScheduleInitialized $event): void
    {
        DB::table('analytic_schedules')->insert([
            'schedule_id' => $event->id,
            'journey_id' => $event->journeyId,
            'target_id' => $event->target,
            'occurred_at' => $event->occurredAt()->format('Y-m-d H:i:s'),
        ]);
    }
}
