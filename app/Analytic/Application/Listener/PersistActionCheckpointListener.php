<?php

declare(strict_types=1);

namespace App\Analytic\Application\Listener;

use App\Schedule\Domain\Event\ActionCheckpointReached;
use App\Schedule\Domain\Model\Checkpoint\ActionCheckpoint;

class PersistActionCheckpointListener extends CheckpointReachedListener
{
    public function handle(ActionCheckpointReached $event): void
    {
        $this->persist($event, ActionCheckpoint::TYPE);
    }
}
