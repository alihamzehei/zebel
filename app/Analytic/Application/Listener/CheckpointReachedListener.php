<?php

declare(strict_types=1);

namespace App\Analytic\Application\Listener;


use App\Shared\Domain\DomainEvent;
use Illuminate\Support\Facades\DB;

abstract class CheckpointReachedListener
{
    protected function persist(DomainEvent $event, string $type): void
    {
        $schedule = DB::table('analytic_schedules')
            ->select('id', 'journey_id')
            ->where('schedule_id', $event->scheduleId)
            ->first();

        DB::table('analytic_checkpoints')->insert([
            'schedule_id' => $schedule->id,
            'journey_id' => $schedule->journey_id,
            'checkpoint_id' => $event->checkpointId,
            'type' => $type,
            'occurred_at' => $event->occurredAt()->format('Y-m-d H:i:s'),
        ]);
    }
}
