<?php

use App\Schedule\Domain\Event\ActionCheckpointReached;
use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->purpose('Display an inspiring quote');


Artisan::command('app:test', function () {
//    event(new ActionCheckpointReached(
//        '665c38a960a15',
//        '665c56db384e2',
//        '39a720a7-311f-49f2-903a-9732a2cc49e1',
//        '1',
//        []
//    ));
})->purpose('Display an inspiring quote');
